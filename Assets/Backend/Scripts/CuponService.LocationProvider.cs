﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CuponGo
{
    partial class CuponService
    {
        public interface ILocationProvider : IDisposable
        {
            event Action<float, float> PositionChanged;
            event Action<float> OrientationChanged;

            void Start();
            void Stop();
            //void enablePositionUpdate(bool enable);
            //void enableOrientationUpdate(bool enable);
        }

        private class AndroidJavaLocationProvider : AndroidJavaProxy, ILocationProvider
        {
            public event Action<float> OrientationChanged;
            public event Action<float, float> PositionChanged;

            private AndroidJavaObject _prov;

            public AndroidJavaLocationProvider()
                :
                base("com.cupongo.android.LocationListener")
            {
                using (var cls = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                using (var act = cls.GetStatic<AndroidJavaObject>("currentActivity"))
                using (var ctx = act.Call<AndroidJavaObject>("getApplicationContext"))
                {
                    this._prov = new AndroidJavaObject("com.cupongo.android.AndroidJavaLocationProvider", ctx);
                }

                this._prov.Call("setListener", this);
            }

            public void Dispose()
            {
                if (this._prov != null)
                {
                    this._prov.Call("dispose");
                    this._prov = null;
                }
            }

            public void enableOrientationUpdate(bool enable)
            {
                this._prov.Call("enableOrientationUpdate", enable);
            }

            public void enablePositionUpdate(bool enable)
            {
                this._prov.Call("enablePositionUpdate", enable);
            }

            public void Start()
            {
                this._prov.Call("start");
            }

            public void Stop()
            {
                this._prov.Call("stop");
            }

            public void onPositionChanged(float x, float y)
            {
                var h = this.PositionChanged;

                if (h != null)
                {
                    h(x, y);
                }
            }

            public void onOrientationChanged(float angle)
            {
                var h = this.OrientationChanged;

                if (h != null)
                {
                    h(angle);
                }
            }
        }
    }
}