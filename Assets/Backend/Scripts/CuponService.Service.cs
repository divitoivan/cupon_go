﻿using System;
using UnityEngine;
using CuponServerLib;
using Debug = UnityEngine.Debug;

namespace CuponGo
{
    partial class CuponService
    {
        public enum ConnectionState : int
        {
            Disconnected = 0,
            Connecting = 1,
            Connected = 2,
            Error = 3,
        }

        //private const string CLOUD_ADDRESS = "localhost";
        //private const string CLOUD_WEB = "http://localhost:52059/";

        private const string CLOUD_ADDRESS = "pspet.ddns.net";
        private const string CLOUD_WEB = "http://pspet.ddns.net:8083/CuponGO/";

        private const int CLOUD_PORT = 6677;

        //public class Service
        //{
        private const int MESSAGE_CONNECT = 1;
            private const int MESSAGE_RECONNECT = 2;

            //private bool _isLoggedIn;
            private string _userId;
            private string _passwd;
            private string _connectionId;
            private ConnectionState _connectionState;

            private CuponClient _cli;
            private bool _isLoggedIn;

            private string _cloudWeb;
            private Vector2 _currentPosition;
            private int _currentMap = 1;

            public bool IsLoggedIn
            {
                get { return this._isLoggedIn; }
            }

        //public ConnectionState ConnectionState
        //{
        //    get { return this._connectionState; }
        //}

        public class CoroutineResult : CustomYieldInstruction
        {
            private bool _completed;

            public bool Cancelled { get; private set; }

            public Exception Error { get; private set; }

            public override bool keepWaiting
            {
                get
                {
                    return !this._completed;
                }
            }

            public void SetComplete(bool cancelled, Exception error)
            {
                this.Cancelled = cancelled;
                this.Error = error;
                this._completed = true;
            }
        }

        public class CoroutineResult<T> : CoroutineResult
        {
            public T Result { get; private set; }

            public void SetComplete(bool cancelled, Exception error, T result)
            {
                this.Result = result;
                this.SetComplete(cancelled, error);
            }
        }

        public CoroutineResult LoginCoroutine(string user, string passwd)
        {
            var res = new CoroutineResult();

            this.LoginAsync(user, passwd, ares =>
            {
                res.SetComplete(ares.Cancelled, ares.Error);
            });

            return res;
        }

            public void LoginAsync(string user, string passwd, Action<AsyncResult> callback)
            {
                if (this._cli != null)
                {
                    this.CloseCloudConnection();
                }

                //this._isLoggedIn = false;
                this._userId = user;
                this._passwd = passwd;
                this._isLoggedIn = false;

                this.OpenCloudConnectionAsync(res =>
                {
                    if (res.Cancelled || res.Error != null)
                    {
                        this._userId = null;
                        this._passwd = null;
                    }
                    else
                    {
                        this._isLoggedIn = true;
                        this.OnConnectionStateChanged(ConnectionState.Connected);
                    }

                    if (callback != null)
                    {
                        this.runOnUiThread(() =>
                        {
                            callback(res);
                        });
                    }
                });
            }

        public CoroutineResult LogoutCoroutine()
        {
            var res = new CoroutineResult();

            this.LogoutAsync(ares =>
            {
                res.SetComplete(ares.Cancelled, ares.Error);
            });

            return res;
        }

            public void LogoutAsync(Action<AsyncResult> callback)
            {
                this._userId = null;
                this._passwd = null;
                this._isLoggedIn = false;

                this.CloseCloudConnection();
                this.OnConnectionStateChanged(ConnectionState.Disconnected);

                if (callback != null)
                {
                    this.runOnUiThread(() =>
                    {
                        callback(AsyncResult.OK);
                    });
                }
            }

            public void SendPing()
            {
                if (this._isLoggedIn)
                {
                    if (this._connectionState == ConnectionState.Disconnected)
                    {
                        this._connectionState = ConnectionState.Connecting;
                        this.RetryConnection();
                    }
                    else
                    {
                        try
                        {
                            //print("sending ping...");
                            var pos = this._currentPosition;

                            this._cli.LogPosition(this._currentMap, pos.x, pos.y, res =>
                            {
                                if (res.Cancelled)
                                {
                                    print("ping cancelled");

                                    this.CloseCloudConnection();
                                    this.OnConnectionStateChanged(ConnectionState.Connecting);
                                    this.RetryConnection();
                                    return;
                                }

                                Exception exc = res.Error;

                                if (exc != null)
                                {
                                    print("ping failed");
                                    Debug.LogError(res.Error);

                                    this.CloseCloudConnection();
                                    this.OnConnectionStateChanged(ConnectionState.Connecting);
                                    this.RetryConnection();
                                    return;
                                }

                                //print("...ping OK");
                            });
                        }
                        catch (Exception exc)
                        {
                            CDebug.LogError(exc);

                            this.CloseCloudConnection();
                            this.OnConnectionStateChanged(ConnectionState.Connecting);
                            this.RetryConnection();
                        }
                    }
                }
            }

            private void RetryConnection()
            {
                this.OpenCloudConnectionAsync(res =>
                {
                    if (res.Cancelled)
                    {
                        this._connectionState = ConnectionState.Disconnected;
                        return;
                    }

                    var exc = res.Error;

                    if (exc != null)
                    {
                        Debug.LogError(exc);

                        var lexc = exc as LoginException;

                        if (lexc == null || lexc.Status == 1)
                        {
                            this._connectionState = ConnectionState.Disconnected;
                        }
                        else
                        {
                            this.OnConnectionStateChanged(ConnectionState.Error);
                        }

                        return;
                    }

                    this.OnConnectionStateChanged(ConnectionState.Connected);
                });
            }

            //private bool _positionUpdate;
            //private bool _orientationUpdate;
            //private bool _cuponUpdate;
            private ILocationProvider _locationProvider;

            public void SetLocationProvider(ILocationProvider prov)
            {
                var prev = this._locationProvider;

                if (prev != null)
                {
                    prev.Stop();

                    prev.OrientationChanged -= this.OnOrientationChanged;
                    prev.PositionChanged -= this.OnPositionChanged;
                }

                this._locationProvider = prov;

                if (prov != null)
                {
                    prov.OrientationChanged += this.OnOrientationChanged;
                    prov.PositionChanged += this.OnPositionChanged;

                    prov.Start();
                }
            }

            private ILocationProvider CreateLocationProvider()
            {
                //if (Application.platform == RuntimePlatform.Android)
                //{
                //    try
                //    {
                //        return new AndroidJavaLocationProvider();
                //    }
                //    catch (Exception exc)
                //    {
                //        Debug.LogError(exc);
                //    }
                //}

                return null;
            }

            private void OnPause(bool paused)
            {
                if (paused)
                {
                    this.CloseCloudConnection();
                    this.OnConnectionStateChanged(ConnectionState.Disconnected);

                    if (this._locationProvider != null)
                    {
                        this._locationProvider.Stop();
                    }
                }
                else
                {
                    if (this._isLoggedIn)
                    {
                        if (this._cli == null)
                        {
                            this.OnConnectionStateChanged(ConnectionState.Connecting);
                            this.OpenCloudConnectionAsync(res =>
                            {
                                if (res.Cancelled)
                                {
                                    this.OnConnectionStateChanged(ConnectionState.Disconnected);
                                    return;
                                }

                                var exc = res.Error;

                                if (exc != null)
                                {
                                    var lexc = exc as LoginException;

                                    if (lexc == null || lexc.Status == 1)
                                    {
                                        this.OnConnectionStateChanged(ConnectionState.Disconnected);
                                    }
                                    else
                                    {
                                        this.OnConnectionStateChanged(ConnectionState.Error);
                                    }

                                    return;
                                }

                                this.OnConnectionStateChanged(ConnectionState.Connected);
                            });
                        }
                    }

                    if (this._locationProvider != null)
                    {
                        this._locationProvider.Start();
                    }
                }
            }

            private void Quit()
            {
                this.CloseCloudConnection();
            }

            private void OpenCloudConnectionAsync(Action<AsyncResult> callback)
            {
                if (this._cli != null)
                {
                    this._cli.NewMessage -= this.CuponClient_NewMessage;
                    this._cli.Close();
                }

                string cloudAddress = PlayerPrefs.GetString("CLOUD_ADDRESS", CLOUD_ADDRESS);
                int cloudPort = PlayerPrefs.GetInt("CLOUD_PORT", CLOUD_PORT);

                Debug.LogFormat("connecting to cloud: {0}:{1}", cloudAddress, cloudPort);

                this._cli = new CuponClient(cloudAddress, cloudPort);
                this._cli.NewMessage += this.CuponClient_NewMessage;

                this._cli.ConnectAsync(this._userId, this._passwd, res =>
                {
                    if (res.Cancelled || res.Error != null)
                    {
                        if (res.Error != null)
                        {
                            Debug.LogError(res.Error);
                        }

                        if (callback != null)
                        {
                            callback(new AsyncResult(res.Cancelled, res.Error));
                        }
                    }
                    else
                    {
                        if (callback != null)
                        {
                            callback(AsyncResult.OK);
                        }
                    }
                });
            }

            private void CloseCloudConnection()
            {
                if (this._cli != null)
                {
                    this._cli.Close();
                    this._cli = null;
                }
            }
        //}
    }
}