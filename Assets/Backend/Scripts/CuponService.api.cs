﻿using CuponServerLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CuponGo
{
    public partial class CuponService
    {
        public CoroutineResult<Cupon[]> GetUserCuponsCoroutine()
        {
            var res = new CoroutineResult<Cupon[]>();

            this.GetUserCuponsAsync(r =>
            {
                res.SetComplete(r.Cancelled, r.Error, r.Result);
            });

            return res;
        }

        public CoroutineResult<Cupon[]> GetFreeCuponsCoroutine()
        {
            var res = new CoroutineResult<Cupon[]>();

            this.GetFreeCuponsAsync(r =>
            {
                res.SetComplete(r.Cancelled, r.Error, r.Result);
            });

            return res;
        }
        public CoroutineResult CatchCuponCoroutine(int cuponId)
        {
            var res = new CoroutineResult();

            this.CatchCuponAsync(cuponId, r =>
            {
                res.SetComplete(r.Cancelled, r.Error);
            });

            return res;
        }


        public void GetUserCuponsAsync(Action<AsyncResult<Cupon[]>> callback)
        {
            // get cupons
            CDebug.Log("GetUserCupons...");

            this._cli.GetUserCupons(res =>
            {
                CDebug.Log("...GetUserCupons");

                if (res.Cancelled || res.Error != null)
                {
                    this.runOnUiThread(() =>
                    {
                        callback(new AsyncResult<Cupon[]>(res.Cancelled, res.Error, null));
                    });

                    return;
                }

                Cupon[] result;

                var cupons = res.Result;

                if (cupons == null || cupons.Length == 0)
                {
                    result = new Cupon[0];
                }
                else
                {
                    result = new Cupon[cupons.Length];

                    var ids = new List<int>();

                    foreach (var t in cupons)
                    {
                        var id = t.Second;

                        if (!ids.Contains(id))
                        {
                            ids.Add(id);
                        }
                    }

                    CDebug.Log("GetCuponTypes...");
                    // get cupon types
                    this._cli.GetCuponTypes(ids.ToArray(), res2 =>
                    {
                        CDebug.Log("...GetCuponTypes");
                        if (res2.Cancelled || res2.Error != null)
                        {
                            this.runOnUiThread(() =>
                            {
                                callback(new AsyncResult<Cupon[]>(res2.Cancelled, res2.Error, null));
                            });

                            return;
                        }

                        var types = res2.Result;

                        if (types == null || types.Length == 0)
                        {
                            this.runOnUiThread(() =>
                            {
                                callback(new AsyncResult<Cupon[]>(false, new Exception("no types"), null));
                            });

                            return;
                        }

                        ids.Clear();

                        foreach (var t in types)
                        {
                            var id = t.Second;

                            if (!ids.Contains(id))
                            {
                                ids.Add(id);
                            }
                        }

                        CDebug.Log("GetBrands...");
                        // get brands
                        this._cli.GetBrands(ids.ToArray(), res3 =>
                        {
                            CDebug.Log("...GetBrands");
                            if (res3.Cancelled || res3.Error != null)
                            {
                                this.runOnUiThread(() =>
                                {
                                    callback(new AsyncResult<Cupon[]>(res3.Cancelled, res3.Error, null));
                                });

                                return;
                            }

                            var brands = res3.Result;

                            if (brands == null || brands.Length == 0)
                            {
                                this.runOnUiThread(() =>
                                {
                                    callback(new AsyncResult<Cupon[]>(false, new Exception("no brands"), null));
                                });

                                return;
                            }

                            var brandDict = brands.ToDictionary<ValueTuple<int, string>, int>(b => b.First);
                            var typeDict = types.ToDictionary<ValueTuple<int, int, string>, int>(t => t.First);

                            result = new Cupon[cupons.Length];

                            for (int i = 0; i < cupons.Length; i++)
                            {
                                var c = cupons[i];
                                ValueTuple<int, int, string> t;
                                ValueTuple<int, string> b;

                                if (!typeDict.TryGetValue(c.Second, out t))
                                {
                                    //t = null;
                                }

                                if (!brandDict.TryGetValue(t.Second, out b))
                                {
                                    //b = null;
                                }

                                result[i] = new Cupon()
                                {
                                    Id = c.First,
                                    CuponDescription = t.Third,
                                    CompanyName = b.Second,
                                    ImageUrl = GetBrandImageUrl(b.First),
                                    //ImageUrl = this._cloudWeb + "images/cupon_6_mcdonalds.png",
                                };
                            }

                            this.runOnUiThread(() =>
                            {
                                callback(new AsyncResult<Cupon[]>(false, null, result));
                            });
                        });
                    });
                }
            });
        }

        public void GetFreeCuponsAsync(Action<AsyncResult<Cupon[]>> callback)
        {
            // get cupons
            CDebug.Log("GetFreeCupons...");

            this._cli.GetFreeCupons(res =>
            {
                CDebug.Log("...GetFreeCupons");

                if (res.Cancelled || res.Error != null)
                {
                    this.runOnUiThread(() =>
                    {
                        callback(new AsyncResult<Cupon[]>(res.Cancelled, res.Error, null));
                    });

                    return;
                }

                Cupon[] result;

                var cupons = res.Result;

                if (cupons == null || cupons.Length == 0)
                {
                    result = new Cupon[0];
                }
                else
                {
                    result = new Cupon[cupons.Length];

                    var ids = new List<int>();

                    foreach (var t in cupons)
                    {
                        var id = t.Second;

                        if (!ids.Contains(id))
                        {
                            ids.Add(id);
                        }
                    }

                    CDebug.Log("GetCuponTypes...");
                    // get cupon types
                    this._cli.GetCuponTypes(ids.ToArray(), res2 =>
                    {
                        CDebug.Log("...GetCuponTypes");
                        if (res2.Cancelled || res2.Error != null)
                        {
                            this.runOnUiThread(() =>
                            {
                                callback(new AsyncResult<Cupon[]>(res2.Cancelled, res2.Error, null));
                            });

                            return;
                        }

                        var types = res2.Result;

                        if (types == null || types.Length == 0)
                        {
                            this.runOnUiThread(() =>
                            {
                                callback(new AsyncResult<Cupon[]>(false, new Exception("no types"), null));
                            });

                            return;
                        }

                        ids.Clear();

                        foreach (var t in types)
                        {
                            var id = t.Second;

                            if (!ids.Contains(id))
                            {
                                ids.Add(id);
                            }
                        }

                        CDebug.Log("GetBrands...");
                        // get brands
                        this._cli.GetBrands(ids.ToArray(), res3 =>
                        {
                            CDebug.Log("...GetBrands");
                            if (res3.Cancelled || res3.Error != null)
                            {
                                this.runOnUiThread(() =>
                                {
                                    callback(new AsyncResult<Cupon[]>(res3.Cancelled, res3.Error, null));
                                });

                                return;
                            }

                            var brands = res3.Result;

                            if (brands == null || brands.Length == 0)
                            {
                                this.runOnUiThread(() =>
                                {
                                    callback(new AsyncResult<Cupon[]>(false, new Exception("no brands"), null));
                                });

                                return;
                            }

                            var brandDict = brands.ToDictionary<ValueTuple<int, string>, int>(b => b.First);
                            var typeDict = types.ToDictionary<ValueTuple<int, int, string>, int>(t => t.First);

                            result = new Cupon[cupons.Length];

                            for (int i = 0; i < cupons.Length; i++)
                            {
                                var c = cupons[i];
                                ValueTuple<int, int, string> t;
                                ValueTuple<int, string> b;

                                if (!typeDict.TryGetValue(c.Second, out t))
                                {
                                    //t = null;
                                }

                                if (!brandDict.TryGetValue(t.Second, out b))
                                {
                                    //b = null;
                                }

                                result[i] = new Cupon()
                                {
                                    Id = c.First,
                                    CuponDescription = t.Third,
                                    CompanyName = b.Second,
                                    ImageUrl = GetBrandImageUrl(b.First),
                                    //ImageUrl = this._cloudWeb + "images/cupon_6_mcdonalds.png",
                                };
                            }

                            this.runOnUiThread(() =>
                            {
                                callback(new AsyncResult<Cupon[]>(false, null, result));
                            });
                        });
                    });
                }
            });
        }

        public void CatchCuponAsync(int cuponId, Action<AsyncResult> callback)
        {
            // get cupons
            CDebug.Log("CatchCupon...");

            this._cli.CatchCupon(cuponId, res =>
            {
                CDebug.Log("...CatchCupon");

                if (res.Cancelled || res.Error != null)
                {
                    this.runOnUiThread(() =>
                    {
                        callback(new AsyncResult<Cupon[]>(res.Cancelled, res.Error, null));
                    });

                    return;
                }

                this.runOnUiThread(() =>
                {
                    callback(new AsyncResult(false, null));
                });
            });
        }

        private string GetBrandImageUrl(int brandId)
        {
            return this._cloudWeb + "Brand/Logo?id=" + brandId;
        }
    }
}
