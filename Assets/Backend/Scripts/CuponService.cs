﻿using CuponGo.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CuponGo
{
    public partial class CuponService : MonoBehaviour // Singleton<CuponService>
    {
        private static CuponService _instance;

        public static CuponService Instance
        {
            get
            {
                return _instance;
            }
        }

        public static bool IsBakendEnabled
        {
            get
            {
                return PlayerPrefs.GetInt("BACKEND_ENABLED", 0) != 0;
            }
        }

        //private Thread _uiThread;
        private readonly List<IEventHandler> _eventHandlers = new List<IEventHandler>();
        private readonly object _eventSync = new object();
        private bool _connectionStateChanged;
        private bool _positionChanged;
        private float _positionX;
        private float _positionY;
        private bool _orientationChanged;
        private float _orientationAngle;

        private readonly List<Action> _tasks = new List<Action>();

        private void Awake()
        {
            if (FindObjectsOfType(this.GetType()).Length > 1)
            {
                Destroy(this.gameObject);
                return;
            }

            _instance = this;
            DontDestroyOnLoad(this.gameObject);

            this._cloudWeb = PlayerPrefs.GetString("CLOUD_WEB", CLOUD_WEB);
        }

        private void OnApplicationQuit()
        {
            Debug.Log("quit");
            this.Quit();
        }

        private void OnApplicationPause(bool pause)
        {
            //print("pause " + pause);

            if (IsBakendEnabled)
            {
                this.OnPause(pause);
            }
        }

        protected virtual void OnGUI()
        {
            if (this._isLoggedIn)
            {
                float w = Screen.width / 2;
                float h = Screen.height / 10;

                switch (this._connectionState)
                {
                    case CuponService.ConnectionState.Disconnected:
                    case CuponService.ConnectionState.Connecting:
                        if (GUI.Button(new Rect(Screen.width / 2 - (w / 2f), Screen.height / 2 - (h / 2f), w, h), "CONNECTING..."))
                        {
                        }

                        break;

                    case CuponService.ConnectionState.Error:
                        if (GUI.Button(new Rect(Screen.width / 2 - (w / 2f), Screen.height / 2 - (h / 2f), w, h), "CONNECTION ERROR"))
                        {
                            //SceneManager.LoadScene("scene_02");
                            CuponGoManager.Instance.LoadScene(ScenesIndex.Login);
                        }

                        break;
                }
            }
        }

        // Use this for initialization
        private void Start()
        {
            //print("Start");
            EventManager.Instance.AddListener<OnMainCharacterMoveEvent>(this.OnMainCharacterMoveEvent);
            EventManager.Instance.AddListener<OnDropCuponOnBasketEvent>(this.OnDropCuponOnBasketEvent);
            EventManager.Instance.AddListener<ChangeMapLevelEvent>(this.OnChangeMapLevelEvent);
            
            this.StartCoroutine(this.CheckAlive());
        }

        private void OnDestroy()
        {
            EventManager.Instance.RemoveListener<OnMainCharacterMoveEvent>(this.OnMainCharacterMoveEvent);
            EventManager.Instance.RemoveListener<OnDropCuponOnBasketEvent>(this.OnDropCuponOnBasketEvent);
            EventManager.Instance.RemoveListener<ChangeMapLevelEvent>(this.OnChangeMapLevelEvent);
        }

        private void OnMainCharacterMoveEvent(OnMainCharacterMoveEvent e)
        {
            var v = e.MoveDest;
            this._currentPosition = new Vector2(v.x, v.z);
        }

        private void OnDropCuponOnBasketEvent(OnDropCuponOnBasketEvent e)
        {
            var c = e.DropObject.GetComponent<CuponController>();

            if (c != null)
            {
                var cupon = c.Cupon;

                if (cupon != null)
                {
                    //print("catch");
                    this.StartCoroutine(this.StartCatchCuponCoroutine((int)cupon.Id));
                }
            }
        }

        private void OnChangeMapLevelEvent(ChangeMapLevelEvent e)
        {
            // request free cupons
            this.StartCoroutine(this.GetFreeCupons());
        }

        private IEnumerator GetFreeCupons()
        {
            var res = this.GetFreeCuponsCoroutine();

            yield return res;

            if (res.Cancelled || res.Error != null)
            {
                yield break;
            }

            var cupons = res.Result;

            if (cupons != null && cupons.Length > 0)
            {
                runOnUiThread(() =>
                {
                    foreach (var cupon in cupons)
                    {
                        EventManager.Instance.Raise(new OnNewCuponEvent { Cupon = cupon });
                    }
                });
            }
        }

        private IEnumerator StartCatchCuponCoroutine(int cuponId)
        {
            var res = CuponService.Instance.CatchCuponCoroutine(cuponId);
            yield return res;
        }

        private IEnumerator CheckAlive()
        {
            //print("checkAlive");

            while (this.isActiveAndEnabled)
            {
                //print("wait 10");
                yield return new WaitForSecondsRealtime(10f);

                //print("check alive");
                this.SendPing();
            }

            //print("checkAlive END");
        }

        private Action[] _actionTemp = new Action[10];

        private void Update()
        {
            // events
            if (this._eventHandlers.Count > 0)
            {
                lock (this._eventSync)
                {
                    if (this._connectionStateChanged)
                    {
                        foreach (var h in this._eventHandlers)
                        {
                            h.OnConnectionStateChanged(this._connectionState);
                        }

                        this._connectionStateChanged = false;
                    }

                    if (this._positionChanged)
                    {
                        foreach (var h in this._eventHandlers)
                        {
                            h.OnPositionChanged(this._positionX, this._positionY);
                        }

                        this._positionChanged = false;
                    }

                    if (this._orientationChanged)
                    {
                        foreach (var h in this._eventHandlers)
                        {
                            h.OnOrientationChanged(this._orientationAngle);
                        }

                        this._orientationChanged = false;
                    }
                }
            }

            // ejecutar tareas en thread de UI
            int len;

            lock (this._tasks)
            {
                len = Math.Min(this._tasks.Count, this._actionTemp.Length);

                if (len > 0)
                {
                    this._tasks.CopyTo(this._actionTemp);
                    this._tasks.RemoveRange(0, len);
                }
            }

            if (len > 0)
            {
                for (int i = 0; i < len; i++)
                {
                    var t = this._actionTemp[i];
                    t();
                }
            }
        }

        private void runOnUiThread(Action act)
        {
            lock (this._tasks)
            {
                this._tasks.Add(act);
            }
        }

        public interface IEventHandler
        {
            //bool RequiresPositionUpdate { get; }
            //bool RequiresOrientationUpdate { get; }
            //bool RequiresCuponUpdate { get; }

            void OnConnectionStateChanged(ConnectionState state);
            void OnPositionChanged(float x, float y);
            void OnOrientationChanged(float angle);
        }

        private void OnConnectionStateChanged(ConnectionState state)
        {
            //if (Thread.CurrentThread.Equals(this._uiThread))
            //{
            //    if (this._eventHandler != null)
            //    {
            //        this._eventHandler.onConnectionStateChanged(state);
            //    }
            //}
            //else
            //{
                lock (this._eventSync)
                {
                    this._connectionState = state;
                    this._connectionStateChanged = true;
                }
            //}
        }

        private void OnPositionChanged(float x, float y)
        {
            //if (Thread.CurrentThread.Equals(this._uiThread))
            //{
            //    if (this._eventHandler != null)
            //    {
            //        this._eventHandler.onPositionChanged(x, y);
            //    }
            //}
            //else
            //{
                lock (this._eventSync)
                {
                    this._positionChanged = true;
                    this._positionX = x;
                    this._positionY = y;
                }
            //}
        }

        private void OnOrientationChanged(float angle)
        {
            //if (Thread.CurrentThread.Equals(this._uiThread))
            //{
            //    if (this._eventHandler != null)
            //    {
            //        this._eventHandler.onOrientationChanged(angle);
            //    }
            //}
            //else
            //{
                lock (this._eventSync)
                {
                    this._orientationChanged = true;
                    this._orientationAngle = angle;
                }
            //}
        }

        public void AddEventHandler(IEventHandler h)
        {
            // no es necesario la sincronizacion porque debe hacerse en el thread de UI
            this._eventHandlers.Add(h);
        }

        public void RemoveEventHandler(IEventHandler h)
        {
            // no es necesario la sincronizacion porque debe hacerse en el thread de UI
            this._eventHandlers.Remove(h);
        }
    }
}