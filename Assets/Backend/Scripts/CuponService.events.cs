﻿using CuponGo.Events;
using CuponServerLib;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuponGo
{
    partial class CuponService
    {
        private void CuponClient_NewMessage(int messageType, int messageId, CloudMessage msg)
        {
            switch (messageType)
            {
                case CuponClient.MESSAGE_NEWCUPON:
                    var brandId = msg.GetInt32();

                    var cupon = new Cupon()
                    {
                        Id = msg.GetInt32(),
                        CompanyName = msg.GetString(),
                        CuponDescription = msg.GetString(),
                        ImageUrl = GetBrandImageUrl(brandId),
                    };

                    // x, y
                    msg.GetSingle();
                    msg.GetSingle();

                    runOnUiThread(() =>
                    {
                        EventManager.Instance.Raise(new OnNewCuponEvent { Cupon = cupon });
                    });

                    break;

                default:
                    break;
            }
        }
    }
}