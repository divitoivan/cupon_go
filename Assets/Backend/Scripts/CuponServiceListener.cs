﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CuponGo
{
    public class CuponServiceListener : MonoBehaviour, CuponService.IEventHandler
    {

        //private bool _positionUpdate;
        //private bool _orientationUpdate;
        //private bool _cuponUpdate;

        public CuponServiceListener()
            :
            this(true, true, true)
        {

        }

        protected CuponServiceListener(bool positionUpdate, bool orientationUpdate, bool cuponUpdate)
        {
            //this._positionUpdate = positionUpdate;
            //this._orientationUpdate = orientationUpdate;
            //this._cuponUpdate = cuponUpdate;
        }

        //public bool RequiresCuponUpdate
        //{
        //    get
        //    {
        //        return this._cuponUpdate;
        //    }
        //}

        //public bool RequiresOrientationUpdate
        //{
        //    get
        //    {
        //        return this._orientationUpdate;
        //    }
        //}

        //public bool RequiresPositionUpdate
        //{
        //    get
        //    {
        //        return this._positionUpdate;
        //    }
        //}

        public virtual void OnConnectionStateChanged(CuponService.ConnectionState state)
        {
            Debug.Log("conn state: " + state);
        }

        public virtual void OnOrientationChanged(float angle)
        {
            Debug.Log("orientation: " + angle);
        }

        public virtual void OnPositionChanged(float x, float y)
        {
            Debug.Log("position: " + x + " " + y);
        }

        protected virtual void Awake()
        {
            CuponService.Instance.AddEventHandler(this);
        }

        protected virtual void OnDestroy()
        {
            CuponService.Instance.RemoveEventHandler(this);
        }
    }
}