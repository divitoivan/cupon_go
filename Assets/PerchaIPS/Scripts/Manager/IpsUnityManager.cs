﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace CuponGo.PerchaIPS
{
    public class IpsUnityManager : Singleton<IpsUnityManager>
    {
        [SerializeField]
        private string WifiMapFileName = "map_wifi_25_7_2017";
        //private string WifiMapFileName = "casa_1";
        private string WifiMapFileExt = ".pim";

        [SerializeField]
        private IpsParameters ipsParameters;
        public IpsParameters IpsParameters
        {
            get
            {
                if(ipsParameters == null)
                {
                    ipsParameters = new IpsParameters();
                }
                return ipsParameters ;
            }

            set
            {
                ipsParameters = value;
                UpdateIpsParameters();
            }
        }

        private PositionEstimation positionEstimation ;
        public PositionEstimation PositionEstimation
        {
            get
            {
                if(positionEstimation == null)
                {
                    UpdatePositionEstimation();
                }
                return positionEstimation;
            }

        }

        private TestingIpsManager ipsManager;
        public TestingIpsManager IpsManager
        {
            get
            {
                if(ipsManager == null)
                {
                    ipsManager = new TestingIpsManager();
                }
                return ipsManager;
            }
        }

        protected IpsUnityManager()
        {
        }

        public void UpdateIpsParameters()
        {
            IpsManager.IpsParameters = IpsParameters;
            IpsManager.SetWifiMapFile(getWifiMapPath());
        }

        public void UpdatePositionEstimation()
        {
            positionEstimation = IpsManager.Estimation;
        }

        public string getWifiMapPath()
        {
            string appDir = IpsManager.GetAppDir();
#if UNITY_EDITOR
            appDir = Application.dataPath;
            Debug.Log(appDir);
#endif
            string filePath = System.IO.Path.Combine(appDir, WifiMapFileName + WifiMapFileExt);
            
            if (!File.Exists(filePath))
            {
                TextAsset textAsset = Resources.Load(WifiMapFileName) as TextAsset;
                if (textAsset != null)
                {
                    File.WriteAllBytes(filePath, textAsset.bytes);
                }
            }

            return filePath;

        }

        void Start()
        {

            IpsManager.SetWifiMapFile(getWifiMapPath());

            UpdateIpsParameters();

            IpsManager.onStart();
        }

        void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                IpsManager.onStop();
            } else
            {
                IpsManager.onStart();
            }

        }

        void OnDisable()
        {
            IpsManager.onStop();
        }

    }

}

