﻿
//#define IPS_DEBUG

#if UNITY_ANDROID
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using CuponGo.PerchaIPS;


namespace CuponGo.PerchaIPS
{
    public class TestingIpsManager
    {

        private bool serviceRunning = false;

        private AndroidJavaObject androidIpsManager;
        public AndroidJavaObject AndroidIpsManager
        {
            get
            {
                if(androidIpsManager == null)
                {
                    CreateJavaInteface();
                }
                return androidIpsManager;
            }
        }

        public PositionEstimation Estimation
        {
            get
            {
                return GetPositionEstimation();
            }
        }

        private IpsParameters ipsParameters;
        public IpsParameters IpsParameters
        {
            get
            {
                if(ipsParameters == null)
                {
                    IpsParameters = new IpsParameters();
                }
                return ipsParameters;
            }

            set
            {
                ipsParameters = value;
                RefreshAndroidIpsParameters();
            }
        }


        public TestingIpsManager()
        {
            //Debug.Log("androidIpsManager en TestingIpsManager por crear");
            AndroidJavaObject androidIpsManager = AndroidIpsManager;
            if(androidIpsManager != null)
            {
               //Debug.Log("androidIpsManager en TestingIpsManager creado");
            }
        }

        private void CreateJavaInteface()
        {
            AndroidJNIHelper.debug = true;
            AndroidJNI.AttachCurrentThread();// revisar si hace falta porque estoy en otro thread
#if !UNITY_EDITOR
		    AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		    AndroidJavaObject currentActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
#else
            //using gyrodroid fallback
            AndroidJavaObject currentActivity = null;
#endif

            //Debug.Log("TestingIpsManager en TestingIpsManager por llamar");
            androidIpsManager = new AndroidJavaObject("percha.ips.android.manager.testing.TestingIpsManager", currentActivity);
            //Debug.Log("TestingIpsManager en TestingIpsManager por llamado");
        }

        private PositionEstimation GetPositionEstimation()
        {
            //Debug.Log("GetPositionEstimation en TestingIpsManager por llamar");
            using (AndroidJavaObject androidEstimation = AndroidIpsManager.Call<AndroidJavaObject>("getEstimation"))
            {
                //Debug.Log("getEstimation en TestingIpsManager llamado");
#if IPS_DEBUG
                return new PositionEstimation(new AndroidJavaObject());
#else
                return new PositionEstimation(androidEstimation);
#endif
            }
        }

        public void SetWifiMapFile(string filename)
        {
            //Debug.Log("setWifiMap en TestingIpsManager por llamar");
            AndroidIpsManager.Call("setWifiMap",filename);
            //Debug.Log("setWifiMap en TestingIpsManager por llamado");
        }
        
        public String GetAppDir()
        {
            //Debug.Log("GetAppDir en TestingIpsManager por llamar");
            string appDir = AndroidIpsManager.Call<string>("getAppDir");
            //Debug.Log("GetAppDir en TestingIpsManager llamado: " + appDir);
            if (String.IsNullOrEmpty(appDir))
            {
                return null;
            }
            return appDir;
        }

        public void RefreshAndroidIpsParameters()
        {
            //Debug.Log("TestingIpsManager llamo RefreshAndroidIpsParameters");
            using (AndroidJavaObject androidIpsParameters = IpsParameters.CreateAndroidJavaOject())
            {
                AndroidIpsManager.Call("setIpsParameters", androidIpsParameters);
                //Debug.Log("setIpsParameters en TestingIpsManager llamado");
            }
        }

        public void onStart()
        {
            if (!serviceRunning)
            {
                serviceRunning = true;

                //Debug.Log("onStart en TestingIpsManager por llamar");
                AndroidIpsManager.Call("onStart");
                //Debug.Log("onStart en TestingIpsManager por llamado");
            }
        }

        public void onStop()
        {
            if (serviceRunning)
            {
                //Debug.Log("onStop en TestingIpsManager por llamar");
                AndroidIpsManager.Call("onStop");
                //Debug.Log("onStop en TestingIpsManager por llamado");
                serviceRunning = false;
            }
        }
    }


#if IPS_DEBUG
    public class AndroidJavaObject : System.IDisposable
    {

        public void Dispose() { }
        public void Call(string method, params object[] parameters) {
            Debug.Log("Call " + method);
        }
        public void CallStatic(string method, params object[] parameters) { }
        public T Get<T>(string field) {
            Debug.Log("Get " + field);

            if (field == "position")
            {
                double[] result = new double[3];
                result[0] = UnityEngine.Random.Range(-10f, 10f);
                result[1] = UnityEngine.Random.Range(-10f, 10f);
                result[2] = 0f;
                return (T)Convert.ChangeType(result, typeof(T));
            }

            if (field == "positionVariance")
            {
                double[] result = new double[3];
                result[0] = UnityEngine.Random.Range(1f, 5f);
                result[1] = UnityEngine.Random.Range(1f, 5f);
                result[2] = 0f;
                return (T)Convert.ChangeType(result, typeof(T));
            }

            return default(T);
        }
        public void Set<T>(string field, T val) {
            Debug.Log("Set " + field);

        }
        public T GetStatic<T>(string field) { return default(T); }
        public void SetStatic<T>(string field, T val) { }
        public System.IntPtr GetRawObject() { return System.IntPtr.Zero; }
        public System.IntPtr GetRawClass() { return System.IntPtr.Zero; }
        public T Call<T>(string method, params object[] args) {
            Debug.Log("Call " + method);
            return default(T);
        }
        public T CallStatic<T>(string method, params object[] args) { return default(T); }

        public AndroidJavaObject(params object[] parameters) {
            Debug.Log("AndroidJavaObject");
        }
    }

    public class AndroidJavaClass : AndroidJavaObject
    {
        public AndroidJavaClass(string className) { }
    }

    public class AndroidJNI
    {
        public static void AttachCurrentThread() { }

        public static System.IntPtr FindClass(params object[] parameters)
        {
            return System.IntPtr.Zero;
        }

        public static System.IntPtr GetStaticFieldID(params object[] parameters)
        {
            return System.IntPtr.Zero;
        }

        public static System.IntPtr GetStaticObjectField(params object[] parameters)
        {
            return System.IntPtr.Zero;
        }

        public static System.IntPtr GetMethodID(params object[] parameters)
        {
            return System.IntPtr.Zero;
        }

        public static System.IntPtr NewObject(params object[] parameters)
        {
            return System.IntPtr.Zero;
        }
    }

    public class jvalue
    {
        public object l;
    }
#endif

}

#endif
