﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace CuponGo.PerchaIPS
{
    [Serializable]
    public class IpsParameters
    {

        public bool neural = true;
        
        public int k = 3;

        public int noRss = -85;//dB

        public double horizontalMeasurementVariance = 1;// m^2
        public double verticalMeasurementVariance = 0.5 * 0.5;// m^2

        public bool enableKalmanFilter = true;

        public double horizontalVelocityVariance = 0.2 * 0.2;// m^2/s^2
        public double verticalVelocityVariance = 0.01 * 0.01;// m^2/s^2

        public AndroidJavaObject CreateAndroidJavaOject()
        {

            //Debug.Log("IpsParameters en IpsParameters por llamar");
            AndroidJavaObject androidIpsParameters = new AndroidJavaObject("percha.ips.android.manager.testing.IpsParameters");
            //Debug.Log("IpsParameters en IpsParameters por llamado");

            androidIpsParameters.Set<bool>("neural", neural);
            androidIpsParameters.Set<int>("k", k);
            androidIpsParameters.Set<int>("noRss", noRss);
            androidIpsParameters.Set<double>("horizontalMeasurementVariance", horizontalMeasurementVariance);
            androidIpsParameters.Set<double>("verticalMeasurementVariance", verticalMeasurementVariance);
            androidIpsParameters.Set<bool>("enableKalmanFilter", enableKalmanFilter);
            androidIpsParameters.Set<double>("horizontalVelocityVariance", horizontalVelocityVariance);
            androidIpsParameters.Set<double>("verticalVelocityVariance", verticalVelocityVariance);

            return androidIpsParameters;
        }

    }
}
