﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace CuponGo.PerchaIPS
{

    public class PositionEstimation
    {

        /**
         * Instant of the position in milis in Unix time
         */
        [FormerlySerializedAs("instant")]
        [FormerlySerializedAs("i")]
        [SerializeField]
        public long instant = 0; //Unix timestamp milis

        /**
         * Estimated position array. Components correspond to x, y and z respectively. It may be null.
         */
        [FormerlySerializedAs("position")]
        [FormerlySerializedAs("p")]
        [SerializeField]
        public double[] position = null; //m

        /**
         * Estimated bearing. It is measured in degrees, clockwise with 0 pointing upwards (y-axis). Can be NaN if not available.
         */
        [FormerlySerializedAs("bearing")]
        [FormerlySerializedAs("b")]
        [SerializeField]
        public double bearing = Double.NaN; //degrees

        /**
         * Estimated velocity array. Components correspond to x, y and z respectively. It may be null.
         */
        [FormerlySerializedAs("velocity")]
        [FormerlySerializedAs("v")]
        [SerializeField]
        public double[] velocity = null; //m/s

        /**
         * Estimated position variance array, used to determine uncertainty. Components correspond
         * to x, y and z respectively. It may be null.
         */
        [FormerlySerializedAs("positionVariance")]
        [FormerlySerializedAs("pv")]
        [SerializeField]
        public double[] positionVariance = null; //m^2

        /**
         * Estimated velocity variance array, used to determine uncertainty. Components correspond
         * to x, y and z respectively. It may be null.
         */
        [FormerlySerializedAs("velocityVariance")]
        [FormerlySerializedAs("vv")]
        [SerializeField]
        public double[] velocityVariance = null; //(m/s)^2

        /**
         * Estimated position-velocity covariance array. Components correspond to x, y and z
         * respectively. It may be null.
         */
        [FormerlySerializedAs("positionVelocityCovariance")]
        [FormerlySerializedAs("pvc")]
        [SerializeField]
        public double[] positionVelocityCovariance = null; //m^2/s

        public PositionEstimation(AndroidJavaObject javaObject)
        {
            if(javaObject != null)
            {
                FillFromJavaObject(javaObject);
            }
        }

        public Vector3 GetPositionVector()
        {
            if(position != null && !Double.IsNaN(position[0]))
            {
                // unity X = localCoord  X (East)
                // unity Y = localCoord  Z (Floor)
                // unity Z = localCoord  Y (Nort)
                return new Vector3((float)position[0], (float)position[2], (float)position[1]);
            }
            return new Vector3();
        }

        public Vector3 GetVelocityVector()
        {
            if(velocity != null && !Double.IsNaN(velocity[0]))
            {
                // unity X = localCoord  X (East)
                // unity Y = localCoord  Z (Floor)
                // unity Z = localCoord  Y (Nort)
                return new Vector3((float)velocity[0], (float)velocity[2], (float)velocity[1]);
            }
            return new Vector3();
        }

        public void FillFromJavaObject(AndroidJavaObject javaObject)
        {
            //TODO revisar si es necesario levantar todos los campos (por performance)
            if (javaObject != null)
            {
                //Debug.Log("inicio FillFromJavaObject");
                instant = javaObject.Get<long>("instant");
                position = javaObject.Get<double[]>("position");
                bearing = javaObject.Get<double>("bearing");
                //TODO corregir nulls desde plugin por performance
                //Debug.Log("FillFromJavaObject velocity por llamar");
                //velocity = javaObject.Get<double[]>("velocity");
                //Debug.Log("FillFromJavaObject positionVariance por llamar");
                //positionVariance = javaObject.Get<double[]>("positionVariance");
                //Debug.Log("FillFromJavaObject velocityVariance por llamar");
                //velocityVariance = javaObject.Get<double[]>("velocityVariance");
                //Debug.Log("FillFromJavaObject positionVelocityCovariance por llamar");
                //positionVelocityCovariance = javaObject.Get<double[]>("positionVelocityCovariance");
                //Debug.Log("fin FillFromJavaObject");
            }
        }


    }

}
