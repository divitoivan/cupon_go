﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using CuponGo;
using CuponGo.Events;

namespace CuponGo
{
    public class MapCameraController : MonoBehaviourEventHandler
    {
        [SerializeField] private float _pivotHeight = 2;
        [SerializeField] private float _cameraMinBackwardDist = -4;
        [SerializeField] private float _cameraMaxBackwardDist = -10;
        [SerializeField] private float _cameraMinTilt = 10;
        [SerializeField] private float _cameraMaxTilt = 50;
        
        [SerializeField] private float _followTargetSpeed = 10f;
        [SerializeField] private float _turnSmoothing = 0.0f;
       
        [SerializeField] private float _horizontalRotationScaler = 180f; 
        [SerializeField] private float _verticalRotationScaler = 180f;
        [SerializeField] private float _pitchZoomScaler = 5f;
        
        //[SerializeField] private Vector3 m_minZoomPos;
        //[SerializeField] private Vector3 m_maxZoomPos;

        [SerializeField] private Vector3 _centerOnObjectPos;
        [SerializeField] private float _centerOnObjectZoom = 0.5f;
        private float _centerOnObjectAnimationDuration = 1f;

        
        private Camera _camera;
        private Transform _cameraTransform;
        private Transform _cameraPivotTransform;
        private Transform _cameraBaseTransform;
        
        private Transform _toFollowTarget;
        private float _toFollowTargetBaseScreenHeight = 0.5f;

        private float _lookAngle;
        private Quaternion _cameraBaseRot;

        [SerializeField] private float _zoomPercent = 0.5f;
        private float _tiltAngle = 30;

        private bool _rotateEnable = true;
        private bool _centerOnObjectEnable = false;
        private GameObject _centerOnGameObject;
        private float _centerOnObjectAnimationPercent = 0f;
        private Vector3 _centerOnObjectInitialCameraPos;
        private Vector3 _centerOnObjectInitialCameraPivotPos;
        private Vector3 _centerOnObjectCamPos;

        private Quaternion _cameraPivotRot;

        public float HorizontalRotationScaler
        {
            get { return _horizontalRotationScaler; }
            set { _horizontalRotationScaler = value; }
        }

        public float VerticalRotationScaler
        {
            get { return _verticalRotationScaler; }
            set { _verticalRotationScaler = value; }
        }

        public bool CenterOnObjectEnable
        {
            get { return _centerOnObjectEnable; }
        }

        public bool RotateEnable
        {
            get { return _rotateEnable; }

            set { _rotateEnable = value; }
        }

        private void Awake()
        {
            // find the camera in the object hierarchy
            _camera = GetComponentInChildren<Camera>();
            _cameraTransform = _camera.transform;
            _cameraPivotTransform = _cameraTransform.parent;
            _cameraBaseTransform = _cameraPivotTransform.parent;
            
            _cameraPivotTransform.localPosition = Vector3.up * _pivotHeight;

            GameObject go = GameObject.FindGameObjectWithTag(TagsEnum.player.ToString());
            if (go != null)
            {
                _toFollowTarget = go.transform;
            }

        }

        private void Update()
        {
            if (CenterOnObjectEnable)
            {
                CenterCamaraOnObject();
                if (RotateEnable)
                {
                    HandleRotation();
                }
            }
            else
            {
                FollowTarget();
                if (RotateEnable)
                {
                    HandleRotation();
                    HandleZoom();
                }
                ProtectFromWallClip();
            }

        }

        private void FollowTarget()
        {
            if (_toFollowTarget == null) return;
            _cameraBaseTransform.position = Vector3.Lerp(_cameraBaseTransform.position, _toFollowTarget.position, Time.deltaTime * _followTargetSpeed);
            _toFollowTargetBaseScreenHeight = _camera.WorldToScreenPoint(_toFollowTarget.position).y/_camera.pixelHeight;
        }

        private void HandleRotation()
        {
            //update target look angle
            float horizontalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragHorizontalAxisName);
            float verticalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragVerticalAxisName);
            
            float horizontalMoveOrigin = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchPositionHorizontalAxisName);
            float verticalMoveOrigin = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchPositionVerticalAxisName);

            //rotate look angle
            if (verticalMoveOrigin < _toFollowTargetBaseScreenHeight)
            {
                _lookAngle += horizontalMove * _horizontalRotationScaler;
            }
            else
            {
                _lookAngle -= horizontalMove * _horizontalRotationScaler;
            }

            _cameraBaseRot = Quaternion.Euler(0f, _lookAngle, 0f);

            if (_turnSmoothing > 0)
            {
                _cameraBaseTransform.localRotation = Quaternion.Slerp(_cameraBaseTransform.localRotation, _cameraBaseRot,
                    _turnSmoothing * Time.deltaTime);
            }
            else
            {
                _cameraBaseTransform.localRotation = _cameraBaseRot;
            }

            //rotate tilt angle
            _tiltAngle -= verticalMove * _verticalRotationScaler;
            if (_tiltAngle > _cameraMaxTilt)
            {
                _tiltAngle = _cameraMaxTilt;
            }
            else if (_tiltAngle < _cameraMinTilt)
            {
                _tiltAngle = _cameraMinTilt;
            }

            _cameraPivotRot = Quaternion.Euler(_tiltAngle, 0f, 0f);
            _cameraPivotTransform.localRotation = _cameraPivotRot;

        }

        private void HandleZoom()
        {
            //update zoom percent
            float pitch = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchPitchAxisName);
            _zoomPercent = Mathf.Clamp(_zoomPercent + pitch * _pitchZoomScaler, 0f, 1f);

            //update pivot position
            //_cameraPivotTransform.transform.localPosition = Vector3.LerpUnclamped(m_minZoomPos, m_maxZoomPos, _zoomPercent);
            _cameraTransform.localPosition =
                Vector3.forward * Mathf.Lerp(_cameraMinBackwardDist, _cameraMaxBackwardDist, _zoomPercent);

            //update pivot rotation
            //m_TiltAngle = Mathf.LerpUnclamped(m_minZoomTilt, m_maxZoomTilt, m_ZoomPercent);
            //m_PivotTargetRot = Quaternion.Euler(m_TiltAngle, 0f, 0f);
            //m_Pivot.localRotation = m_PivotTargetRot;
        }

        private void ProtectFromWallClip()
        {
            //TODO esto poray no tiene sentido implementarlo, va a depender mucho de como sean los modelos 3d y como manejemos el tema de multiples pisos. Asi que lo dejo para despeus
        }

        private void CenterCamaraOnObject()
        {

            if (_centerOnGameObject != null)
            {
                _centerOnObjectCamPos = _centerOnGameObject.transform.position;
                _centerOnObjectAnimationPercent = Mathf.Clamp(
                    _centerOnObjectAnimationPercent + Time.deltaTime / _centerOnObjectAnimationDuration, 0f, 1f);
            }
            else
            {
                _centerOnObjectAnimationPercent = Mathf.Clamp(
                    _centerOnObjectAnimationPercent - Time.deltaTime / _centerOnObjectAnimationDuration, 0f, 1f);
                if (Mathf.Approximately(_centerOnObjectAnimationPercent, 0f))
                {
                    _centerOnObjectEnable = false;
                }
            }

            _cameraBaseTransform.position = Vector3.LerpUnclamped(_centerOnObjectInitialCameraPos, _centerOnObjectCamPos,
                _centerOnObjectAnimationPercent);
            _cameraPivotTransform.transform.localPosition = Vector3.LerpUnclamped(_centerOnObjectInitialCameraPivotPos,
                _centerOnObjectPos, _centerOnObjectAnimationPercent);
            float aux = Mathf.Lerp(_zoomPercent, _centerOnObjectZoom, _centerOnObjectAnimationPercent);
            _cameraTransform.localPosition = Vector3.forward * Mathf.Lerp(_cameraMinBackwardDist, _cameraMaxBackwardDist, aux);

        }

        public void CenterCameraOnObject(GameObject gameObject)
        {
            _centerOnGameObject = gameObject;
            if (gameObject != null)
            {
                _centerOnObjectAnimationPercent = 0f;
                _centerOnObjectInitialCameraPos = _cameraBaseTransform.position;
                _centerOnObjectInitialCameraPivotPos = _cameraPivotTransform.transform.localPosition;
                _centerOnObjectEnable = true;
            }
        }

        public override void SubscribeEvents()
        {
            EventManager.Instance.AddListener<CameraActivateRotationEvent>(OnActivateCameraRotationEvent);
            EventManager.Instance.AddListener<CameraCenterOnObjectEvent>(OnCameraCenterOnObjectEvent);
            EventManager.Instance.AddListener<OnMainCharacterChangeEvent>(OnMainCharacterChangeEvent);
        }

        public override void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<CameraActivateRotationEvent>(OnActivateCameraRotationEvent);
            EventManager.Instance.RemoveListener<CameraCenterOnObjectEvent>(OnCameraCenterOnObjectEvent);
            EventManager.Instance.RemoveListener<OnMainCharacterChangeEvent>(OnMainCharacterChangeEvent);
        }

        private void OnActivateCameraRotationEvent(CameraActivateRotationEvent e)
        {
            RotateEnable = e.activateRotation;
        }

        private void OnCameraCenterOnObjectEvent(CameraCenterOnObjectEvent e)
        {
            CenterCameraOnObject(e.CenterGameObject);
        }

        private void OnMainCharacterChangeEvent(OnMainCharacterChangeEvent e)
        {
            _toFollowTarget = e.NewCharacterTransform;
        }
    }
}