﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using ZXing;
using CuponGo;
using System;
using UnityStandardAssets.CrossPlatformInput;

public class QRCameraController : MonoBehaviour
{
    public RawImage image;
    public RectTransform imageParent;
    public AspectRatioFitter imageFitter;

    public Texture targetTexture;

    public GameObject configPopupPrefab;
    public Canvas popupCanvas;
    public RectTransform message;

    public GameObject productImage;
    public GameObject lastBidLabel;
    public GameObject remainingTimeLabel;
    public GameObject bidInput;

//    private readonly string[] _productUrls = new string[]
//        {
//"http://i-cdn.phonearena.com/images/phone360view/11839-large/Apple-iPhone-7-360-0.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11843-large/Apple-iPhone-7-360-1.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11845-large/Apple-iPhone-7-360-2.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11847-large/Apple-iPhone-7-360-3.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11848-large/Apple-iPhone-7-360-4.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11849-large/Apple-iPhone-7-360-5.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11850-large/Apple-iPhone-7-360-6.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11851-large/Apple-iPhone-7-360-7.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11852-large/Apple-iPhone-7-360-8.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11853-large/Apple-iPhone-7-360-9.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11854-large/Apple-iPhone-7-360-10.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11855-large/Apple-iPhone-7-360-11.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11856-large/Apple-iPhone-7-360-12.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11857-large/Apple-iPhone-7-360-13.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11858-large/Apple-iPhone-7-360-14.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11859-large/Apple-iPhone-7-360-15.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11860-large/Apple-iPhone-7-360-16.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11861-large/Apple-iPhone-7-360-17.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11862-large/Apple-iPhone-7-360-18.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11863-large/Apple-iPhone-7-360-19.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11864-large/Apple-iPhone-7-360-20.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11865-large/Apple-iPhone-7-360-21.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11866-large/Apple-iPhone-7-360-22.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11867-large/Apple-iPhone-7-360-23.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11868-large/Apple-iPhone-7-360-24.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11869-large/Apple-iPhone-7-360-25.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11870-large/Apple-iPhone-7-360-26.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11871-large/Apple-iPhone-7-360-27.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11872-large/Apple-iPhone-7-360-28.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11873-large/Apple-iPhone-7-360-29.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11874-large/Apple-iPhone-7-360-30.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11875-large/Apple-iPhone-7-360-31.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11876-large/Apple-iPhone-7-360-32.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11877-large/Apple-iPhone-7-360-33.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11878-large/Apple-iPhone-7-360-34.jpg",
//"http://i-cdn.phonearena.com/images/phone360view/11879-large/Apple-iPhone-7-360-35.jpg",
//        };

    private enum QRState
    {
        Scanning,
        ViewAuction,
    }

    private QRState _currentState = QRState.Scanning;
    private ConfigPopUp configPopUp;

    // Device cameras
    WebCamDevice activeCameraDevice;

    WebCamTexture activeCameraTexture;

    // Image rotation
    Vector3 rotationVector = new Vector3(0f, 0f, 0f);

    // Image uvRect
    Rect defaultRect = new Rect(0f, 0f, 1f, 1f);
    Rect fixedRect = new Rect(0f, 1f, 1f, -1f);

    // Image Parent's scale
    Vector3 defaultScale = new Vector3(1f, 1f, 1f);
    Vector3 fixedScale = new Vector3(-1f, 1f, 1f);

    private BarcodeReader barCodeReader;

    private Texture2D[][] _product;
    private int _productLevel;
    private int _productIndex;
    private float _productLevelAnalog;
    private float _productIndexAnalog;
    //private Texture2D _productLoading;
    private bool _moving;
    //private bool _over;

    private float _auctionTime;
    private float _auctionStartTime;
    private bool _auctionAnim;

    void Start()
    {
        // Check for device cameras
        if (WebCamTexture.devices.Length == 0)
        {
            Debug.Log("No devices cameras found");
            this._cameraInitialized = true;
        }
        else
        {
            // Get the device's cameras and create WebCamTextures with them
            var backCameraDevice = WebCamTexture.devices.First();

            var backCameraTexture = new WebCamTexture(backCameraDevice.name);

            // Set camera filter modes for a smoother looking image
            backCameraTexture.filterMode = FilterMode.Trilinear;

            // Set the camera to use by default
            SetActiveCamera(backCameraTexture);

            this.barCodeReader = new BarcodeReader();
        }

        this.StartCoroutine(this.ReadQR());
    }

    // Set the device camera to use and start it
    public void SetActiveCamera(WebCamTexture cameraToUse)
    {
        if (activeCameraTexture != null)
        {
            activeCameraTexture.Stop();
        }

        activeCameraTexture = cameraToUse;
        activeCameraDevice = WebCamTexture.devices.FirstOrDefault(device =>
            device.name == cameraToUse.deviceName);

        image.texture = activeCameraTexture;
        //image.material.mainTexture = activeCameraTexture;

        activeCameraTexture.Play();
    }

    //private void OnMouseEnter()
    //{
    //    this._over = true;
    //}

    //private void OnMouseExit()
    //{
    //    this._over = false;
    //}

    // Make adjustments to image every frame to be safe, since Unity isn't
    // guaranteed to report correct data as soon as device camera is started
    void Update()
    {
        /*
        if (Input.touchCount == 1)
        {
            touchInit = true;
        }

        if (touchInit && Input.touchCount == 0)
        {
            touchInit = false;
            SwitchCamera();
        }

        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            SwitchCamera();
        }
        */

        if (this._currentState == QRState.ViewAuction && this.message != null)
        {
            if (this._auctionAnim)
            {
                float scale = 0.5f + (Time.realtimeSinceStartup - this._auctionStartTime);

                if (scale >= 1f)
                {
                    this.message.transform.localScale = new Vector3(1f, 1f, 1f);
                    this._auctionAnim = false;
                }
                else
                {
                    this.message.transform.localScale = new Vector3(scale, scale, 1f);
                }
            }
            else
            {
                //if (this._over)
                //{
                if (this.IsOver360())
                {
                    float horizontalMove = 0f;
                    float verticalMove = 0f;

#if UNITY_EDITOR
                    if (Input.mousePresent)
                    {
                        if (Input.GetMouseButton(0))
                        {
                            horizontalMove = Input.GetAxis("Mouse X") * 2f;
                            verticalMove = Input.GetAxis("Mouse Y") * 2f;
                        }
                    }
                    else
                    {
#else
                        horizontalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragHorizontalAxisName) * 10f;
                        verticalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragVerticalAxisName) * 10f;
#endif
#if UNITY_EDITOR
                    }
#endif
                    if (horizontalMove != 0f || verticalMove != 0f)
                    {
                        if (verticalMove != 0f)
                        {
                            this._productLevelAnalog -= verticalMove;
                            var f = (float)(this._product.Length - 1) + 0.5f;

                            if (this._productLevelAnalog < 0.5f)
                            {
                                this._productLevelAnalog = 0.5f;
                            }
                            else if (this._productLevelAnalog > f)
                            {
                                this._productLevelAnalog = f;
                            }
                        }

                        var lvl = (int)Math.Floor(this._productLevelAnalog);

                        if (horizontalMove != 0f)
                        {
                            //Debug.Log("H: " + horizontalMove + " V: " + verticalMove);

                            this._productIndexAnalog -= horizontalMove;
                            var f = (float)this._product[lvl].Length;

                            if (this._productIndexAnalog < 0f)
                            {
                                while (this._productIndexAnalog < 0f)
                                {
                                    this._productIndexAnalog += f;
                                }
                            }
                            else
                            {
                                while (this._productIndexAnalog >= f)
                                {
                                    this._productIndexAnalog -= f;
                                }
                            }
                        }

                        this.ShowProductImage(lvl, (int)Math.Floor(this._productIndexAnalog));
                        this._moving = true;
                    }
                    else if (this._moving)
                    {
                        this._productLevelAnalog = (float)this._productLevel + 0.5f;
                        this._productIndexAnalog = (float)this._productIndex + 0.5f;
                        this._moving = false;
                    }
                }
                else if (this._moving)
                {
                    this._productLevelAnalog = (float)this._productLevel + 0.5f;
                    this._productIndexAnalog = (float)this._productIndex + 0.5f;
                    this._moving = false;
                }
            }
        }

        if (activeCameraTexture == null)
        {
            return;
        }

        // Skip making adjustment for incorrect camera data
        if (activeCameraTexture.width < 100)
        {
            Debug.Log("Still waiting another frame for correct info... " + activeCameraTexture.width);
            return;
        }

        // Rotate image to show correct orientation
        rotationVector.z = -activeCameraTexture.videoRotationAngle;
        image.rectTransform.localEulerAngles = rotationVector;

        // Set AspectRatioFitter's ratio
        float videoRatio =
            (float)activeCameraTexture.width / (float)activeCameraTexture.height;
        imageFitter.aspectRatio = videoRatio;

        // Unflip if vertically flipped
        image.uvRect =
            activeCameraTexture.videoVerticallyMirrored ? fixedRect : defaultRect;

        // Mirror front-facing camera's image horizontally to look more natural
        imageParent.localScale =
            activeCameraDevice.isFrontFacing ? fixedScale : defaultScale;

        this._cameraInitialized = true;
    }

    private void DestroyProduct()
    {
        //if (this._productLoading != null)
        //{
        //    DestroyImmediate(this._productLoading, true);
        //    this._productLoading = null;
        //}

        if (this._product != null)
        {
            foreach (var arr in this._product)
            {
                if (arr != null)
                {
                    foreach (var txt in arr)
                    {
                        if (txt != null)
                        {
                            DestroyImmediate(txt, true);
                        }
                    }
                }
            }

            this._product = null;
        }
    }

    private void OnDestroy()
    {
        this.DestroyProduct();
    }

    private bool _cameraInitialized;
    private bool _curr;
    private string _text;
    private Color32[] _colors = new Color32[0];
    private byte[] _rgb;

    private IEnumerator ReadQR()
    {
        while (this.isActiveAndEnabled && !this._cameraInitialized)
        {
            yield return new WaitForSeconds(0.5f);
        }

        while (this.isActiveAndEnabled)
        {
            if (this._currentState == QRState.Scanning)
            {
                if (this.activeCameraTexture != null)
                {
                    var w = this.activeCameraTexture.width;
                    var h = this.activeCameraTexture.height;

                    var c = w * h;

                    if (c > this._colors.Length)
                    {
                        this._colors = new Color32[c];
                        this._rgb = new byte[c * 3];
                    }

                    this.activeCameraTexture.GetPixels32(this._colors);

                    for (var i = 0; i < c; i++)
                    {
                        var j = i * 3;

                        var color = this._colors[i];
                        this._rgb[j] = color.r;
                        this._rgb[j + 1] = color.g;
                        this._rgb[j + 2] = color.b;
                    }

                    var data = this.barCodeReader.Decode(this._rgb, w, h, RGBLuminanceSource.BitmapFormat.RGB24);
                    //var data = barCodeReader.decode(cameraFeed.Pixels, cameraFeed.BufferWidth, cameraFeed.BufferHeight, RGBLuminanceSource.BitmapFormat.RGB24);
                    if (data != null)
                    {
                        // QRCode detected.
                        var text = data.Text;
                        Debug.Log(text);

                        //this._text = text;
                        //this._curr = true;

                        //var txt = new Texture2D(w, h);
                        //txt.SetPixels32(this._colors);
                        //txt.Apply();

                        //if (AuctionController.background != null)
                        //{
                        //    DestroyImmediate(AuctionController.background);
                        //}

                        //AuctionController.background = txt;
                        //AuctionController.code = text;
                        //CuponGoManager.Instance.LoadScene(ScenesIndex.Auction, false);
                        //yield break;

                        this.ShowAuction();
                    }
                    //else
                    //{
                    //    Debug.Log("No QR code detected !");
                    //    this._curr = false;
                    //}
                }
            }
            else if (this._currentState == QRState.ViewAuction)
            {
                if (this.remainingTimeLabel != null)
                {
                    var txt = this.remainingTimeLabel.GetComponent<Text>();

                    if (txt != null)
                    {
                        var elapsed = Time.realtimeSinceStartup - this._auctionStartTime;
                        var rem = this._auctionTime - elapsed;

                        string s;

                        if (rem <= 0f)
                        {
                            s = "00:00:00";
                        }
                        else
                        {
                            var ts = TimeSpan.FromSeconds(rem);
                            s = FormatTimespan(ts);
                        }

                        txt.text = s;
                    }
                }
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    void OnApplicationPause(bool pause)
    {
        if (activeCameraTexture != null)
        {
            if (pause)
            {
                activeCameraTexture.Stop();
            }
            else
            {
                activeCameraTexture.Play();
            }
        }
    }

    void OnDisable()
    {
        if (activeCameraTexture != null)
        {
            activeCameraTexture.Stop();
        }
    }

    private void OnGUI()
    {
        var w = Screen.width;
        var h = Screen.height;

        GUIStyle st;

        float l;

        if (h < w)
        {
            l = h * 8f / 10f;
        }
        else
        {
            l = w * 8f / 10f;
        }

        var pos = new Rect((w - l) / 2f, (h - l) / 2f, l, l);

        if (this._currentState == QRState.Scanning)
        {
            if (this.targetTexture != null)
            {
                st = new GUIStyle();

                //int m = (int)(l / 10f);
                //st.border = new RectOffset(m, m, m, m);
                st.stretchWidth = true;
                st.stretchHeight = true;

                //GUI.Box(new Rect((w - l) / 2f, (h - l) / 2f, l, l), this.targetTexture, st);
                //var pos = new Rect((w - l) / 2f, (h - l) / 2f, l, l);
                GUI.DrawTexture(pos, this.targetTexture, ScaleMode.StretchToFill);
            }
            //}

            // view QR content

            //////var w = (float)Screen.width;
            //////var h = (float)Screen.height;
            //////var wmid = w / 2f;
            //////var hmid = h / 2f;

            ////if (GUI.Button(new Rect(0f, 0f, 100f, 40f), (this._curr ? "OK" : "NOK") + ": " + this._text))
            ////{
            ////}
            ////int w = Screen.width, h = Screen.height;

            //var style = new GUIStyle();

            //Rect rect = new Rect(0, 200, w, h * 2 / 100);
            //style.alignment = TextAnchor.UpperLeft;
            ////style.fontSize = h * 2 / 100;
            //style.fontSize = h * 2 / 50;
            //style.normal.textColor = this._curr ? new Color(0.0f, 0.0f, 0.5f, 1.0f) : new Color(1.0f, 0.0f, 0.0f, 1.0f);
            //string text = this._text;
            //GUI.Label(rect, text, style);

            //if (this._currentState == QRState.Scanning)
            //{
            st = new GUIStyle();

            if (GUI.Button(pos, "", st))
            {
                if (this.activeCameraTexture != null)
                {
                    if (this._currentState == QRState.Scanning)
                    {
                        this.ShowAuction();
                    }
                    else
                    {
                        this.activeCameraTexture.Play();
                        this._currentState = QRState.Scanning;
                    }

                    //this.activeCameraTexture.GetPixels32(this._colors);

                    //w = this.activeCameraTexture.width;
                    //h = this.activeCameraTexture.height;

                    //var txt = new Texture2D(w, h);
                    //txt.SetPixels32(this._colors);
                    //txt.Apply();

                    //if (AuctionController.background != null)
                    //{
                    //    DestroyImmediate(AuctionController.background);
                    //}

                    //AuctionController.background = txt;
                }
                else
                {
                    //AuctionController.background = null;
                    if (this._currentState == QRState.Scanning)
                    {
                        this.ShowAuction();
                    }
                    else
                    {
                        this._currentState = QRState.Scanning;
                    }
                }

                //AuctionController.code = null;
                //CuponGoManager.Instance.LoadScene(ScenesIndex.Auction, false);
            }
        }
#if UNITY_EDITOR
        else if (this._currentState == QRState.ViewAuction)
        {
            if (GUI.Button(new Rect(0f, (h / 2f) + 20f, 40f, 40f), "<"))
            {
                this.ShowNextProductImage(-1);
            }

            if (GUI.Button(new Rect(w - 40f, (h / 2f) + 20f, 40f, 40f), ">"))
            {
                this.ShowNextProductImage(1);
            }
        }
#endif
    }

    private string FormatTimespan(TimeSpan ts)
    {
        return string.Format("{0:00}:{1:00}:{2:00}", (int)Math.Floor(ts.TotalHours), ts.Minutes, ts.Seconds);
    }

    private void ShowAuction()
    {
        if (this.activeCameraTexture != null)
        {
            this.activeCameraTexture.Pause();
            this.activeCameraTexture.Stop();
        }

        this._currentState = QRState.ViewAuction;

        //this._auctionTime = UnityEngine.Random.Range(90f * 60f, 120f * 60f);
        this._auctionTime = UnityEngine.Random.Range(20f * 60f, 30f * 60f);
        this._auctionStartTime = Time.realtimeSinceStartup;

        if (this.productImage != null)
        {
            var img = this.productImage.GetComponent<RawImage>();

            if (img != null)
            {
                this.DestroyProduct();

                //this._product = new Texture2D[36];
                //this._product = new Texture2D[24];
                this._product = new Texture2D[3] [];

                for (int i = 0; i < this._product.Length; i++)
                {
                    this._product[i] = new Texture2D[24];
                }

                this._productLevel = 0;
                this._productLevelAnalog = 0.5f;

                this._productIndex = 0;
                this._productIndexAnalog = 0.5f;

                //this._productLoading = Resources.Load<Texture2D>("sale_1");

                //if (this._productLoading == null)
                //{
                //    Debug.Log("no image file");
                //}

                //img.texture = this._productLoading;

                //this.StartCoroutine(this.LoadProductImage(0));

                for (var j = 0; j < this._product.Length; j++)
                {
                    for (int i = 0; i < this._product[j].Length; i++)
                    {
                        this.StartCoroutine(this.LoadProductImage(j, i));
                    }
                }
            }
            else
            {
                Debug.Log("no image 2");
            }
        }
        else
        {
            Debug.Log("no image");
        }

        if (this.remainingTimeLabel != null)
        {
            var txt = this.remainingTimeLabel.GetComponent<Text>();
            if (txt != null)
            {
                txt.text = FormatTimespan(TimeSpan.FromSeconds(this._auctionTime));
            }
        }

        //GameObject popup = Instantiate(configPopupPrefab);
        //popup.SetActive(true);
        //popup.transform.localScale = Vector3.zero;
        //popup.transform.SetParent(popupCanvas.transform, false);
        //configPopUp = popup.GetComponent<ConfigPopUp>();
        //configPopUp.SetCuponList(Cupon.GetCuponDebugList());
        ////configPopUp.AddCuponController.onCuponListItemClickEvent += onCuponItemClicked;
        //configPopUp.onCloseEvent += onPopupClose;
        //configPopUp.Open(popupCanvas);

        //this.ActiveObjectAsync(this.message.gameObject, true, 0.1f);
        this.message.gameObject.SetActive(true);
        this._auctionAnim = true;
    }

    private void onPopupClose()
    {
        //EventManager.Instance.Raise(new CameraActivateRotationEvent { activateRotation = true });
        this._currentState = QRState.Scanning;
    }

    private IEnumerator ActiveObjectAsync(GameObject gameObject, bool active, float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(active);
    }

    public void OkClick()
    {
        CuponGoManager.Instance.OnBackPress(new CuponGo.Events.OnBackPressedEvent());
    }

    private IEnumerator LoadProductImage(int lvl, int index)
    {
        var url = "file://" + Application.temporaryCachePath + "/product_360_" + lvl + "_" + index + ".jpg";
        var www = new WWW(url);

        yield return www;

        if (www.error == null)
        {
            var txt = www.texture;

            if (txt != null)
            {
                this._product[lvl][index] = txt;

                if (this._productIndex == index && this._productLevel == lvl)
                {
                    //Debug.Log("load from file: " + index);
                    this.productImage.GetComponent<RawImage>().texture = txt;
                }
            }
        }
        else
        {
            yield return this.StartCoroutine(this.DownloadProductImage(lvl, index));
        }
    }

    private IEnumerator DownloadProductImage(int lvl, int index)
    {
        //var url = "http://i-cdn.phonearena.com/images/phone360view/11839-large/Apple-iPhone-7-360-" + index + ".jpg";
        //var url = this._productUrls[index];
        var url = "https://sf-hosting-sys-live.s3.amazonaws.com/home/1682/shared/img-sets/79637/spinset_750747_r" + (lvl + 1).ToString("00") + "_c" + (index + 1).ToString("00") + ".jpg/is_main.jpg";
        Debug.Log("download: " + url);

        var www = new WWW(url);

        yield return www;

        if (www.error == null)
        {
            var txt = www.texture;

            if (txt != null)
            {
                var fname = Application.temporaryCachePath + "/product_360_" + lvl + "_" + index + ".jpg";

                if (System.IO.File.Exists(fname))
                {
                    System.IO.File.Delete(fname);
                }

                using (var s = System.IO.File.Create(fname))
                {
                    var bs = www.bytes;
                    s.Write(bs, 0, bs.Length);
                }

                this._product[lvl][index] = txt;

                if (this._productIndex == index && this._productLevel == lvl)
                {
                    Debug.Log("load from web: " + index);
                    this.productImage.GetComponent<RawImage>().texture = txt;
                }
            }
        }
    }

    private void ShowNextProductImage(int offset)
    {
        Debug.Log("show next: " + offset);
        int index = this._productIndex;

        if (offset > 0)
        {
            index = (index + offset) % this._product.Length;
        }
        else
        {
            index += offset;

            while (index < 0)
            {
                index += this._product.Length;
            }
        }

        this.ShowProductImage(this._productLevel, index);
    }

    private void ShowProductImage(int lvl, int index)
    {
        this._productLevel = lvl;
        this._productIndex = index;
        var txt = this._product[lvl][index];

        if (txt != null)
        {
            //Debug.Log("load: " + index);
            this.productImage.GetComponent<RawImage>().texture = txt;
        }
        else
        {
            //Debug.Log("downloading: " + index);
            //this.productImage.GetComponent<RawImage>().texture = this._productLoading;
            this.StartCoroutine(this.LoadProductImage(lvl, index));
        }
    }

    private bool IsOver360()
    {
        return true;
        //var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //RaycastHit hit;

        //if (Physics.Raycast(ray, out hit))
        //{
        //    print(hit.collider.name);
        //    return "image360".Equals(hit.collider.name);
        //}

        //return false;
    }
}
