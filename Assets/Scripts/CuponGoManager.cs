﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using CuponGo.PerchaIPS;
using CuponGo.Events;
using UnityEngine.CrashReportHandler;

namespace CuponGo
{

    //este enum tiene que corresponderse con el generado por unity en build seting
    public enum ScenesIndex { MainMenu = 0, Map = 1, Camera = 2, CuponMenu = 3, CuponDetail = 4, CameraSigns = 5, QRCapture = 6, Auction = 7, ArScene = 8, Login = 9 , ShopWindow = 10 , CherArScene = 11 };

    public class CuponGoManager : Singleton<CuponGoManager>, IEventHandler
    {

        private int qualityLevel = 0;
        private Stack<ScenesIndex> sceneStack ;
        private ScenesIndex currentSceneIndex;
        private AppState appState;
        private GoogleAnalyticsV4 googleAnalytics;

        private string defaultAppStateFile = "AppState.json";

        protected CuponGoManager() {
            sceneStack = new Stack<ScenesIndex>();
            currentSceneIndex = ScenesIndex.MainMenu;
        }

        public AppState AppState
        {
            get { return appState; }
        }

        public GoogleAnalyticsV4 GoogleAnalytics
        {
            get
            {
                if (googleAnalytics == null)
                {
                    GameObject go = GameObject.FindGameObjectWithTag(TagsEnum.analytics.ToString());
                    if(go != null)
                    {
                        googleAnalytics = go.GetComponent<GoogleAnalyticsV4>();
                        googleAnalytics.StartSession();
                    }
                }
                return googleAnalytics;
            }
        }

        private void Awake()
        {
            LoadAppState(defaultAppStateFile);
            MobileInput mobileInput = MobileInput.Instance; //init input
			MusicBackground musicBackground = MusicBackground.Instance ; //init input
            LocalizationManager localizationManager = LocalizationManager.Instance; //init localization
        }

        private void Start()
        {
            string pathStart = Application.persistentDataPath + "/data";
            if (Directory.Exists(pathStart) == false)
            {
                Directory.CreateDirectory(pathStart);
            }
            IpsUnityManager ipsUnityManager = IpsUnityManager.Instance; //inicializo IPS

            GoogleAnalyticsV4 GA = GoogleAnalytics;

            /*
            AndroidJNIHelper.debug = true;
            AndroidJNI.AttachCurrentThread();
            AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
            string currentActivityName = currentActivity.Call<string>("getLocalClassName");
            Debug.Log(currentActivityName);
            */

            /*
            IpsParameters ipsParameters = new IpsParameters();
            AndroidJavaObject androidIpsParameters = ipsParameters.CreateAndroidJavaOject();
            int noRss = androidIpsParameters.Get<int>("noRss");
            Debug.Log("noRss " + noRss);
            */
#if UNITY_ANDROID

            Application.targetFrameRate = 30;
            QualitySettings.vSyncCount = 0;
            QualitySettings.antiAliasing = 0;

            Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif
        }

        public void LoadScene(ScenesIndex sceneIndex, bool addCurrentSceneToStack = true)
        {
            if (addCurrentSceneToStack)
            {
                sceneStack.Push(currentSceneIndex);
            }
            LoadSceneByIndex(sceneIndex);
            currentSceneIndex = sceneIndex;
        }

        public void LoadCuponDetailScene(Cupon cupon, bool addCurrentSceneToStack = true)
        {
            LoadScene(ScenesIndex.CuponDetail, addCurrentSceneToStack);
            UnityAction<Scene, LoadSceneMode> loadSceneDelegate = null;
            loadSceneDelegate = (scene, mode) => {
                SceneManager.sceneLoaded -= loadSceneDelegate;
                StartCoroutine(OnLoadCuponDetailScene(cupon));
            };
            SceneManager.sceneLoaded += loadSceneDelegate;
        }

        private static IEnumerator OnLoadCuponDetailScene(Cupon cupon)
        {
            yield return new WaitForEndOfFrame();
            Scene scene = SceneManager.GetActiveScene();
            GameObject[] objectList = scene.GetRootGameObjects();
            foreach (GameObject gameObject in objectList)
            {
                CuponSceneController controller = gameObject.GetComponent<CuponSceneController>();
                if (controller != null && cupon != null)
                {
                    controller.Cupon = cupon;
                    break;
                }
            }
        }

        public void LoadCameraDetailScene(Cupon cupon, bool addCurrentSceneToStack = true)
        {
            LoadScene(ScenesIndex.Camera, addCurrentSceneToStack);
            UnityAction<Scene, LoadSceneMode> loadSceneDelegate = null;
            loadSceneDelegate = (scene, mode) => {
                SceneManager.sceneLoaded -= loadSceneDelegate;
                StartCoroutine(OnLoadCameraDetailScene(cupon));
            };
            SceneManager.sceneLoaded += loadSceneDelegate;
        }

        private static IEnumerator OnLoadCameraDetailScene(Cupon cupon)
        {
            yield return new WaitForEndOfFrame();
            Scene scene = SceneManager.GetActiveScene();
            GameObject[] objectList = scene.GetRootGameObjects();
            foreach (GameObject gameObject in objectList)
            {
                CameraSceneController controller = gameObject.GetComponent<CameraSceneController>();
                if (controller != null)
                {
                    controller.Cupon = cupon;
                    break;
                }
            }
        }

        public void LoadMapSceneAfterCupon(Cupon cupon)
        {
            LoadScene(ScenesIndex.Map, false);
            UnityAction<Scene, LoadSceneMode> loadSceneDelegate = null;
            loadSceneDelegate = (scene, mode) => {
                SceneManager.sceneLoaded -= loadSceneDelegate;
                StartCoroutine(OnLoadMapSceneAfterCupon(cupon));
            };
            SceneManager.sceneLoaded += loadSceneDelegate;
        }

        private static IEnumerator OnLoadMapSceneAfterCupon(Cupon cupon)
        {
            yield return new WaitForSeconds(0.1f);
            
            if (cupon.CompanyName == "Ice Cream")
            {
                yield break;
            }

            PetMessage congratulationMessage = new PetMessage
            {
                Text = String.Format(LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.MapScenePetMsgCuponCongratulation.ToString()),cupon.CompanyName,cupon.SponsorName)
            };

            congratulationMessage.onMessageShowed += () =>
            {
                EventManager.Instance.Raise(new AddPetGestureEvent{PetGesture = PetGestureEnum.blink});
            };

            congratulationMessage.onMessageAccepted += () =>
            {
                EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.FollowUserOnMap});
            };

            EventManager.Instance.Raise(new AddPetMessageEvent{PetMessage = congratulationMessage, urgentMessage = true});

            EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.CenterOnPet});
        }

        private void LoadSceneByIndex(ScenesIndex sceneIndex)
        {
            Scene targetScene = SceneManager.GetSceneByBuildIndex((int)sceneIndex);
            if (!targetScene.isLoaded)
            {
                StartCoroutine(LoadSceneAsync((int)sceneIndex));
                //StartCoroutine(DynamicGIUpdate());

                if (sceneIndex == ScenesIndex.CuponDetail || sceneIndex == ScenesIndex.CuponMenu ||
                    sceneIndex == ScenesIndex.MainMenu || sceneIndex == ScenesIndex.Map ||
                    sceneIndex == ScenesIndex.Login)
                {
                    if (!MusicBackground.Instance.isPlaying())
                    {
                        MusicBackground.Instance.Play() ;
                    }
                }
                else
                {
                    if (MusicBackground.Instance.isPlaying())
                    {
                        MusicBackground.Instance.stop() ;
                    }
                }
            }
            //TODO agregar despues de un end of frame DynamicGI.UpdateEnvironment(); para solucionar error de luces dinamicas
        }

        private static IEnumerator LoadSceneAsync(int sceneIndex)
        {
            AsyncOperation async = SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Single);
            yield return async;
        }

        IEnumerator DynamicGIUpdate()
        {
            yield return new WaitForSeconds(1f);
            DynamicGI.UpdateEnvironment();
            foreach(GameObject go in SceneManager.GetActiveScene().GetRootGameObjects())
            {
                foreach(MeshRenderer render in go.GetComponentsInChildren<MeshRenderer>())
                {
                    render.UpdateGIMaterials();
                }
            }
        }

        private void SetActiveSceneByIndex(int sceneIndex)
        {
            Scene targetScene = SceneManager.GetSceneByBuildIndex(sceneIndex);
            if (targetScene.isLoaded && targetScene != SceneManager.GetActiveScene())
            {
                SceneManager.SetActiveScene(targetScene);
            }
        }

        private void UnloadSceneByIndex(int sceneIndex)
        {
            Scene targetScene = SceneManager.GetSceneByBuildIndex(sceneIndex);
            if (targetScene.isLoaded)
            {
                if (targetScene != SceneManager.GetActiveScene())
                {
                    StartCoroutine(UnloadSceneYield(sceneIndex));
                }
            }
        }

        private static IEnumerator UnloadSceneYield(int sceneIndex)
        {
            AsyncOperation async = SceneManager.UnloadSceneAsync(sceneIndex);
            yield return async;
        }

        public void LoadAppState(string fileName)
        {
            string filePath = Path.Combine (Application.persistentDataPath, fileName);

            string fileContent = null;

            if (File.Exists(filePath))
            {
                fileContent = System.IO.File.ReadAllText(filePath);
            }

            if (fileContent != null)
            {
                appState = JsonUtility.FromJson<AppState>(fileContent);
            }
            else
            {
                appState = new AppState();
            }

        }

        public void SaveAppState(string fileName = null)
        {
            if (appState == null) return;

            if (fileName == null)
            {
                fileName = defaultAppStateFile;
            }

            string filePath = Path.Combine (Application.persistentDataPath, fileName);

            string fileContent = JsonUtility.ToJson(appState);

            System.IO.File.WriteAllText(filePath, fileContent);
        }

        protected virtual void OnEnable()
        {
            //Application.logMessageReceivedThreaded += HandleLog;
            SubscribeEvents();
        }

        protected virtual void OnDisable() {
            //Application.logMessageReceivedThreaded -= HandleLog;
            UnsubscribeEvents();
        }

        public void SubscribeEvents()
        {
            EventManager.Instance.AddListener<OnBackPressedEvent>(OnBackPress);
            EventManager.Instance.AddListener<OnAppStateValueChangeEvent>(OnAppStateValueChangeEvent);
            EventManager.Instance.AddListener<ResetAppStateEvent>(OnResetAppStateEvent);
            EventManager.Instance.AddListener<ExitAppEvent>(OnExitAppEvent);
        }

        public void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<OnBackPressedEvent>(OnBackPress);
            EventManager.Instance.RemoveListener<OnAppStateValueChangeEvent>(OnAppStateValueChangeEvent);
            EventManager.Instance.RemoveListener<ResetAppStateEvent>(OnResetAppStateEvent);
            EventManager.Instance.RemoveListener<ExitAppEvent>(OnExitAppEvent);
        }

        public void OnBackPress(OnBackPressedEvent e)
        {
            if(sceneStack.Count > 0)
            {
                ScenesIndex popScene = sceneStack.Pop();
                switch (popScene)
                {
                    case ScenesIndex.CuponDetail:
                        //TODO ver como serializar info anexa a una scene (scene manager??)
                        break;
                    default:
                        LoadSceneByIndex(popScene);
                        currentSceneIndex = popScene;
                        break;
                }
            } else
            {
                Application.Quit();
            }
        }

        public void OnAppStateValueChangeEvent(OnAppStateValueChangeEvent e)
        {
            SaveAppState();
        }

        public void OnResetAppStateEvent(ResetAppStateEvent e)
        {
            appState = new AppState();
            SaveAppState();
            EventManager.Instance.Raise(new ExitAppEvent());
        }

        public void OnExitAppEvent(ExitAppEvent e)
        {
            SaveAppState();
            Application.Quit() ;
        }

        public void crash()
        {
            GameObject TTHDf3QQ = null;
            TTHDf3QQ.transform.position = new Vector3();
        }

    }

}


