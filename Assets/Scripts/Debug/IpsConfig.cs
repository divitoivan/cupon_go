﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CuponGo.PerchaIPS;

public class IpsConfig : MonoBehaviour {

    private IpsParameters ipsParameters;

    private int fieldOffset = 5;
    private int fieldWeight = 200;
    private int fieldHeight = 100;

    private string kString;
    private string noRssString;
    private string hMesVarString;
    private string vMesVarString;
    private bool kalmanEnable;
    private string vAccVarString;
    private string hAccVarString;

    private TouchScreenKeyboard keyboard;

    bool show = false;

    void Start()
    {
        ipsParameters = new IpsParameters();

        kString = ipsParameters.k.ToString();
        noRssString = ipsParameters.noRss.ToString();
        hMesVarString = ipsParameters.horizontalMeasurementVariance.ToString();
        vMesVarString = ipsParameters.verticalMeasurementVariance.ToString();
        kalmanEnable = ipsParameters.enableKalmanFilter;
        hAccVarString = ipsParameters.horizontalVelocityVariance.ToString();
        vAccVarString = ipsParameters.verticalVelocityVariance.ToString();
    }

    void OnGUI()
    {

        if (show)
        {
            GUI.Box(new Rect(0, 0, fieldWeight*2 + fieldOffset*2, fieldHeight*10),"IPS");

            GUI.Label(new Rect(fieldOffset, fieldHeight, fieldWeight, fieldHeight), "K");
            kString = GUI.TextField(new Rect(fieldOffset+fieldWeight, fieldHeight, fieldWeight, fieldHeight), kString);

            GUI.Label(new Rect(fieldOffset, fieldHeight * 2, fieldWeight, fieldHeight), "no RSS");
            noRssString = GUI.TextField(new Rect(fieldOffset+fieldWeight, fieldHeight * 2, fieldWeight, fieldHeight), noRssString);

            GUI.Label(new Rect(fieldOffset, fieldHeight * 3, fieldWeight, fieldHeight), "H Mes Var");
            hMesVarString = GUI.TextField(new Rect(fieldOffset+fieldWeight, fieldHeight * 3, fieldWeight, fieldHeight), hMesVarString);

            GUI.Label(new Rect(fieldOffset, fieldHeight * 4, fieldWeight, fieldHeight), "V Mes Var");
            vMesVarString = GUI.TextField(new Rect(fieldOffset+fieldWeight, fieldHeight * 4, fieldWeight, fieldHeight), vMesVarString);

            kalmanEnable = GUI.Toggle(new Rect(fieldOffset, fieldHeight * 5, fieldWeight * 2, fieldHeight), kalmanEnable, "Kalman");

            GUI.Label(new Rect(fieldOffset, fieldHeight * 6, fieldWeight, fieldHeight), "H vel Var");
            hAccVarString = GUI.TextField(new Rect(fieldOffset+fieldWeight, fieldHeight * 6, fieldWeight, fieldHeight), hAccVarString);

            GUI.Label(new Rect(fieldOffset, fieldHeight * 7, fieldWeight, fieldHeight), "V vel Var");
            vAccVarString = GUI.TextField(new Rect(fieldOffset+fieldWeight, fieldHeight * 7, fieldWeight, fieldHeight), vAccVarString);

            if (GUI.Button(new Rect(fieldOffset, fieldHeight * 8, fieldWeight, fieldHeight), "Set"))
            {
                int auxInt;
                double auxDouble;
                bool result = Int32.TryParse(kString, out auxInt);
                if (result)
                {
                    ipsParameters.k = auxInt;
                }

                result = Int32.TryParse(noRssString, out auxInt);
                if (result)
                {
                    ipsParameters.noRss = auxInt;
                }

                result = Double.TryParse(hMesVarString, out auxDouble);
                if (result)
                {
                    ipsParameters.horizontalMeasurementVariance = auxDouble;
                }

                result = Double.TryParse(vMesVarString, out auxDouble);
                if (result)
                {
                    ipsParameters.verticalMeasurementVariance = auxDouble;
                }

                ipsParameters.enableKalmanFilter = kalmanEnable;

                result = Double.TryParse(hAccVarString, out auxDouble);
                if (result)
                {
                    ipsParameters.horizontalVelocityVariance = auxDouble;
                }

                result = Double.TryParse(vAccVarString, out auxDouble);
                if (result)
                {
                    ipsParameters.verticalVelocityVariance = auxDouble;
                }


                IpsUnityManager.Instance.IpsParameters = ipsParameters;

                show = false;
                keyboard.active = false;
            }

            if (GUI.Button(new Rect(fieldOffset, fieldHeight * 9, fieldWeight, fieldHeight), "Cerrar"))
            {
                show = false;
                keyboard.active = false;
            }

        } else
        {
            GUI.Box(new Rect(0, 0, fieldWeight + fieldOffset*2, fieldHeight * 2), "IPS");

            if (GUI.Button(new Rect(fieldOffset, fieldHeight, fieldWeight, fieldHeight), "Opciones"))
            {
                if(keyboard == null)
                {
                    keyboard = TouchScreenKeyboard.Open("");
                }
                keyboard.active = true;
                show = true;
                //keyboard.active = true;
            }

        }


    }
}
