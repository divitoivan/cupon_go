﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CuponGo;

public class MapConfig : MonoBehaviour {

    [SerializeField]
    private MapLevelSelector MapLevelSelector;
	[SerializeField]
	private MapCameraController mapCameraController;

    private int fieldOffset = 5;
    private int fieldWeight = 200;
    private int fieldHeight = 100;

    private string petScaleString;
    private bool forceLevel;
    private string currentLevelString;
    private string moveVelString;
    private string bearingRotationThreshold;
	private string walkDistanceThreshold;
	private string lockSpeed;
	private string tiltSpeed;

    private TouchScreenKeyboard keyboard;

    private PetMove PetMove;
    private Transform PetTransform;

    bool show = false;

    private void Awake()
    {
        GameObject go = GameObject.FindGameObjectWithTag(TagsEnum.player.ToString());
        if (go != null)
        {
            PetTransform = go.transform;
            PetMove = go.GetComponent<PetMove>();
        }
    }

    void Start () {

        petScaleString = PetTransform.transform.localScale.x.ToString();
        forceLevel = MapLevelSelector.ForceLevel;
        currentLevelString =  MapLevelSelector.CurrentLevel.ToString();
        moveVelString = PetMove.MoveVel.ToString();
        bearingRotationThreshold = PetMove.UseBearingRotationThreshold.ToString();
		walkDistanceThreshold = PetMove.WalkDistanceThreshold.ToString();
		lockSpeed = mapCameraController.HorizontalRotationScaler.ToString();
		tiltSpeed = mapCameraController.VerticalRotationScaler.ToString();

    }

    void OnGUI()
    {

        if (show)
        {
            GUI.Box(new Rect(Screen.width - (fieldWeight * 2 + fieldOffset * 2), 0, fieldWeight * 2 + fieldOffset * 2, fieldHeight * 10), "Map");

            GUI.Label(new Rect(Screen.width - (fieldOffset + fieldWeight * 2), fieldHeight, fieldWeight, fieldHeight), "Pet Scale");
            petScaleString = GUI.TextField(new Rect(Screen.width - (fieldOffset + fieldWeight), fieldHeight, fieldWeight, fieldHeight), petScaleString);

            forceLevel = GUI.Toggle(new Rect(Screen.width - (fieldOffset + fieldWeight * 2), fieldHeight * 2, fieldWeight, fieldHeight), forceLevel, "Force Level");
            currentLevelString = GUI.TextField(new Rect(Screen.width - (fieldOffset + fieldWeight), fieldHeight *2, fieldWeight, fieldHeight), currentLevelString);

            GUI.Label(new Rect(Screen.width - (fieldOffset + fieldWeight * 2), fieldHeight * 3, fieldWeight, fieldHeight), "move vel");
            moveVelString = GUI.TextField(new Rect(Screen.width - (fieldOffset + fieldWeight), fieldHeight * 3, fieldWeight, fieldHeight), moveVelString);

            GUI.Label(new Rect(Screen.width - (fieldOffset + fieldWeight * 2), fieldHeight * 4, fieldWeight, fieldHeight), "bearing thr");
            bearingRotationThreshold = GUI.TextField(new Rect(Screen.width - (fieldOffset + fieldWeight), fieldHeight * 4, fieldWeight, fieldHeight), bearingRotationThreshold);

            GUI.Label(new Rect(Screen.width - (fieldOffset + fieldWeight * 2), fieldHeight * 5, fieldWeight, fieldHeight), "walk thr");
            walkDistanceThreshold = GUI.TextField(new Rect(Screen.width - (fieldOffset + fieldWeight), fieldHeight * 5, fieldWeight, fieldHeight), walkDistanceThreshold);

			GUI.Label(new Rect(Screen.width - (fieldOffset + fieldWeight * 2), fieldHeight * 6, fieldWeight, fieldHeight), "lock speed");
			lockSpeed = GUI.TextField(new Rect(Screen.width - (fieldOffset + fieldWeight), fieldHeight * 6, fieldWeight, fieldHeight), lockSpeed);

			GUI.Label(new Rect(Screen.width - (fieldOffset + fieldWeight * 2), fieldHeight * 7, fieldWeight, fieldHeight), "tilt speed");
			tiltSpeed = GUI.TextField(new Rect(Screen.width - (fieldOffset + fieldWeight), fieldHeight * 7, fieldWeight, fieldHeight), tiltSpeed);

            /*
            GUI.Label(new Rect(fieldOffset, fieldHeight * 2, fieldWeight, fieldHeight), "no RSS");
            noRssString = GUI.TextField(new Rect(fieldOffset + fieldWeight, fieldHeight * 2, fieldWeight, fieldHeight), noRssString);

            GUI.Label(new Rect(fieldOffset, fieldHeight * 3, fieldWeight, fieldHeight), "H Mes Var");
            hMesVarString = GUI.TextField(new Rect(fieldOffset + fieldWeight, fieldHeight * 3, fieldWeight, fieldHeight), hMesVarString);

            GUI.Label(new Rect(fieldOffset, fieldHeight * 4, fieldWeight, fieldHeight), "V Mes Var");
            vMesVarString = GUI.TextField(new Rect(fieldOffset + fieldWeight, fieldHeight * 4, fieldWeight, fieldHeight), vMesVarString);

            kalmanEnable = GUI.Toggle(new Rect(fieldOffset, fieldHeight * 5, fieldWeight * 2, fieldHeight), kalmanEnable, "Kalman");

            GUI.Label(new Rect(fieldOffset, fieldHeight * 6, fieldWeight, fieldHeight), "H acc Var");
            hAccVarString = GUI.TextField(new Rect(fieldOffset + fieldWeight, fieldHeight * 6, fieldWeight, fieldHeight), hAccVarString);

            GUI.Label(new Rect(fieldOffset, fieldHeight * 7, fieldWeight, fieldHeight), "V acc Var");
            vAccVarString = GUI.TextField(new Rect(fieldOffset + fieldWeight, fieldHeight * 7, fieldWeight, fieldHeight), vAccVarString);
            */
            if (GUI.Button(new Rect(Screen.width - (fieldOffset + fieldWeight *2), fieldHeight * 8, fieldWeight, fieldHeight), "Set"))
            {

                int auxInt;
                float auxFloat;
                bool result = float.TryParse(petScaleString, out auxFloat);
                if (result)
                {
                    Vector3 scale = new Vector3(auxFloat, auxFloat, auxFloat);
                    PetTransform.transform.localScale = scale;
                }

                MapLevelSelector.ForceLevel = forceLevel;

                result = Int32.TryParse(currentLevelString, out auxInt);
                if (result)
                {
                    MapLevelSelector.SetLevel(auxInt, true);
                }

                result = float.TryParse(moveVelString, out auxFloat);
                if (result)
                {
                    PetMove.MoveVel = auxFloat;
                }

                result = float.TryParse(bearingRotationThreshold, out auxFloat);
                if (result)
                {
                    PetMove.UseBearingRotationThreshold = auxFloat;
                }

                result = float.TryParse(walkDistanceThreshold, out auxFloat);
                if (result)
                {
                    PetMove.WalkDistanceThreshold = auxFloat;
                }

				result = float.TryParse(lockSpeed, out auxFloat);
				if (result)
				{
					mapCameraController.HorizontalRotationScaler = auxFloat;
				}

				result = float.TryParse(tiltSpeed, out auxFloat);
				if (result)
				{
					mapCameraController.VerticalRotationScaler = auxFloat;
				}

                show = false;
                keyboard.active = false;
            }

            if (GUI.Button(new Rect(Screen.width - (fieldOffset + fieldWeight * 2), fieldHeight * 9, fieldWeight, fieldHeight), "Cerrar"))
            {
                show = false;
                keyboard.active = false;
            }


        }
        else
        {
            GUI.Box(new Rect(Screen.width - (fieldWeight + fieldOffset * 2), 0, fieldWeight + fieldOffset * 2, fieldHeight * 2), "Map");

            if (GUI.Button(new Rect(Screen.width - (fieldOffset + fieldWeight), fieldHeight, fieldWeight, fieldHeight), "Opciones"))
            {
                if (keyboard == null)
                {
                    keyboard = TouchScreenKeyboard.Open("");
                }
                keyboard.active = true;
                show = true;
                //keyboard.active = true;
            }

        }


    }
}
