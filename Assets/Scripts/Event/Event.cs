using CuponGo.PerchaIPS;
using UnityEngine;
using Vuforia;

namespace CuponGo.Events {

    /// <summary>
    /// Base event for all EventManager events.
    /// </summary>
    public class Event {
    }

    //input
    public class OnTouchClickEvent : Event
    {
        public GameObject ClickedObject { get; set; }
        public Vector3 ClickedPoint { get; set; }
    }

    public class OnBackPressedEvent : Event
    {
    }

    //drag and drop
    public class OnDragCuponEvent : Event
    {
        public GameObject DragObject { get; set; }
        public Cupon Cupon { get; set; }
        public bool DragBegin { get; set; }
    }

    public class OnDropCuponOnBasketEvent : Event
    {
        public GameObject DropConteinerObject { get; set; }
        public GameObject DropObject { get; set; }
    }

    //map scene callback
    public class OnMainCharacterMoveEvent : Event
    {
        public Transform CharacterTransform { get; set; }
        public Vector3 MoveDest { get; set; }
        public float MoveVel { get; set; }
    }

    public class OnSceneMapModeChangeEvent : Event
    {
        public SceneMode PrevMode { get; set; }
        public SceneMode CurrentMode { get; set; }
    }

    public class OnCuponAppearsEvent : Event
    {
        public GameObject CuponGameObject { get; set; }
        public Cupon Cupon { get; set; }
    }

    public class OnPetGestureChangeEvent : Event
    {
        public PetGestureEnum CurrentPetGesture { get; set; }
    }

    public class OnChangeMapLevelEvent : Event
    {
        public int PrevLevel{ get; set; }
        public int CurrentLevel { get; set; }
    }

    public class OnUpdatePositionEstimation : Event
    {
        public PositionEstimation PositionEstimation { get; set; }
    }

    public class OnAddPetMessageEvent: Event
    {
    }

    public class OnAcceptPetMessageEvent: Event
    {
        public PetMessage petMessage { get; set; }
    }

    public class OnMainCharacterChangeEvent : Event
    {
        public Transform NewCharacterTransform { get; set; }
    }

    public class OnAppStateValueChangeEvent : Event
    {
    }

    public class OnInitialPetMessageShowedChangeEvent : OnAppStateValueChangeEvent
    {
    }

    public class OnCurrentCharacterChangeEvent : OnAppStateValueChangeEvent
    {
    }

    public class OnCatchCuponMessageShowed : OnAppStateValueChangeEvent
    {
    }

    public class OnEnableIPSChangeEvent : OnAppStateValueChangeEvent
    {
    }

    //map scene actions

    public class ChangeMapLevelEvent : Event
    {
        public ChangeMapLevelEvent(int newLevel)
        {
            NewLevel = newLevel ;
            ForceChange = false;
        }

        public int NewLevel { get; set; }
        public bool ForceChange { get; set; }
    }

    public class ChangeMapSceneModeEvent : Event
    {
        public SceneMode newSceneMode { get; set; }
    }

    public class AddPetMessageEvent : Event
    {
        public PetMessage PetMessage{ get; set; }
        public bool urgentMessage{ get; set; }
    }

    public class AcceptPetMessageEvent : Event
    {
    }

    public class AddPetGestureEvent : Event
    {
        public PetGestureEnum PetGesture{ get; set; }
    }

    public class AddPetGesturePointingEvent : AddPetGestureEvent
    {
        public AddPetGesturePointingEvent(GameObject pointingGameObject, float rotationVel = -1)
        {
            PetGesture = PetGestureEnum.point;
            PointingGameObject = pointingGameObject;
            RotationVel = rotationVel;
        }

        public GameObject PointingGameObject { get; set; }
        public float RotationVel { get; set; }
    }

    public class GoToStoreEvent : Event
    {
        public string StoreName{ get; set; }
    }

    public class MovePetEvent : Event
    {
        public MovePetEvent(bool followMainCharacter)
        {
            FollowMainCharacter = followMainCharacter;
        }

        public MovePetEvent(Vector3 dest)
        {
            FollowMainCharacter = false;
            Dest = dest;
        }

        public bool FollowMainCharacter { get; private set; }
        public Vector3 Dest{ get; private set; }
    }

    public class MoveMainCharacterEvent : Event
    {
        private float _moveVel = -1f;
        
        public Vector3 Dest{ get; set; }

        public float MoveVel
        {
            get { return _moveVel; }
            set { _moveVel = value; }
        }
    }

    public class CameraActivateRotationEvent : Event
    {
        public bool activateRotation{ get; set; }
    }

    public class CameraCenterOnObjectEvent : Event
    {
        public GameObject CenterGameObject{ get; set; }
    }

    public class MainCharacterChangeEvent : Event
    {
        public Transform NewCharacterTransform { get; set; }
    }

    public class MainCharacterChangeGenderEvent : Event
    {
        public bool characterIsMale { get; set; }
    }

    public class ResetAppStateEvent : Event
    {
    }

    public class ExitAppEvent : Event
    {
    }

    public class OnNewCuponEvent : Event
    {
        public Cupon Cupon { get; set; }
    }
    
    /*//AR
    
    public class OnCherImageTrackableStateChanged : Event
    {
        public CherImageIndex ImageIndex { get; set; }
        public TrackableBehaviour.Status newStatus { get; set; }
    }
    
    public class ShowCherArProduct : Event
    {
        public CherImageIndex ImageIndex { get; set; }
        public bool Show { get; set; }
    }*/
    
    public class OnProductBubbleClicked : Event
    {
    }
    
    public class OnProductImageConteinerShow : Event
    {
        public bool Show { get; set; }
    }

    //DEBUG

    public class SetGoToPositionEvent : Event
    {
    }
}
