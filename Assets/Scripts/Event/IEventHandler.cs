using UnityEngine;

namespace CuponGo.Events {

    /// <summary>
    ///  Interface for event handlers
    /// </summary>
    public interface IEventHandler {

        /// <summary>
        /// Subscribe to events
        ///
        /// @example
        ///   Events.AddListener<MoveResolvedEvent>(OnMoveResolved);
        ///     or
        ///   EventManager.OnSetRule += OnSetRule;
        /// </summary>
        void SubscribeEvents();

        /// <summary>
        /// Unsubscribe from events
        ///
        /// @example
        ///   Events.RemoveListener<MoveResolvedEvent>(OnMoveResolved);
        ///     or
        ///   EventManager.OnSetRule -= OnSetRule;
        /// </summary>
        void UnsubscribeEvents();

    }

    /// <summary>
    /// Event handler
    /// </summary>
    public abstract class MonoBehaviourEventHandler : MonoBehaviour, IEventHandler {

        /// <summary>
        /// Subscribe to events
        ///
        /// @example
        ///   EventManager.Instance.AddListener<MoveResolvedEvent>(OnMoveResolved);
        /// </summary>
        public abstract void SubscribeEvents();

        /// <summary>
        /// Unsubscribe from events
        ///
        /// @example
        ///   EventManager.Instance.RemoveListener<MoveResolvedEvent>(OnMoveResolved);
        /// </summary>
        public abstract void UnsubscribeEvents();

        protected virtual void OnEnable() {
            SubscribeEvents();
        }

        protected virtual void OnDisable() {
            UnsubscribeEvents();
        }

    }
}
