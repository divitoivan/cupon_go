﻿using CuponGo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuctionController : MonoBehaviour
{
    public static Texture2D background;
    public static string code;

    private Texture2D _product;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnGUI()
    {
        var w = Screen.width;
        var h = Screen.height;

        if (background != null)
        {
            GUI.DrawTexture(new Rect(0f, 0f, w, h), background, ScaleMode.ScaleToFit);
        }

        if (code == null)
        {
            code = "1";
        }

        var st = new GUIStyle();
        st.alignment = TextAnchor.UpperCenter;
        st.fontSize = h / 40;
        st.normal.textColor = Color.green;
        st.wordWrap = true;

        var m = w / 10f;
        var w2 = w - (2f * m);
        var m2 = m / 2f;

        //GUI.Label(new Rect(m, m, w - (2 * m), h - m), "Subasta de un par de zapatos según la imagen. Por favor ingresa tu precio de compra, ganará quien subaste el mayor precio. La subasta cierra en 120 minutos.", st);

        //GUILayout.BeginScrollView(new Vector2(0f, 0f));

        //GUILayout.BeginHorizontal();
        //GUILayout.Space(m);
        st.padding = new RectOffset(0, 0, 0, 0);

        GUILayout.BeginArea(new Rect(m - 5f, m - 5f, w2 + 10f, h - (2f * m) + 10f), st);

        GUILayout.BeginVertical(GUILayout.Width(w2));
        GUILayout.Space(m);
        GUILayout.Label("Subasta de un par de zapatos según la imagen. Por favor ingresa tu precio de compra, ganará quien subaste el mayor precio.", st);

        if (this._product == null)
        {
            this._product = Resources.Load<Texture2D>("sale_" + code);
        }

        var st2 = new GUIStyle();
        st2.alignment = TextAnchor.MiddleCenter;
        st2.imagePosition = ImagePosition.ImageOnly;
        st2.stretchHeight = true;
        st2.stretchWidth = true;

        GUILayout.Space(m2);
        GUILayout.Box(this._product, st2, GUILayout.Width(w2), GUILayout.Height(w2), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

        GUILayout.Space(m2);

        GUILayout.Label("Última oferta: $300", st);

        GUILayout.Space(m2);
        st.alignment = TextAnchor.MiddleLeft;
        GUILayout.Label("Ingresa tu oferta:", st);

        GUILayout.BeginHorizontal();
        var st3 = GUI.skin.textField;
        st3.fontSize = st.fontSize;
        st3.alignment = TextAnchor.MiddleRight;

        GUILayout.TextField("310", st3, GUILayout.Width(w2 - m), GUILayout.Height(m));
        if (GUILayout.Button("$", GUILayout.Width(m), GUILayout.Height(m)))
        {
            CuponGoManager.Instance.OnBackPress(new CuponGo.Events.OnBackPressedEvent());
        }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
        GUILayout.EndArea();
        //GUILayout.EndHorizontal();
        //GUILayout.EndScrollView();
    }

    private void OnDestroy()
    {
        if (background != null)
        {
            DestroyImmediate(background);
            background = null;
        }

        if (this._product != null)
        {
            //Destroy(this._product);
            Resources.UnloadAsset(this._product);
        }
    }
}
