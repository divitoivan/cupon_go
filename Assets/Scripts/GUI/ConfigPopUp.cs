﻿using System;
using System.Collections;
using System.Collections.Generic;
using CuponGo;
using CuponGo.Events;
using UnityEngine;
using UnityEngine.UI;

public class ConfigPopUp : Popup
{
    [SerializeField] private CuponListScrollControler addCuponController;
    [SerializeField] private GameObject IpsConfigPopupPrefab;

    public CuponListScrollControler AddCuponController
    {
        get { return addCuponController; }
    }

    public void SetCuponList(List<Cupon> cuponList)
    {

        if (addCuponController != null)
        {
            foreach (var cupon in cuponList)
            {
                addCuponController.CuponList.Add(cupon);
            }
        }
    }

    public void OnCelebrateButtonClick()
    {
        float delay = 5f;
        StartCoroutine(StartCelebrateEventDelayed(delay)) ;
        Close(delay+1f);
    }

    public void OnChangeCharacterButtonClick()
    {
        EventManager.Instance.Raise(new MainCharacterChangeGenderEvent{characterIsMale = !CuponGoManager.Instance.AppState.CurrentCharacterIsMale});
        Close();
    }

    public void OnResetAppStateClick()
    {
        EventManager.Instance.Raise(new ResetAppStateEvent());
        Close();
    }

    public void OnNewAuctionClick()
    {

        float delay = 5f;
        StartCoroutine(StartNewAuctionEventDelayed(delay)) ;
        Close(delay+1f) ;
    }

    public void OnWinAuctionClick()
    {
        float delay = 5f;
        StartCoroutine(StartWinAuctionEventDelayed(delay)) ;
        Close(delay+1f) ;
    }

    public void OnSetGoToStoreDestClick()
    {
        EventManager.Instance.Raise(new SetGoToPositionEvent());
        Close() ;
    }

    public void OnEnableIPSClick()
    {
        GameObject popup = Instantiate(IpsConfigPopupPrefab);
        popup.SetActive(true);
        popup.transform.localScale = Vector3.zero;
        popup.transform.SetParent(canvas.transform, false);
    }

    public void OnChangeFloorClick()
    {
        EventManager.Instance.Raise(new ChangeMapLevelEvent(-1));
        Close();
    }
    
    private IEnumerator StartCelebrateEventDelayed(float delay)
    {
        yield return new WaitForSeconds(delay);

        PetMessage celebrateMessage = new PetMessage
        {
            Text = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.MapScenePetMsgCelebrate.ToString())
        };

        celebrateMessage.onMessageShowed += () =>
        {
            EventManager.Instance.Raise(new AddPetGestureEvent{PetGesture = PetGestureEnum.celebrate});
        };

        celebrateMessage.onMessageAccepted += () =>
        {
            EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.FollowUserOnMap});
        };

        EventManager.Instance.Raise(new AddPetMessageEvent{PetMessage = celebrateMessage, urgentMessage = true});

        EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.CenterOnPet});

    }

    private IEnumerator StartNewAuctionEventDelayed(float delay)
    {
        yield return new WaitForSeconds(delay);

        PetMessage newAuctionMessage = new PetMessage
        {
            Text = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.MapScenePetMsgNewAuction.ToString())
        };

        newAuctionMessage.onMessageShowed += () =>
        {
            EventManager.Instance.Raise(new AddPetGestureEvent{PetGesture = PetGestureEnum.ok}) ;
        };
        newAuctionMessage.onMessageAccepted += () =>
        {
            EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.FollowUserOnMap});
        };

        EventManager.Instance.Raise(new AddPetMessageEvent{PetMessage = newAuctionMessage, urgentMessage = true});

    }

    private IEnumerator StartWinAuctionEventDelayed(float delay)
    {
        yield return new WaitForSeconds(delay);

        PetMessage newAuctionMessage = new PetMessage
        {
            Text = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.MapScenePetMsgWinAuction.ToString())
        };

        newAuctionMessage.onMessageShowed += () =>
        {
            EventManager.Instance.Raise(new AddPetGestureEvent{PetGesture = PetGestureEnum.clamp});
        };
        newAuctionMessage.onMessageAccepted += () =>
        {
            EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.FollowUserOnMap});
        };

        EventManager.Instance.Raise(new AddPetMessageEvent{PetMessage = newAuctionMessage, urgentMessage = true}) ;

    }
}
