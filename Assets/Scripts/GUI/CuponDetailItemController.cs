﻿using UnityEngine;
using UnityEngine.UI;
using CuponGo;

public class CuponDetailItemController : MonoBehaviour {


    private Cupon cupon;
	[SerializeField]
    private Text cuponDescription;
	[SerializeField]
    private Image companyImage;

    public Cupon Cupon
    {
        get
        {
            return cupon;
        }
        set
        {
            cupon = value;
            FillGUI();
        }
    }

    void Start () {
        //Transform text = transform.Find("Text");
        //cuponDescription = text.GetComponent<Text>();
        //Transform image = transform.Find("Image");
        //companyImage = image.GetComponent<Image>();

        if (Cupon != null)
        {
            FillGUI();
        }
    }

	void Update () {

	}

    private void FillGUI()
    {
        if (cuponDescription != null)
        {
            cuponDescription.text = Cupon.CuponDescription;
        }
        if (companyImage != null)
        {
            StartCoroutine(Utils.loadImage(Cupon.ImageUrl, companyImage,0));
        }
    }

}
