﻿using UnityEngine;
using UnityEngine.UI;
using Ricimi;
using CuponGo;

public class CuponListItemController : MonoBehaviour {

    public delegate void CuponListItemOnClick(GameObject sender, Cupon cupon);
    public event CuponListItemOnClick onClickEvent;

    private Cupon cupon;
    private Text companyName;
    private Image companyImage;

    public Cupon Cupon
    {
        get
        {
            return cupon;
        }
        set
        {
            cupon = value;
            FillGUI();
        }
    }

    private void Start ()
    {
        companyName = GetComponentInChildren<Text>();
        Transform image = transform.Find("Image");
        companyImage = image.GetComponent<Image>();

        if (Cupon != null)
        {
            FillGUI();
        }

        BasicButton basicButton = GetComponent<BasicButton>();
        if (basicButton != null)
        {
            basicButton.OnClicked.AddListener(onClick);
        }
    }

    private void FillGUI()
    {
        if (companyName != null)
        {
            companyName.text = Cupon.CompanyName;
        }
        if (companyImage != null)
        {
            StartCoroutine(CuponGo.Utils.loadImage(Cupon.ImageUrl, companyImage));
        }
    }

    public void onClick()
    {
        if (onClickEvent != null)
        {
            onClickEvent(transform.gameObject, Cupon);
        }
    }
}
