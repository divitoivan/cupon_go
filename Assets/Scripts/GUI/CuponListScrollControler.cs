﻿using UnityEngine;

public class CuponListScrollControler : MonoBehaviour {

    [SerializeField] private GameObject cuponListConteiner;
    [SerializeField] private GameObject cuponListItemPrefab;

    private ObservableList<Cupon> cuponList;

    public event CuponListItemController.CuponListItemOnClick onCuponListItemClickEvent;

    public ObservableList<Cupon> CuponList
    {
        get
        {
            if (cuponList == null)
            {
                cuponList = new ObservableList<Cupon>();
                cuponList.ItemAdded += onCuponListItemAdded;
                cuponList.ItemRemoved += onCuponListItemRemoved;
                cuponList.ListCleared += onCuponListCleared;
            }
            return cuponList;
        }
    }

    public void onCuponItemClicked(GameObject sender, Cupon cupon)
    {
        if (onCuponListItemClickEvent != null)
        {
            onCuponListItemClickEvent(transform.gameObject, cupon);
        }
    }

    //TODO agregar incertar en cualquier ubicacion
    void onCuponListItemAdded(object sender, Cupon updatedValue)
    {
        GameObject item = Instantiate(cuponListItemPrefab);
        item.transform.SetParent(cuponListConteiner.transform);
        item.GetComponent<RectTransform>().localScale = Vector3.one;
        CuponListItemController controller = item.GetComponent<CuponListItemController>();
        controller.Cupon = updatedValue;
        controller.onClickEvent += onCuponItemClicked;
    }

    void onCuponListItemRemoved(object sender, Cupon updatedValue)
    {
        foreach(GameObject gameObject in transform)
        {
            CuponListItemController controller = gameObject.GetComponent<CuponListItemController>();
            if(controller != null && updatedValue == controller.Cupon)
            {
                Destroy(gameObject);
            }
        }
    }

    void onCuponListCleared(object sender)
    {
        foreach (GameObject gameObject in transform)
        {
            CuponListItemController controller = gameObject.GetComponent<CuponListItemController>();
            if (controller != null)
            {
                Destroy(gameObject);
            }
        }
    }
}
