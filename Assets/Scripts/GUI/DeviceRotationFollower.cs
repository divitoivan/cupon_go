// prefrontal cortex -- http://prefrontalcortex.de
// Full Android Sensor Access for Unity3D
// Contact:
// 		contact@prefrontalcortex.de

using UnityEngine;

public class DeviceRotationFollower : MonoBehaviour
{
    private void Start () {

		SensorHelper.ActivateRotation();

	}

    private void Update ()
    {
        transform.rotation = SensorHelper.rotation;
    }
    /*
    void OnGUI () {
        GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = 40;
        string text = "null "+SensorHelper.rotation.eulerAngles;
        if (Sensor.Get(Sensor.Type.RotationVector).active)
        {
            text = "RotationVector "+SensorHelper.rotation.eulerAngles;
        } else if(Sensor.Get(Sensor.Type.Orientation).active)
        {
            text = "Orientation "+Sensor.orientation;
        } else if (Sensor.Get(Sensor.Type.MagneticField).active && Sensor.Get(Sensor.Type.Accelerometer).active)
        {
            text = "MagneticField "+SensorHelper.rotation.eulerAngles;
        }
        GUI.Box (new Rect (10,10,700,80), new GUIContent(text));

        if (GUI.Button(new Rect(10, 100, 700, 80), "RotationVector"))
        {
            if (!Sensor.Get(Sensor.Type.RotationVector).active)
            {
                SensorHelper.DeactivateRotation();
                SensorHelper.ActivateRotation(RotationFallbackType.RotationQuaternion);
            }

        }
        
        if (GUI.Button(new Rect(10, 200, 700, 80), "Orientation"))
        {
            if (!Sensor.Get(Sensor.Type.Orientation).active)
            {
                SensorHelper.DeactivateRotation();
                SensorHelper.ActivateRotation(RotationFallbackType.OrientationAndAcceleration);
            }

        }
    }*/

}