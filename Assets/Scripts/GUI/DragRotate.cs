﻿using System.Collections;
using System.Collections.Generic;
using CuponGo;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class DragRotate : MonoBehaviour
{

    [SerializeField] private float m_HorizontalRotationScaler = 180f;

    [SerializeField] private float rotationBias = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	    float horizontalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragHorizontalAxisName);

	    rotationBias += horizontalMove * m_HorizontalRotationScaler;

	    transform.rotation = Quaternion.Euler(0f, rotationBias, 0f) ;

	}
}
