﻿using System.Collections;
using System.Collections.Generic;
using CuponGo;
using CuponGo.Events;
using UnityEngine;
using UnityEngine.UI;

public class GoToStorePopUp : Popup
{
    [SerializeField] private InputField storeNameInputField;

    private void Start()
    {
        SetDefaultStoreText();
    }

    public void OnEditStoreNameFinish(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            SetDefaultStoreText();
        }
    }

    public void OnEditStoreName(string value)
    {

    }

    public void OnSearchButtonClicked()
    {

        EventManager.Instance.Raise(new GoToStoreEvent{StoreName = storeNameInputField.text});
        Close();

    }

    private void SetDefaultStoreText()
    {
        if(storeNameInputField == null) return;

        storeNameInputField.text = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.MapSceneGoToStorePopupDefaultStoreName.ToString());

    }
}
