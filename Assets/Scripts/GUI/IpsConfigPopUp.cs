﻿using System;
using System.Collections;
using System.Collections.Generic;
using CuponGo;
using CuponGo.Events;
using CuponGo.PerchaIPS;
using UnityEngine;
using UnityEngine.UI;

public class IpsConfigPopUp : Popup
{
    [SerializeField]
    private InputField KnnKInputField;
    [SerializeField]
    private InputField KnnNoRssInputField;
    [SerializeField]
    private InputField KnnHorizontalVarInputField;
    [SerializeField]
    private InputField KnnVerticalVarInputField;
    [SerializeField]
    private InputField KnnKalmanHorizontalVarInputField;
    [SerializeField]
    private InputField KnnKalmanVerticalVarInputField;
    
    [SerializeField]
    private GameObject KnnParametersConteiner;
    [SerializeField]
    private GameObject KnnKalmanParameterConeteiner;
    
    private IpsParameters ipsParameters;
    
    void Start()
    {
        ipsParameters = IpsUnityManager.Instance.IpsParameters;
        onIpsConfigChange();
    }

    private void onIpsConfigChange()
    {
        if(ipsParameters==null)
            return;
        
        if (ipsParameters.neural)
        {
            KnnParametersConteiner.SetActive(false);
        }
        else
        {
            KnnParametersConteiner.SetActive(true);
            KnnKInputField.text = ipsParameters.k.ToString();
            KnnNoRssInputField.text = ipsParameters.noRss.ToString();
            KnnHorizontalVarInputField.text = ipsParameters.horizontalMeasurementVariance.ToString();
            KnnVerticalVarInputField.text = ipsParameters.verticalMeasurementVariance.ToString();
        }
        
        if (ipsParameters.enableKalmanFilter)
        {
            KnnKalmanParameterConeteiner.SetActive(true);
            KnnKalmanHorizontalVarInputField.text = ipsParameters.horizontalVelocityVariance.ToString();
            KnnKalmanVerticalVarInputField.text = ipsParameters.verticalVelocityVariance.ToString();
        }
        else
        {
            KnnKalmanParameterConeteiner.SetActive(false);
        }
    }
    
    public void OnEnableIpsButtonClick()
    {
        CuponGoManager.Instance.AppState.IPSEnable = !CuponGoManager.Instance.AppState.IPSEnable;
        onIpsConfigChange();
    }

    public void OnChangeMethodButtonClick()
    {
        ipsParameters.neural = !ipsParameters.neural;
        IpsUnityManager.Instance.IpsParameters = ipsParameters;
        onIpsConfigChange();
    }
    
    public void OnChangeKnnKButtonClick()
    {
        int auxInt;
        bool result = Int32.TryParse(KnnKInputField.text, out auxInt);
        if (result)
        {
            ipsParameters.k = auxInt;
            IpsUnityManager.Instance.IpsParameters = ipsParameters;
        }
        onIpsConfigChange();
    }
    
    public void OnChangeKnnNoRssButtonClick()
    {
        int auxInt;
        bool result = Int32.TryParse(KnnNoRssInputField.text, out auxInt);
        if (result)
        {
            ipsParameters.noRss = auxInt;
            IpsUnityManager.Instance.IpsParameters = ipsParameters;
        }
        onIpsConfigChange();
    }
    
    public void OnChangeKnnHorizontalVarButtonClick()
    {
        double aux;
        bool result = Double.TryParse(KnnHorizontalVarInputField.text, out aux);
        if (result)
        {
            ipsParameters.horizontalMeasurementVariance = aux;
            IpsUnityManager.Instance.IpsParameters = ipsParameters;
        }
        onIpsConfigChange();
    }
    
    public void OnChangeKnnVerticalVarButtonClick()
    {
        double aux;
        bool result = Double.TryParse(KnnVerticalVarInputField.text, out aux);
        if (result)
        {
            ipsParameters.verticalMeasurementVariance = aux;
            IpsUnityManager.Instance.IpsParameters = ipsParameters;
        }
        onIpsConfigChange();
    }
    
    public void OnChangeKalmanEnableClick()
    {
        ipsParameters.enableKalmanFilter = !ipsParameters.enableKalmanFilter;
        IpsUnityManager.Instance.IpsParameters = ipsParameters;
        onIpsConfigChange();
    }
    
    public void OnChangeKalmanHorizontalVarButtonClick()
    {
        double aux;
        bool result = Double.TryParse(KnnKalmanHorizontalVarInputField.text, out aux);
        if (result)
        {
            ipsParameters.horizontalVelocityVariance = aux;
            IpsUnityManager.Instance.IpsParameters = ipsParameters;
        }
        onIpsConfigChange();
    }
    
    public void OnChangeKalmanVerticalVarButtonClick()
    {
        double aux;
        bool result = Double.TryParse(KnnKalmanVerticalVarInputField.text, out aux);
        if (result)
        {
            ipsParameters.verticalVelocityVariance = aux;
            IpsUnityManager.Instance.IpsParameters = ipsParameters;
        }
        onIpsConfigChange();
    }
}
