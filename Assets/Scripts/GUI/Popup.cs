using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Popup : MonoBehaviour
{
    public Color backgroundColor = new Color(10.0f / 255.0f, 10.0f / 255.0f, 10.0f / 255.0f, 0.6f);

    protected Canvas canvas;

    public float destroyTime = 0.5f;

    private GameObject m_background;

    public delegate void OnClose();
    public event OnClose onCloseEvent;

    public void Open(Canvas canvas)
    {
        this.canvas = canvas;
        AddBackground();
    }

    public void Close(float destroyTime = -1)
    {
        if (destroyTime <= 0)
        {
            destroyTime = this.destroyTime;
        }

        var animator = GetComponent<Animator>();
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
            animator.Play("Close");

        RemoveBackground();
        DestroyPopUp(destroyTime);
        if (onCloseEvent != null)
        {
            onCloseEvent.Invoke();
        }
    }

    private void DestroyPopUp(float destroyTime)
    {
        if (m_background != null)
        {
            Destroy(m_background);
        }
        Destroy(gameObject,destroyTime);
    }

    private void AddBackground()
    {
        GameObject background = GameObject.Find("PopupBackground");
        if (background == null)
        {
            return;
        }
        
        var bgTex = new Texture2D(1, 1);
        bgTex.SetPixel(0, 0, backgroundColor);
        bgTex.Apply();

        m_background = new GameObject("PopupBackground");
        var image = m_background.AddComponent<Image>();
        var rect = new Rect(0, 0, bgTex.width, bgTex.height);
        var sprite = Sprite.Create(bgTex, rect, new Vector2(0.5f, 0.5f), 1);
        image.material.mainTexture = bgTex;
        image.sprite = sprite;
        var newColor = image.color;
        image.color = newColor;
        image.canvasRenderer.SetAlpha(0.0f);
        image.CrossFadeAlpha(1.0f, 0.4f, false);

        m_background.transform.localScale = new Vector3(1, 1, 1);
        m_background.GetComponent<RectTransform>().sizeDelta = canvas.GetComponent<RectTransform>().sizeDelta;
        m_background.transform.SetParent(canvas.transform, false);
        m_background.transform.SetSiblingIndex(transform.GetSiblingIndex());
    }

    private void RemoveBackground()
    {
        if (m_background == null)
        {
            return;
        }
        var image = m_background.GetComponent<Image>();
        if (image != null)
            image.CrossFadeAlpha(0.0f, 0.2f, false);
    }
}
