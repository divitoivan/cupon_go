// Copyright (C) 2015, 2016 ricimi - All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement.
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms.

using UnityEngine;

namespace Ricimi
{
    // This class is responsible for creating and opening a popup of the given prefab and add
    // it to the UI canvas of the current scene.
    public class PopupOpener : MonoBehaviour
    {
        [SerializeField]
        private GameObject popupPrefab;
        [SerializeField]
        private Canvas m_canvas;

        protected void Start()
        {
        }

        public virtual void OpenPopup()
        {
            GameObject popup = Instantiate(popupPrefab);
            popup.SetActive(true);
            popup.transform.localScale = Vector3.zero;
            popup.transform.SetParent(m_canvas.transform, false);
            popup.GetComponent<ConfigPopUp>().Open(m_canvas);
        }
    }
}
