// prefrontal cortex -- http://prefrontalcortex.de
// Full Android Sensor Access for Unity3D
// Contact:
// 		contact@prefrontalcortex.de

using UnityEngine;

public class SignController : MonoBehaviour
{

    [SerializeField] public float bearing = 0;

    private void Start () {
        transform.rotation = Quaternion.Euler(
            new Vector3 (0, bearing, 0)
        );
	}

}