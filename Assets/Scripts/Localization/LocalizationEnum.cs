﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace CuponGo
{
    public sealed class LocalizationEnum
    {

        private readonly string name;
        private readonly int value;
        private static int lastValue = 0;

        public static readonly LocalizationEnum MainMenuSceneTag = new LocalizationEnum("main_menu_scene_tag");
        public static readonly LocalizationEnum MainMenuSceneExplore = new LocalizationEnum("main_menu_scene_explore");
		public static readonly LocalizationEnum MapSceneFriends = new LocalizationEnum("map_scene_friends");
		public static readonly LocalizationEnum MapSceneMyCoupon = new LocalizationEnum("map_scene_my_coupon");
		public static readonly LocalizationEnum MapSceneConfig = new LocalizationEnum("map_scene_config");
        public static readonly LocalizationEnum MapSceneAuctions = new LocalizationEnum("map_scene_auctions");
        public static readonly LocalizationEnum MapSceneCamera = new LocalizationEnum("map_scene_camera");
        public static readonly LocalizationEnum MapSceneConfigPopupTitle = new LocalizationEnum("map_scene_config_popup_title");
        public static readonly LocalizationEnum MapScenePetMsgWelcome1 = new LocalizationEnum("map_scene_pet_msg_welcome_1");
        public static readonly LocalizationEnum MapScenePetMsgWelcome2 = new LocalizationEnum("map_scene_pet_msg_welcome_2");
        public static readonly LocalizationEnum MapScenePetMsgCelebrate = new LocalizationEnum("map_scene_pet_msg_celebrate");
        public static readonly LocalizationEnum MapScenePetMsgNewAuction = new LocalizationEnum("map_scene_pet_msg_new_auction");
        public static readonly LocalizationEnum MapScenePetMsgWinAuction = new LocalizationEnum("map_scene_pet_msg_win_auction");
        public static readonly LocalizationEnum MapScenePetMsgCuponCongratulation = new LocalizationEnum("map_scene_pet_msg_cupon_congratulation");
        public static readonly LocalizationEnum CuponDetailSceneExchange = new LocalizationEnum("cupon_detail_scene_exchange");
        public static readonly LocalizationEnum CuponDetailSceneGift = new LocalizationEnum("cupon_detail_scene_gift");
        public static readonly LocalizationEnum CuponDetailSceneGoStore = new LocalizationEnum("cupon_detail_scene_go_store");
        public static readonly LocalizationEnum CuponDetailSceneGoDiscard = new LocalizationEnum("cupon_detail_scene_go_discard");
        public static readonly LocalizationEnum CuponListSceneTitle = new LocalizationEnum("cupon_list_scene_title");
        public static readonly LocalizationEnum DefaultCuponCheekyDescription = new LocalizationEnum("default_cupon_cheeky_description");
        public static readonly LocalizationEnum DefaultCuponMimoCoDescription = new LocalizationEnum("default_cupon_mimo_co_description");
        public static readonly LocalizationEnum DefaultCuponMcDonaldsDescription = new LocalizationEnum("default_cupon_mc_donalds_description");
        public static readonly LocalizationEnum DefaultCuponSubwayDescription = new LocalizationEnum("default_cupon_subway_description");
        public static readonly LocalizationEnum DefaultCuponFreddoDescription = new LocalizationEnum("default_cupon_freddo_description");
        public static readonly LocalizationEnum DefaultCuponCafeMartinezDescription = new LocalizationEnum("default_cupon_cafe_martinez_description");
        public static readonly LocalizationEnum DefaultCuponBonafideDescription = new LocalizationEnum("default_cupon_bonafide_description");
        public static readonly LocalizationEnum DefaultCuponHavannaDescription = new LocalizationEnum("default_cupon_havanna_description");
        public static readonly LocalizationEnum DefaultCuponCherDescription = new LocalizationEnum("default_cupon_cher_description");
        public static readonly LocalizationEnum DefaultCuponAyNotDeadDescription = new LocalizationEnum("default_cupon_ay_not_dead_description");
        public static readonly LocalizationEnum DefaultCuponRossiCarusoDescription = new LocalizationEnum("default_cupon_rossi_caruso_description");
        public static readonly LocalizationEnum MapSceneConfigPopupCelebrate = new LocalizationEnum("map_scene_config_popup_celebrate");
        public static readonly LocalizationEnum MapSceneConfigPopupChangeCharacter = new LocalizationEnum("map_scene_config_popup_change_character");
        public static readonly LocalizationEnum MapSceneConfigPopupResetAppState = new LocalizationEnum("map_scene_config_popup_reset_app_state");
        public static readonly LocalizationEnum MapSceneConfigPopupNewAuction = new LocalizationEnum("map_scene_config_popup_new_auction");
        public static readonly LocalizationEnum MapSceneConfigPopupWinAuction = new LocalizationEnum("map_scene_config_popup_win_auction");
        public static readonly LocalizationEnum MapSceneGoToStorePopupTitle = new LocalizationEnum("map_scene_go_to_store_popup_title");
        public static readonly LocalizationEnum MapSceneGoToStorePopupGoButton = new LocalizationEnum("map_scene_go_to_store_popup_go_button");
        public static readonly LocalizationEnum MapSceneGoToStorePopupDescription = new LocalizationEnum("map_scene_go_to_store_popup_description");
        public static readonly LocalizationEnum MapSceneGoToStorePopupDefaultStoreName = new LocalizationEnum("map_scene_go_to_store_popup_default_store_name");
        public static readonly LocalizationEnum MapScenePetMsgGoToStore = new LocalizationEnum("map_scene_pet_msg_go_to_store");
        public static readonly LocalizationEnum PetMenuFindStore = new LocalizationEnum("pet_menu_find_store");
        public static readonly LocalizationEnum PetMenuFindProduct = new LocalizationEnum("pet_menu_find_product");
        public static readonly LocalizationEnum PetMenuFindPromo = new LocalizationEnum("pet_menu_find_promo");
        public static readonly LocalizationEnum PetMenuFindFriend = new LocalizationEnum("pet_menu_find_friend");
        public static readonly LocalizationEnum PetMenuFindCar = new LocalizationEnum("pet_menu_find_car");

        private LocalizationEnum(string name)
        {
            this.name = name;
            lastValue++;
            this.value = lastValue;
        }

        private LocalizationEnum(int value, string name)
        {
            this.name = name;
            this.value = value;
            if (value > lastValue)
            {
                lastValue = value;
            }
        }

        public override string ToString()
        {
            return name;
        }

    }
}
