﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : Singleton<LocalizationManager> {

    private Dictionary<string, string> localizedText;
    private bool isReady = false;
    private string missingTextString = "Localized text not found";
    private string localizationFolder = "Localization";
    private string defaultLocalizationFile = "localization_en.json";
    private string localizationFile;

    public delegate void OnLocalizationChange();
    public event OnLocalizationChange onLocalizationChange;

    protected LocalizationManager()
    {
    }

    private void Awake()
    {
        StartCoroutine(LoadLocalizedText(defaultLocalizationFile));
    }

    public IEnumerator LoadLocalizedText(string fileName)
    {
        if (fileName != localizationFile)
        {
            string filePath = Path.Combine (Application.streamingAssetsPath, localizationFolder);
            filePath = Path.Combine (filePath, fileName);

            //string filePath = Application.streamingAssetsPath + "/" + localizationFolder;
            //filePath = filePath + "/" + fileName;

            /*
            if(Application.platform == RuntimePlatform.Android)
            {
                filePath =  "jar:file://" + filePath;
            }*/

            string fileContent = null;

            if (filePath.Contains("://"))
            {
                WWW www = new WWW(filePath);
                yield return www;
                if (string.IsNullOrEmpty(www.error))
                {
                    fileContent = www.text;
                }
                else
                {
                    Debug.LogError(www.error);
                }
            } else
            {
                if (File.Exists(filePath))
                {
                    fileContent = System.IO.File.ReadAllText(filePath);
                } else
                {
                    Debug.LogError ("Cannot find file!");
                }
            }

            if (fileContent != null)
            {
                localizationFile = fileName;
                localizedText = new Dictionary<string, string> ();

                LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(fileContent);

                foreach (LocalizationItem localizationItem in loadedData.items)
                {
                    localizedText.Add (localizationItem.key, localizationItem.value);
                }

                isReady = true;
                if (onLocalizationChange != null)
                {
                    onLocalizationChange.Invoke();
                }

            }

        }

    }

    public string GetLocalizedValue(string key)
    {
        string result;

        if (localizedText == null || !localizedText.TryGetValue(key, out result))
        {
            return missingTextString;
        }

        return result;
    }

    public bool GetIsReady()
    {
        return isReady;
    }

}