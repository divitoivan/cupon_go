﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour {

    [SerializeField] private string key;
    [TextArea(3,10)] [SerializeField] private string textDebug;

    private Text text;

    private void Start ()
    {
        text = GetComponent<Text>();
        if (text != null)
        {
            text.text = "";
        }

        LocalizationManager.Instance.onLocalizationChange += OnLocalizationChange;

        if (LocalizationManager.Instance.GetIsReady())
        {
            OnLocalizationChange();
        }

    }

    private void OnLocalizationChange()
    {
        if (text != null)
        {
            text.text = GetLocalizedText();
        }
    }

    public string GetLocalizedText()
    {
        if (string.IsNullOrEmpty(textDebug))
        {
            return LocalizationManager.Instance.GetLocalizedValue(key);
        }
        return textDebug;
    }

}