﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TriangleNet;
using TriangleNet.Geometry;

public class MapMeshCreator : MonoBehaviour
{
	[SerializeField] private float limitWidth = 0.1f;
	[SerializeField] private float zoneY = 0f;
	[SerializeField] private float limitY = 0.001f;
	[SerializeField] private float nameY = 0.002f;
	[SerializeField] private Material storeLimitMaterial;
	[SerializeField] private Material standLimitMaterial;
	[SerializeField] private Material storeZoneMaterial;
	[SerializeField] private Material standZoneMaterial;
	[SerializeField] private GameObject TextPrefab;

	private List<GameObject> mapElementList;
	private TriangleNet.Mesh triangleNetMeshTemp; 

	private List<GameObject> MapElementList{

		get {
			if (mapElementList == null) {
				mapElementList = new List<GameObject> ();
			}
			return mapElementList;
		}
	}

	public void CreateMap(JSONObject geo)
	{

		JSONObject layerListArray = geo.GetField ("layerList");
		if (layerListArray.type == JSONObject.Type.ARRAY) {

			foreach(JSONObject layer in layerListArray.list){

				string layerName = layer.GetField ("name").str;

				if(layerName == "store_limits" || layerName == "stands_limits" ){

					JSONObject geometryListArray = layer.GetField ("geometryList");
					if (geometryListArray.type == JSONObject.Type.ARRAY) {

						foreach (JSONObject geometry in geometryListArray.list) {

							string geometryType = geometry.GetField ("type").str;

							JSONObject pointList = geometry.GetField ("points");

							GameObject layerElementConteiner = new GameObject (layerName);
							layerElementConteiner.transform.localPosition = new Vector3 (0, limitY, 0);
							MapElementList.Add (layerElementConteiner);
							layerElementConteiner.transform.parent = transform;
							MeshFilter layerElementConteinerMeshFilter = layerElementConteiner.AddComponent<MeshFilter>();
							MeshRenderer layerElementConteinerMeshRender = layerElementConteiner.AddComponent<MeshRenderer>();
							layerElementConteinerMeshRender.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

							if (geometryType == "lineString") {

								UnityEngine.Mesh mesh = layerElementConteinerMeshFilter.mesh;
								MeshData meshData = new MeshData ();
								List<Vector3> linePoints = new List<Vector3> ();

								foreach(JSONObject point in pointList.list){
									linePoints.Add (new Vector3 (point [0].f, 0, point [1].f));
								}

								CreateLineMesh(linePoints, meshData);						

								mesh.vertices = meshData.Vertices.ToArray();
								mesh.triangles = meshData.Indices.ToArray();
								mesh.SetUVs(0, meshData.UV);
								mesh.RecalculateNormals();

								switch (layerName) {

								case "store_limits":

									layerElementConteinerMeshRender.material = storeLimitMaterial;
									break;

								case "stands_limits":

									layerElementConteinerMeshRender.material = standLimitMaterial;
									break;

								}


							}

						}

					}
						
				}

				if(layerName == "stand_zones" || layerName == "store_zones" ){

					JSONObject geometryListArray = layer.GetField ("geometryList");
					if (geometryListArray.type == JSONObject.Type.ARRAY) {

						foreach (JSONObject geometry in geometryListArray.list) {

							string geometryType = geometry.GetField ("type").str;

							JSONObject poligonPointList = geometry.GetField ("points");

							GameObject layerElementConteiner = new GameObject (layerName);
							layerElementConteiner.transform.localPosition = new Vector3 (0, zoneY, 0);
							MapElementList.Add (layerElementConteiner);
							layerElementConteiner.transform.parent = transform;

							if (geometryType == "polygon") {

								foreach (JSONObject subpoligonPointList in poligonPointList.list) {
									
									GameObject subpoligonGameObject = new GameObject (layerName);
									subpoligonGameObject.transform.parent = layerElementConteiner.transform;
									subpoligonGameObject.transform.localPosition = Vector3.zero;

									MeshFilter subpoligonMeshFilter = subpoligonGameObject.AddComponent<MeshFilter> ();
									MeshRenderer subpoligonMeshRender = subpoligonGameObject.AddComponent<MeshRenderer> ();
									subpoligonMeshRender.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

									UnityEngine.Mesh mesh = subpoligonMeshFilter.mesh;
									MeshData meshData = new MeshData ();
									List<Vector3> linePoints = new List<Vector3> ();

									foreach (JSONObject point in subpoligonPointList.list) {
										linePoints.Add (new Vector3 (point [0].f, 0, point [1].f));
									}

									CreateStoreMesh (linePoints, meshData);						

									mesh.vertices = meshData.Vertices.ToArray ();
									mesh.triangles = meshData.Indices.ToArray ();
									mesh.SetUVs (0, meshData.UV);
									mesh.RecalculateNormals ();

									switch (layerName) {

									case "stand_zones":

										subpoligonMeshRender.material = standZoneMaterial;
										break;

									case "store_zones":

										subpoligonMeshRender.material = storeZoneMaterial;
										break;

									}

								}

							}

						}

					}

				}

				if (layerName == "points_of_interest") {

					JSONObject geometryListArray = layer.GetField ("geometryList");
					if (geometryListArray.type == JSONObject.Type.ARRAY) {

						foreach (JSONObject geometry in geometryListArray.list) {

							string type = geometry.GetField ("type").str;

							if (type == "pointOfInterest") {

								string name = geometry.GetField ("name").str;
								name = System.Text.RegularExpressions.Regex.Unescape(name);
								string imageUrl = geometry.GetField ("imageUrl").str;
								float heading = geometry.GetField ("heading").f;

								JSONObject positionPointList = geometry.GetField ("point");
								Vector3 position = new Vector3 (positionPointList.list [0].f, nameY, positionPointList.list [1].f);

								JSONObject sizePointList = geometry.GetField ("size");
								Vector2 size = new Vector2 (sizePointList.list [0].f, sizePointList.list [1].f);

								GameObject pointOfInterestGameObject = Instantiate(TextPrefab);
								MapElementList.Add (pointOfInterestGameObject);
								pointOfInterestGameObject.transform.name = "POI " + name;
								pointOfInterestGameObject.transform.parent = transform;
								pointOfInterestGameObject.transform.position = position;

								TextMesh text = pointOfInterestGameObject.GetComponentInChildren<TextMesh> ();
								text.text = name;
								float factorWidth = size.x/text.GetComponent<Renderer>().bounds.size.x;
								float factorHeight = size.y/text.GetComponent<Renderer>().bounds.size.y;
								float factor = Mathf.Min (factorWidth, factorHeight) ; 
								text.transform.localScale = new Vector3 (factor, factor, 1f);

								pointOfInterestGameObject.transform.rotation = Quaternion.AngleAxis(90, Vector3.right) * Quaternion.AngleAxis(heading, -Vector3.forward);

								/* imagen
										GameObject storeNameGameObject = GameObject.CreatePrimitive(PrimitiveType.Quad);
										storeNameGameObject.transform.name = layerName + " name";
										storeNameGameObject.transform.parent = subpoligonGameObject.transform;
										Vector3 position = meshData.Center;
										position.y = nameY;
										storeNameGameObject.transform.position = position;
										storeNameGameObject.transform.localScale = new Vector3 (1, 1, 1);
										storeNameGameObject.transform.rotation = Quaternion.AngleAxis(90, Vector3.right);

										Renderer nameRenderer = storeNameGameObject.GetComponent<Renderer>();
										StartCoroutine(Utils.loadRenderTexture (brandImageUrl, nameRenderer));
										//pasar a UniRx para modificar aca el tamaño del bloque
										*/

							}

						}

					}

				}

			}

		}

	}

	public void DeleteMap() {

		foreach (GameObject go in MapElementList) {
			Destroy (go);
		}
		MapElementList.Clear();
	}

	private void CreateLineMesh(List<Vector3> list, MeshData md)
	{

		int vertsStartCount = md.Vertices.Count;
		Vector3 lastPos = Vector3.zero ;
		Vector3 norm = Vector3.zero;
		float lastUV = 0f;
		for (int i = 1; i < list.Count; i++)
		{
			Vector3 p1 = list[i - 1];
			Vector3 p2 = list[i];
			Vector3 p3 = p2;
			if (i + 1 < list.Count)
				p3 = list[i + 1];

			if (lastPos == Vector3.zero)
			{
				lastPos = Vector3.Lerp(p1, p2, 0f);
				norm = GetNormal(p1, lastPos, p2) * limitWidth;
				md.Vertices.Add(lastPos + norm);
				md.Vertices.Add(lastPos - norm);
				md.UV.Add(new Vector2(0, 0));
				md.UV.Add(new Vector2(1, 0));
			}
			var dist = Vector3.Distance(lastPos, p2);
			lastUV += dist;
			lastPos = p2;
			//lastPos = Vector3.Lerp(p1, p2, 1f);
			norm = GetNormal(p1, lastPos, p3) * limitWidth;
			md.Vertices.Add(lastPos + norm);
			md.Vertices.Add(lastPos - norm);
			md.UV.Add(new Vector2(0, lastUV));
			md.UV.Add(new Vector2(1, lastUV));
		}


		for (int j = vertsStartCount; j <= md.Vertices.Count - 3; j += 2)
		{
			Vector3 clock = Vector3.Cross(md.Vertices[j + 1] - md.Vertices[j], md.Vertices[j + 2] - md.Vertices[j + 1]);
			if (clock.y < 0)
			{
				md.Indices.Add(j);
				md.Indices.Add(j + 2);
				md.Indices.Add(j + 1);

				md.Indices.Add(j + 1);
				md.Indices.Add(j + 2);
				md.Indices.Add(j + 3);
			}
			else
			{
				md.Indices.Add(j + 1);
				md.Indices.Add(j + 2);
				md.Indices.Add(j);

				md.Indices.Add(j + 3);
				md.Indices.Add(j + 2);
				md.Indices.Add(j + 1);
			}
		}
	}


	private void CreateStoreMesh(List<Vector3> list, MeshData md)
	{
		triangleNetMeshTemp = new TriangleNet.Mesh();
		InputGeometry input = new InputGeometry(list.Count);

		for (int i = 0; i < list.Count; i++)
		{
			Vector3 v = list[i];
			input.AddPoint(v.x, v.z);
			input.AddSegment(i, (i + 1)%list.Count);
		}
		triangleNetMeshTemp.Behavior.Algorithm = TriangulationAlgorithm.SweepLine;
		triangleNetMeshTemp.Behavior.Quality = true;
		triangleNetMeshTemp.Triangulate(input);

		int vertsStartCount = md.Vertices.Count;
		md.Vertices.AddRange(list.Select(x => new Vector3(x.x, 0, x.z)).ToList());

		foreach (var tri in triangleNetMeshTemp.Triangles)
		{
			md.Indices.Add(vertsStartCount + tri.P1);
			md.Indices.Add(vertsStartCount + tri.P0);
			md.Indices.Add(vertsStartCount + tri.P2);
		}

		foreach (var c in list)
		{
			md.UV.Add(new Vector2((c.x), (c.z)));
		}

		BoundingBox bound = triangleNetMeshTemp.Bounds;
		md.Center = new Vector3 ((float)(bound.Xmin + (bound.Xmax - bound.Xmin) / 2f), 0f, (float)(bound.Ymin + (bound.Ymax - bound.Ymin) / 2f));
	}

	private Vector3 GetNormal(Vector3 p1, Vector3 newPos, Vector3 p2)
	{
		if (newPos == p1 || newPos == p2)
		{
			Vector3 n = (p2 - p1).normalized;
			return new Vector3(-n.z, 0, n.x);
		}

		Vector3 b = (p2 - newPos).normalized + newPos;
		Vector3 a = (p1 - newPos).normalized + newPos;
		Vector3 t = (b - a).normalized;

		if (t == Vector3.zero)
		{
			Vector3 n = (p2 - p1).normalized;
			return new Vector3(-n.z, 0, n.x);
		}

		return new Vector3(-t.z, 0, t.x);
	}

}