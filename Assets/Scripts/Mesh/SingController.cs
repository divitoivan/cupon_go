﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingController : MonoBehaviour {

	[SerializeField]
	private TextMesh ForwardTextMesh;

	[SerializeField]
	private TextMesh BackwardTextMesh;

	[SerializeField]
	private Transform TextBackGround;

	public void setText(string text){

		ForwardTextMesh.text = text;
		BackwardTextMesh.text = text;

		Mesh mesh = ForwardTextMesh.gameObject.GetComponent<MeshFilter>().mesh;

		Bounds bounds = mesh.bounds;

		Vector3 size = bounds.size;
	}

}
