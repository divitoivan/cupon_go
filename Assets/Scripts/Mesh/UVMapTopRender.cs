﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// http://answers.unity3d.com/questions/294165/apply-uv-coordinates-to-unity-cube-by-script.html
public class UVMapTopRender : MonoBehaviour {

    void Start()
    {
        // Get the mesh
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        Mesh mesh = meshFilter.mesh;

        Vector2[] uv = mesh.uv;


        // Front map
        uv[2] = new Vector2(0.0f, 0.0f);
        uv[3] = new Vector2(0.0f, 0.0f);
        uv[0] = new Vector2(0.0f, 0.0f);
        uv[1] = new Vector2(0.0f, 0.0f);

        // back map
        uv[6] = new Vector2(0.0f, 0.0f);
        uv[7] = new Vector2(0.0f, 0.0f);
        uv[10] = new Vector2(0.0f, 0.0f);
        uv[11] = new Vector2(0.0f, 0.0f);

        // left map
        uv[19] = new Vector2(0.0f, 0.0f);
        uv[17] = new Vector2(0.0f, 0.0f);
        uv[16] = new Vector2(0.0f, 0.0f);
        uv[18] = new Vector2(0.0f, 0.0f);

        // right map
        uv[23] = new Vector2(0.0f, 0.0f);
        uv[21] = new Vector2(0.0f, 0.0f);
        uv[20] = new Vector2(0.0f, 0.0f);
        uv[22] = new Vector2(0.0f, 0.0f);

        //top
        uv[4] = new Vector2(0.0f, 1.0f);
        uv[5] = new Vector2(1.0f, 1.0f);
        uv[8] = new Vector2(0.0f, 0.0f);
        uv[9] = new Vector2(1.0f, 0.0f);

        // bottom map
        uv[15] = new Vector2(0.0f, 0.0f);
        uv[13] = new Vector2(0.0f, 0.0f);
        uv[12] = new Vector2(0.0f, 0.0f);
        uv[14] = new Vector2(0.0f, 0.0f);

        mesh.uv = uv;
    }

    // Update is called once per frame
    void Update () {

	}
}
