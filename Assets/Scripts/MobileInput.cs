﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;
using CuponGo.Events;

namespace CuponGo
{
    public class MobileInput : Singleton<MobileInput>
    {

        public static string inputTouchDragHorizontalAxisName = "TouchDragHorizontal";          //Axis horizontal Movement Normalized to screen width
        public static string inputTouchDragVerticalAxisName = "TouchDragVertical";              //Axis vertical Movement Normalized to screen height
        public static string inputTouchPositionHorizontalAxisName = "TouchPositionHorizontal";   //Axis horizontal Positio Normalized to screen width
        public static string inputTouchPositionVerticalAxisName = "TouchPositionVertical";       //Axis vertical Positio Normalized to screen height
        public static string inputTouchPitchAxisName = "TouchPitch";                            //Axis touch pitch Movement Normalized to screen diagonal

        private CrossPlatformInputManager.VirtualAxis inputTouchDragHorizontalAxis;
        private CrossPlatformInputManager.VirtualAxis inputTouchDragVerticalAxis;
        private CrossPlatformInputManager.VirtualAxis inputTouchPositionHorizontalAxis;
        private CrossPlatformInputManager.VirtualAxis inputTouchPositionVerticalAxis;
        private CrossPlatformInputManager.VirtualAxis inputTouchPitchAxis;

        public float clickMoveThr = 100;
        private float touchMove = 0;
        private bool clickStart = false;

        private float m_ScreenDiagonal;
        
#if UNITY_EDITOR
        private Vector2? lastMousePosition;        
#endif

        protected MobileInput()
        {
        }

        private void Awake()
        {
            m_ScreenDiagonal = Mathf.Sqrt(Screen.height * Screen.height + Screen.width * Screen.width);
        }

        private void Start()
        {
            CreateVirtualAxes();
        }

        private void CreateVirtualAxes()
        {
            if (!CrossPlatformInputManager.AxisExists(inputTouchDragHorizontalAxisName))
            {
                inputTouchDragHorizontalAxis = new CrossPlatformInputManager.VirtualAxis(inputTouchDragHorizontalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(inputTouchDragHorizontalAxis);
            }

            if (!CrossPlatformInputManager.AxisExists(inputTouchDragVerticalAxisName))
            {
                inputTouchDragVerticalAxis = new CrossPlatformInputManager.VirtualAxis(inputTouchDragVerticalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(inputTouchDragVerticalAxis);
            }
            
            if (!CrossPlatformInputManager.AxisExists(inputTouchPositionHorizontalAxisName))
            {
                inputTouchPositionHorizontalAxis = new CrossPlatformInputManager.VirtualAxis(inputTouchPositionHorizontalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(inputTouchPositionHorizontalAxis);
            }

            if (!CrossPlatformInputManager.AxisExists(inputTouchPositionVerticalAxisName))
            {
                inputTouchPositionVerticalAxis = new CrossPlatformInputManager.VirtualAxis(inputTouchPositionVerticalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(inputTouchPositionVerticalAxis);
            }

            if (!CrossPlatformInputManager.AxisExists(inputTouchPitchAxisName))
            {
                inputTouchPitchAxis = new CrossPlatformInputManager.VirtualAxis(inputTouchPitchAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(inputTouchPitchAxis);
            }
        }

        private void Update()
        {
            
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                EventManager.Instance.Raise(new OnBackPressedEvent());
            }
            
            if (EventSystem.current != null && Camera.main != null && Input.touchCount == 1)
            {
                Touch touch = Input.GetTouch(0);
                Vector2 move = touch.deltaPosition;
                Vector2 position = touch.position;

                CrossPlatformInputManager.SetAxis(inputTouchDragHorizontalAxisName, move.x / Screen.width);
                CrossPlatformInputManager.SetAxis(inputTouchDragVerticalAxisName, move.y / Screen.height);
                CrossPlatformInputManager.SetAxis(inputTouchPositionHorizontalAxisName, position.x / Screen.width);
                CrossPlatformInputManager.SetAxis(inputTouchPositionVerticalAxisName, position.y / Screen.height);

                switch (touch.phase)
                {
                    case TouchPhase.Began:

                        clickStart = true;
                        touchMove = 0;
                        break;

                    case TouchPhase.Moved:
                        touchMove += touch.deltaPosition.magnitude;
                        if (touchMove > clickMoveThr)
                        {
                            clickStart = false;
                        }
                        break;

                    case TouchPhase.Ended:

                        if (clickStart)
                        {
                            Ray ray = Camera.main.ScreenPointToRay(touch.position);

							PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
							eventDataCurrentPosition.position = new Vector2(touch.position.x, touch.position.y);
							List<RaycastResult> results = new List<RaycastResult>();
							EventSystem.current.RaycastAll (eventDataCurrentPosition, results);

                            RaycastHit hit;
                            if (Physics.Raycast(ray, out hit) && (results.Count == 0 || hit.distance <= results[0].distance))
                            {
                                Vector3 hitPoint = hit.point;
                                GameObject go = hit.transform.gameObject;
                                EventManager.Instance.Raise(new OnTouchClickEvent{ ClickedObject = go, ClickedPoint = hitPoint});
                            }

                            
                        }
                        break;
                }
            }
            else
            {
                CrossPlatformInputManager.SetAxis(inputTouchDragHorizontalAxisName, 0);
                CrossPlatformInputManager.SetAxis(inputTouchDragVerticalAxisName, 0);
                CrossPlatformInputManager.SetAxis(inputTouchPositionHorizontalAxisName, 0);
                CrossPlatformInputManager.SetAxis(inputTouchPositionVerticalAxisName, 0);
                clickStart = false;
            }

            if (Input.touchCount == 2)
            {
                // Store both touches.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                // Find the position in the previous frame of each touch.
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                // Find the magnitude of the vector (the distance) between the touches in each frame.
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                // Find the difference in the distances between each frame.
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                CrossPlatformInputManager.SetAxis(inputTouchPitchAxisName, deltaMagnitudeDiff / m_ScreenDiagonal);

            }
            else
            {
                CrossPlatformInputManager.SetAxis(inputTouchPitchAxisName, 0);
            }

#if UNITY_EDITOR // input simulation in unity editor mode
            if (Input.touchCount == 0)
            {
                CrossPlatformInputManager.SetAxis(inputTouchPitchAxisName, Input.mouseScrollDelta.y / 100) ;
                if (Input.GetMouseButton(0))
                {
                    if (lastMousePosition != null)
                    {
                        Vector2 mousePosition = Input.mousePosition; 
                        Vector2 mouseDelta = mousePosition - lastMousePosition.Value;
                        
                        CrossPlatformInputManager.SetAxis(inputTouchDragHorizontalAxisName,
                            mouseDelta.x / Screen.width);
                        CrossPlatformInputManager.SetAxis(inputTouchDragVerticalAxisName, mouseDelta.y / Screen.height);
                        CrossPlatformInputManager.SetAxis(inputTouchPositionHorizontalAxisName,
                            mousePosition.x / Screen.width);
                        CrossPlatformInputManager.SetAxis(inputTouchPositionVerticalAxisName,
                            mousePosition.y / Screen.height);
                    }

                    lastMousePosition = Input.mousePosition;
                }
                else
                {
                    lastMousePosition = null;
                }
                if (EventSystem.current != null && Camera.main != null && Input.GetMouseButtonDown(0))
                {
                    
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                    PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
                    eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                    List<RaycastResult> results = new List<RaycastResult>();
                    EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
                    
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit) && (results.Count == 0 || hit.distance <= results[0].distance))
                    {
                        Vector3 hitPoint = hit.point;
                        GameObject go = hit.transform.gameObject;
                        EventManager.Instance.Raise(new OnTouchClickEvent
                        {
                            ClickedObject = go,
                            ClickedPoint = hitPoint
                        });
                    }
                    
                }
            }
#endif

        }
    }
}