﻿
using CuponGo.Events;
using UnityEngine;

[System.Serializable]
public class AppState
{

    [SerializeField] private bool initialPetMessageShowed = false;
    [SerializeField] private bool catchCuponMessageShowed = false;
    [SerializeField] private bool currentCharacterIsMale = false;
    [SerializeField] private bool ipsEnable = false;

    public bool InitialPetMessageShowed
    {
        get { return initialPetMessageShowed; }
        set
        {
            initialPetMessageShowed = value;
            EventManager.Instance.Raise(new OnAppStateValueChangeEvent());
            EventManager.Instance.Raise(new OnInitialPetMessageShowedChangeEvent());
        }
    }

    public bool CatchCuponMessageShowed
    {
        get { return catchCuponMessageShowed; }
        set
        {
            catchCuponMessageShowed = value;
            EventManager.Instance.Raise(new OnAppStateValueChangeEvent());
            EventManager.Instance.Raise(new OnCatchCuponMessageShowed()) ;
        }
    }

    public bool CurrentCharacterIsMale
    {
        get { return currentCharacterIsMale; }
        set
        {
            currentCharacterIsMale = value;
            EventManager.Instance.Raise(new OnAppStateValueChangeEvent());
            EventManager.Instance.Raise(new OnCurrentCharacterChangeEvent()) ;
        }
    }

    public bool IPSEnable
    {
        get { return ipsEnable; }
        set
        {
            ipsEnable = value;
            EventManager.Instance.Raise(new OnAppStateValueChangeEvent());
            EventManager.Instance.Raise(new OnEnableIPSChangeEvent()) ;
        }
    }





}
