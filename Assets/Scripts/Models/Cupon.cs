﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CuponGo;

[Serializable]
public class Cupon : System.Object {
    
    
    public long Id { get; set; }
    public string CompanyName { get; set; }
    public string ImageUrl { get; set; }
    public string SponsorImageUrl { get; set; }
    public string SponsorName { get; set; }
    public string CuponDescription { get; set; }

    public static bool operator ==(Cupon a, Cupon b)
    {
        if (System.Object.ReferenceEquals(a, b))
        {
            return true;
        }

        if (((object)a == null) || ((object)b == null))
        {
            return false;
        }

        return a.Id == b.Id;
    }

    public static bool operator !=(Cupon a, Cupon b)
    {
        return !(a == b);
    }

    public override bool Equals(System.Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        Cupon p = obj as Cupon;
        if ((System.Object)p == null)
        {
            return false;
        }

        return (Id == p.Id);
    }

    public bool Equals(Cupon p)
    {
        if ((object)p == null)
        {
            return false;
        }

        return (Id == p.Id);
    }

    public override int GetHashCode()
    {
        return (int)Id;
    }


    public static List<Cupon> GetCuponDebugList()
    {
        List<Cupon> cuponList = new List<Cupon>();

        /*
        Cupon cupon1 = new Cupon
        {
            Id = -16, // debug catch
            CompanyName = "MIMO & CO",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponMimoCoDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=16"
            ImageUrl = "https://pbs.twimg.com/profile_images/687248241923125248/iU7qY-kQ.jpg"
        };
        cuponList.Add(cupon1);

        Cupon cupon2 = new Cupon
        {
            Id = -1, // debug catch
            CompanyName = "Mc Donalds",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponMcDonaldsDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=1"
            ImageUrl = "https://pbs.twimg.com/profile_images/658746945565954048/Zrf2h3RD_400x400.jpg",
            SponsorImageUrl = "http://i.imgur.com/QD0YrmU.jpg"
        };

        cuponList.Add(cupon2);

        Cupon cupon3 = new Cupon
        {
            Id = -3, // debug catch
            CompanyName = "SubWay",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponSubwayDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=3"
            ImageUrl = "https://s3-media3.fl.yelpcdn.com/bphoto/cx25D2F-16iOYahnSFed7g/l.jpg"
        };

        cuponList.Add(cupon3);

        Cupon cupon4 = new Cupon
        {
            Id = -10, // debug catch
            CompanyName = "Freddo",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponFreddoDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=10"
            ImageUrl = "https://pbs.twimg.com/profile_images/711991640643076096/yQKU7lYT.jpg"
        };

        cuponList.Add(cupon4);

        Cupon cupon5 = new Cupon
        {
            Id = -5, // debug catch
            CompanyName = "Cafe Martinez",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponCafeMartinezDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=5"
            ImageUrl = "http://viaresto.viapais.com.ar/img/1mqnvv45f4ublg225qblvd45635404269459132500.jpg"
        };

        cuponList.Add(cupon5);

        Cupon cupon6 = new Cupon
        {
            Id = -7, // debug catch
            CompanyName = "Bonafide",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponBonafideDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=7"
            ImageUrl = "http://www.tarjetajoven.com.ar/wp-content/uploads/2014/03/bonafide500x500.png"
        };

        cuponList.Add(cupon6);

        Cupon cupon7 = new Cupon
        {
            Id = -4, // debug catch
            CompanyName = "Havanna",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponHavannaDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=4"
            ImageUrl = "http://www.argentinamall.com/images/AMAL00190_M.jpg"
        };

        cuponList.Add(cupon7);
        */
        Cupon cupon8 = new Cupon
        {
            Id = 1, // debug catch
            CompanyName = "Cher",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponCherDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=4"
            ImageUrl = "http://i.imgur.com/346eXTM.png",
            SponsorName = "The Blue Card",
            SponsorImageUrl = "http://i.imgur.com/hzrUvON.jpg" // "http://i.imgur.com/UgAFIrX.png" // "http://i.imgur.com/QD0YrmU.jpg"
        };

        cuponList.Add(cupon8);
        
        Cupon cupon9 = new Cupon
        {
            Id = 2, // debug catch
            CompanyName = "AY Not Dead",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponAyNotDeadDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=4"
            ImageUrl = "http://i.imgur.com/0uLkpnY.jpg",
            SponsorName = "Flying Airlines",
            SponsorImageUrl = "http://i.imgur.com/Q0j0jh1.jpg"//"http://i.imgur.com/QD0YrmU.jpg"
        };

        cuponList.Add(cupon9);
        
        Cupon cupon10 = new Cupon
        {
            Id = 3, // debug catch
            CompanyName = "Rossi & Caruso",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponRossiCarusoDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=4"
            ImageUrl = "http://i.imgur.com/wW5cvMR.jpg",
            SponsorName = "Flying Airlines",
            SponsorImageUrl = "http://i.imgur.com/UgAFIrX.png"//"http://i.imgur.com/QD0YrmU.jpg"
        };
        
        cuponList.Add(cupon10);
        
        /*
        Cupon cupon11 = new Cupon
        {
            Id = 4, // debug catch
            CompanyName = "Freddo",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponFreddoDescription.ToString()),
            ImageUrl = "http://i.imgur.com/xl3NRkp.jpg",
            SponsorName = "Flying Airlines",
            SponsorImageUrl = "http://i.imgur.com/UgAFIrX.png"//"http://i.imgur.com/QD0YrmU.jpg"
        };

        cuponList.Add(cupon11);

        Cupon cupon12 = new Cupon
        {
            Id = 5, // debug catch
            CompanyName = "Cafe Martinez",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponCafeMartinezDescription.ToString()),
            ImageUrl = "http://i.imgur.com/CbxGEKE.jpg",
            SponsorName = "Flying Airlines",
            SponsorImageUrl = "http://i.imgur.com/UgAFIrX.png"//"http://i.imgur.com/QD0YrmU.jpg"
        };

        cuponList.Add(cupon12);
        
        */
        
        Cupon iceCreamCupon = new Cupon
        {
            Id = 1, // debug catch
            CompanyName = "The Ice Shop",
            CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponCherDescription.ToString()),
            //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=4"
            ImageUrl = "http://i.imgur.com/VvpsxaP.jpg",
            SponsorName = "The Ice Shop",
            SponsorImageUrl = "http://i.imgur.com/VvpsxaP.jpg"//http://i.imgur.com/UgAFIrX.png "http://i.imgur.com/QD0YrmU.jpg"
        };
        
        cuponList.Add(iceCreamCupon);

        return cuponList;
    }


}
