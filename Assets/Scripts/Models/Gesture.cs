﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuponGo
{
    public sealed class PetGestureEnum
    {

        private readonly string Name;
        public readonly int Value;

        public static readonly PetGestureEnum none = new PetGestureEnum(0, "none");
        public static readonly PetGestureEnum ok = new PetGestureEnum(1, "ok");
        public static readonly PetGestureEnum point = new PetGestureEnum(2, "point");
        public static readonly PetGestureEnum blink = new PetGestureEnum(3, "blink");
        public static readonly PetGestureEnum clamp = new PetGestureEnum(4, "clamp");
        public static readonly PetGestureEnum follow = new PetGestureEnum(5, "follow");
        public static readonly PetGestureEnum jump = new PetGestureEnum(6, "jump");
        public static readonly PetGestureEnum celebrate = new PetGestureEnum(7, "celebrate");

        private PetGestureEnum(int value, string name)
        {
            Name = name;
            Value = value;
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
