﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CoordinateSystem : System.Object
{
    [SerializeField]
    private double b;
    [SerializeField]
    private List<double> o;

    public double Bias
    {
        get
        {
            return b;
        }

        set
        {
            b = value;
        }
    }

    public List<double> Origin
    {
        get
        {
            return o;
        }

        set
        {
            o = value;
        }
    }
}

[Serializable]
public class ZoomData : System.Object
{
    [SerializeField]
    private int z;
    [SerializeField]
    private int tw;
    [SerializeField]
    private int th;

    public int Z
    {
        get
        {
            return z;
        }

        set
        {
            z = value;
        }
    }

    public int Tw
    {
        get
        {
            return tw;
        }

        set
        {
            tw = value;
        }
    }

    public int Th
    {
        get
        {
            return th;
        }

        set
        {
            th = value;
        }
    }
}

[Serializable]
public class TilesData : System.Object
{
    [SerializeField]
    private int w;
    [SerializeField]
    private int h;
    [SerializeField]
    private int ox;
    [SerializeField]
    private int oy;
    [SerializeField]
    private int b;
    [SerializeField]
    private double pxPerMt;
    [SerializeField]
    private ZoomData zoomData;

    public int Width
    {
        get
        {
            return w;
        }

        set
        {
            w = value;
        }
    }

    public int Height
    {
        get
        {
            return h;
        }

        set
        {
            h = value;
        }
    }

    public int OriginX
    {
        get
        {
            return ox;
        }

        set
        {
            ox = value;
        }
    }

    public int OriginY
    {
        get
        {
            return oy;
        }

        set
        {
            oy = value;
        }
    }

    public int Bias
    {
        get
        {
            return b;
        }

        set
        {
            b = value;
        }
    }

    public double PxPerMt
    {
        get
        {
            return pxPerMt;
        }

        set
        {
            pxPerMt = value;
        }
    }

    private ZoomData ZoomData
    {
        get
        {
            return zoomData;
        }

        set
        {
            zoomData = value;
        }
    }
}


[Serializable]
public class MapData : System.Object {

    [SerializeField]
    private string buildingName;
    [SerializeField]
    private int minFloor;
    [SerializeField]
    private int maxFloor;
    [SerializeField]
    private CoordinateSystem coordinateSystem;
    [SerializeField]
    private TilesData tilesData;

    public string BuildingName
    {
        get
        {
            return buildingName;
        }

        set
        {
            buildingName = value;
        }
    }

    public int MinFloor
    {
        get
        {
            return minFloor;
        }

        set
        {
            minFloor = value;
        }
    }

    public int MaxFloor
    {
        get
        {
            return maxFloor;
        }

        set
        {
            maxFloor = value;
        }
    }

    private CoordinateSystem CoordinateSystem
    {
        get
        {
            return coordinateSystem;
        }

        set
        {
            coordinateSystem = value;
        }
    }

    private TilesData TilesData
    {
        get
        {
            return tilesData;
        }

        set
        {
            tilesData = value;
        }
    }
}
