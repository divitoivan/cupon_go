﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PetMessage : System.Object {

	[SerializeField]
	private string text;

    public delegate void OnMessageAccepted();
    public event OnMessageAccepted onMessageAccepted;


    public delegate void OnMessageShowed();
    public event OnMessageShowed onMessageShowed;

	public string Text {
		get {
			return text;
		}
		set {
			text = value;
		}
	}

    public void OnMsgAccepted()
    {
        if (onMessageAccepted != null)
        {
            onMessageAccepted.Invoke();
        }
    }

    public void OnMsgShowed()
    {
        if (onMessageShowed != null)
        {
            onMessageShowed.Invoke();
        }
    }

}
