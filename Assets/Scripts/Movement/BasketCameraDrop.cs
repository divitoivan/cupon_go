﻿using UnityEngine;
using UnityEngine.EventSystems;
using CuponGo.Events;

namespace CuponGo
{
    public class BasketCameraDrop : MonoBehaviourEventHandler, IDropHandler
    {

        private float m_BasketCameraDistance;
        private bool m_show;
        private float m_MovePercent;
        private Ray m_AuxRay;
        private Vector2 m_ShowScreenPoint;

        [SerializeField] private float m_ShowTime = 0.5f; // show time [s]
        [SerializeField] private Vector2 m_ShowScreenPercentPoint; // Screen point when show

        public bool Show
        {
            get { return m_show; }
            set { m_show = value; }
        }

        private void Start()
        {
            m_BasketCameraDistance = Vector3.Distance(Camera.main.transform.position, transform.position);
            m_AuxRay.origin = Camera.main.transform.position;
            m_AuxRay.direction = Camera.main.transform.TransformDirection(Vector3.down);
            transform.position = m_AuxRay.GetPoint(m_BasketCameraDistance);
            m_MovePercent = 0;
            Show = false;
            m_ShowScreenPoint = new Vector2();
        }

        private void Update()
        {

            if (Show)
            {
                if(m_MovePercent < 1)
                {
                    m_MovePercent += Time.deltaTime / m_ShowTime;
                }
            } else
            {
                if (m_MovePercent > 0)
                {
                    m_MovePercent -= Time.deltaTime / m_ShowTime;
                }
            }

            m_AuxRay.direction = Camera.main.transform.TransformDirection(Vector3.down + Vector3.forward);
            Vector3 hideVector = m_AuxRay.GetPoint(m_BasketCameraDistance);
            m_ShowScreenPoint.x = m_ShowScreenPercentPoint.x * Screen.width;
            m_ShowScreenPoint.y = m_ShowScreenPercentPoint.y * Screen.height;
            m_AuxRay = Camera.main.ScreenPointToRay(m_ShowScreenPoint);
            Vector3 showVector = m_AuxRay.GetPoint(m_BasketCameraDistance);

            transform.position = Vector3.Slerp(hideVector, showVector, m_MovePercent);

        }

        public void OnDrop(PointerEventData eventData)
        {
            GameObject go = eventData.pointerDrag;
            if (go == null)
                return;

            CuponCameraDrag dragMe = go.GetComponent<CuponCameraDrag>();
            if (dragMe == null)
                return;

            EventManager.Instance.Raise(new OnDropCuponOnBasketEvent{DropConteinerObject = gameObject, DropObject = go});
        }

        public void OnCuponDrag(OnDragCuponEvent e)
        {
            if (e.Cupon == null)
                return;

            CuponCameraDrag dragMe = e.DragObject.GetComponent<CuponCameraDrag>();
            if (dragMe == null)
                return;

            Show = e.DragBegin;
        }

        public override void SubscribeEvents()
        {
            EventManager.Instance.AddListener<OnDragCuponEvent>(OnCuponDrag);
        }

        public override void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<OnDragCuponEvent>(OnCuponDrag);
        }
    }

}

