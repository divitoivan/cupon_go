﻿using UnityEngine;
using CuponGo.Events;

namespace CuponGo
{
    public class CharacterMove : MonoBehaviour
    {

        [SerializeField] private Transform _debugTransform;
        [SerializeField] private Transform _debugErrorTransform;
        [SerializeField] private Transform _debugBearingTransform;
        
        [SerializeField] private float _moveVel = 1.5f; // (m/seg)
        [SerializeField] private float _rotVel = 360; // (deg/seg)
        [SerializeField] private float _moveTime = 3.5f;
        [SerializeField] private float _moveVelMax = 2.5f; // (m/seg)
        [SerializeField] private float _moveVelMin = 1f; // (m/seg)
        [SerializeField] private float _continuousMoveMaxDistance = 30f;
        [SerializeField] private float _walkDistanceThreshold = 1f;
        [SerializeField] private float _rotationThreshold = 0.1f;
        [SerializeField] private bool _isMainCharacter = false;


        private Animator _animator;
        private Quaternion _moveRot = Quaternion.identity;
        private Quaternion _bearingRot = Quaternion.identity;
        private bool _moveEnable = true;

        public CharacterMove()
        {
            BearingEnable = false;
            Moving = false;
        }
        
        public Vector3? MoveDest { get; private set; }

        public Quaternion MoveRot
        {
            get { return _moveRot; }
            private set { _moveRot = value; }
        }

        public Quaternion BearingRot
        {
            get { return _bearingRot; }
            private set { _bearingRot = value; }
        }

        public float MoveVel
        {
            get { return _moveVel; }

            private set { _moveVel = value; }
        }
        
        public float RotVel
        {
            get { return _rotVel; }

            private set { _rotVel = value; }
        }

        public float RotationThreshold
        {
            get { return _rotationThreshold; }
        }

        public float WalkDistanceThreshold
        {
            get { return _walkDistanceThreshold; }
        }

        public bool Moving { get; private set; }

        public bool BearingEnable { get; set; }
        
        public bool IsMainCharacter 
        {
            get { return _isMainCharacter; }
            
            set { _isMainCharacter = value; }
        }
        
        public bool MoveEnable
        {
            get { return _moveEnable; }
            set { _moveEnable = value; }
        }

        private void Start()
        {
            _animator = GetComponent<Animator>();
            MoveDest = transform.position;
        }

        private void Update()
        {
            if (MoveEnable)
            {
                UpdateTransform();
            }
        }

        public void MoveTo(Vector3 dest, float moveVel = -1f, float rotVel = -1f)
        {
            float moveDist = Vector3.Distance(dest, transform.position);

            if (rotVel > 0)
            {
                _rotVel = rotVel;
            }

            if (moveDist >= WalkDistanceThreshold)
            {
                Moving = true;
                MoveDest = dest;
                MoveRot = Quaternion.LookRotation(MoveDest.Value - transform.position);
                MoveVel = moveVel > 0 ? moveVel : moveDist/_moveTime;
                
                if (MoveVel > _moveVelMax) {
                    MoveVel = _moveVelMax;
                } else if (MoveVel < _moveVelMin) {
                    MoveVel = _moveVelMin ;
                }

                if (IsMainCharacter)
                {
                    EventManager.Instance.Raise( new OnMainCharacterMoveEvent {CharacterTransform = transform,
                                                                               MoveDest = dest,
                                                                               MoveVel = moveVel});
                }
                
                if (_debugTransform != null)
                {
                    _debugTransform.position = dest;
                }

            }
            else
            {
                Moving = false;
                MoveDest = transform.position;
                MoveRot = Quaternion.identity;
                MoveVel = 0;
            }

        }

        public void SetMoveError(float xErrorM, float yErrorM)
        {
            if (_debugErrorTransform != null)
            {
                Vector3 localScale = _debugErrorTransform.localScale;
                localScale.x = xErrorM;
                localScale.z = yErrorM;
                _debugErrorTransform.localScale = localScale;
            }
        }

        public void setBearing(float bearing, float rotVel = -1f)
        {
            if (rotVel > 0)
            {
                RotVel = rotVel;
            }
            BearingRot = Quaternion.Euler(0, bearing, 0);
        }

        private void UpdateTransform()
        {

            float animMoveVel = 0;

            if (MoveDest.HasValue)
            {
                float moveDist = Vector3.Distance(MoveDest.Value, transform.position);
                
                if (moveDist >= _continuousMoveMaxDistance)
                {
                    Moving = false;
                    transform.position = MoveDest.Value;
                } else if (!Mathf.Approximately(moveDist, 0f))
                {
                    Moving = true;
                    
                    if (Quaternion.Angle(transform.rotation, MoveRot) >= RotationThreshold)
                    {
                        transform.rotation =
                            Quaternion.RotateTowards(transform.rotation, MoveRot, RotVel * Time.deltaTime);
                    }
                    
                    transform.position =
                        Vector3.MoveTowards(transform.position, MoveDest.Value, MoveVel * Time.deltaTime);
                    animMoveVel = MoveVel;
                } else
                {
                    Moving = false;
                }
                
            }
            
            if (!Moving && BearingEnable)
            {
                transform.rotation =
                    Quaternion.RotateTowards(transform.rotation, BearingRot, RotVel * Time.deltaTime);
            }
            
            if (_animator != null)
            {
                _animator.SetFloat("vel", animMoveVel / transform.localScale.x);
            }

            if (_debugErrorTransform != null && _debugBearingTransform != null)
            {
                _debugErrorTransform.position = transform.position;
                Vector3 bearingDebugPos = transform.position + BearingRot * Vector3.forward;
                bearingDebugPos.y = 0.06f;
                _debugBearingTransform.position = bearingDebugPos;
            }
        }
    }
}