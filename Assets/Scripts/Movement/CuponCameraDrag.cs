﻿using UnityEngine;
using UnityEngine.EventSystems;
using CuponGo.Events;

namespace CuponGo
{
    public class CuponCameraDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {

        private float m_CuponCameraDistance = 0f;
        private bool m_draging;

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (Mathf.Approximately(m_CuponCameraDistance, 0f))
            {
                m_CuponCameraDistance = Vector3.Distance(Camera.main.transform.position, transform.position);
            }
            EventManager.Instance.Raise(new OnDragCuponEvent
            {
                DragObject = gameObject,
                Cupon = gameObject.GetComponent<CuponController>().Cupon,
                DragBegin = true
            });
            GetComponent<Collider>().enabled = false;
            m_draging = true;
            transform.SetParent(Camera.main.transform, true);
        }

        public void OnDrag(PointerEventData eventData)
        {
            Ray ray = Camera.main.ScreenPointToRay(eventData.position);
            transform.position = ray.GetPoint(m_CuponCameraDistance);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            GetComponent<Collider>().enabled = true;
            EventManager.Instance.Raise(new OnDragCuponEvent
            {
                DragObject = gameObject,
                Cupon = gameObject.GetComponent<CuponController>().Cupon,
                DragBegin = false
            });
            m_draging = false;
            transform.SetParent(null, true);
        }
    }
}
