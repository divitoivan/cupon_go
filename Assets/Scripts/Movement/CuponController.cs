﻿using UnityEngine;
using UnityEngine.UI;

namespace CuponGo
{
    public class CuponController : MonoBehaviour
    {
        [SerializeField] private Cupon cupon;

        [SerializeField] private Image _frontImage;
        [SerializeField] private Image _backImage;
        [SerializeField] private Image _leftImage;
        [SerializeField] private Image _rightImage;
        [SerializeField] private Image _topImage;
        [SerializeField] private Image _bottomImage;

        //private MeshRenderer meshRenderer;

        public Cupon Cupon
        {
            get { return cupon; }
            set
            {
                cupon = value;
                //loadCuponTexture();
                OnCuponDataUpdate();
            }
        }

        private void Start()
        {
            /*
            meshRenderer = GetComponent<MeshRenderer>();

            MeshFilter meshFilter = GetComponent<MeshFilter>();

            Vector2[] UvMap = new Vector2[meshFilter.mesh.uv.Length];
            UvMap = meshFilter.mesh.uv;

            // back map
            UvMap[6] = new Vector2(1.0f, 0.0f);
            UvMap[7] = new Vector2(0.0f, 0.0f);
            UvMap[10] = new Vector2(1.0f, 1.0f);
            UvMap[11] = new Vector2(0.0f, 1.0f);

            meshFilter.mesh.uv = UvMap;

            loadCuponTexture();
            */
            OnCuponDataUpdate();

        }

        private void OnCuponDataUpdate()
        {
            if (cupon == null)
            {
                return;
            }
            if (_frontImage != null)
            {
                StartCoroutine(Utils.loadImage(Cupon.ImageUrl, _frontImage));
            }
            if (_backImage != null)
            {
                StartCoroutine(Utils.loadImage(Cupon.ImageUrl, _backImage));
            }
            if (_leftImage != null)
            {
                StartCoroutine(Utils.loadImage(Cupon.SponsorImageUrl, _leftImage));
            }
            if (_rightImage != null)
            {
                StartCoroutine(Utils.loadImage(Cupon.SponsorImageUrl, _rightImage));
            }
            if (_topImage != null)
            {
                StartCoroutine(Utils.loadImage(Cupon.ImageUrl, _topImage));
            }
            if (_bottomImage != null)
            {
                StartCoroutine(Utils.loadImage(Cupon.ImageUrl, _bottomImage));
            }
            
        }
        /*
        private void loadCuponTexture()
        {
            if (meshRenderer != null && cupon != null)
            {
                StartCoroutine(Utils.loadRenderTexture(Cupon.ImageUrl, meshRenderer));
            }
        }
*/
    }
}