﻿using System.Collections.Generic;
using System.Linq;
using CuponGo.Events;
using UnityEngine;

namespace CuponGo
{

    public class PetGesture : MonoBehaviourEventHandler
    {

        [SerializeField] private float rotVel = 360f;

        private Animator animator;
        private List<AddPetGestureEvent> pendingPetGestureEventList;

        [SerializeField] private bool gestureEnable = true;
        private bool chatMode = false;
        private Quaternion pointingRot ;
        private SceneMode currentMode = SceneMode.FollowUserOnMap;
        [SerializeField] private bool viewToCamera = false;
        private PetGestureEnum lastCelebrateGesture = PetGestureEnum.jump;
        private int lastCelebrateGestureJumpCount = 10 ;

        public bool GestureEnable
        {
            get { return gestureEnable;}
            set { gestureEnable = value;}
        }

        private bool ViewToCamera
        {
            get { return viewToCamera; }
            set { viewToCamera = value; }
        }

        public PetGestureEnum CurrentGesture
        {
            get
            {
                if (PendingPetGestureEventList.Count <= 0)
                {
                    return PetGestureEnum.none;
                }
                return PendingPetGestureEventList[0].PetGesture;
            }
        }
        public List<AddPetGestureEvent> PendingPetGestureEventList
        {
            get { return pendingPetGestureEventList ?? (pendingPetGestureEventList = new List<AddPetGestureEvent>()); }
        }

        private void Start()
        {
            animator = GetComponent<Animator>();
        }

        private void Update()
        {
            if (GestureEnable)
            {
                updateTransform();
            }
        }

        private void InitChatAnimationMode(bool chatMode )
        {
            this.chatMode = chatMode;

            if (chatMode)
            {
                animator.SetFloat("vel", 0f);
                animator.SetBool("chat",true);
                ViewToCamera = true;
            }
            else
            {
                animator.SetBool("chat",false);
                ViewToCamera = false ;
            }
        }

        private void AddGestureEvent(AddPetGestureEvent gestureEvent)
        {

            PendingPetGestureEventList.Add(gestureEvent);

            if (PendingPetGestureEventList.Count <= 1)
            {
                OnCurrentGestureChange();
            }
        }

        private void OnGestureAnimationEnd()
        {
            if (CurrentGesture == PetGestureEnum.celebrate)
            {
                OnCurrentGestureChange();
            }
            else
            {
                RemoveLastGestureEvent();
            }
        }

        private void RemoveLastGestureEvent()
        {
            if (PendingPetGestureEventList.Count > 0)
            {
                PendingPetGestureEventList.RemoveAt(0);
            }
            OnCurrentGestureChange();
        }

        private void OnCurrentGestureChange()
        {
            if (PendingPetGestureEventList.Count > 0)
            {
                InitGesture(PendingPetGestureEventList[0]);
            }
            EventManager.Instance.Raise(new OnPetGestureChangeEvent{CurrentPetGesture = CurrentGesture});
        }

        private void InitGesture(AddPetGestureEvent gestureEvent)
        {
            AddPetGesturePointingEvent pointingEvent = gestureEvent as AddPetGesturePointingEvent;
            if (pointingEvent != null)
            {
                AddPetGesturePointingEvent gesturePointEvent = pointingEvent;
                InitPointing(gesturePointEvent.PointingGameObject);
            } else if (gestureEvent.PetGesture == PetGestureEnum.celebrate)
            {
                animator.SetFloat("vel", 0);
                if (lastCelebrateGesture == PetGestureEnum.jump && lastCelebrateGestureJumpCount >= 3)
                {
                    lastCelebrateGestureJumpCount = 0;
                    lastCelebrateGesture = PetGestureEnum.point;
                    animator.SetTrigger(lastCelebrateGesture.ToString()) ;
                }
                else
                {
                    lastCelebrateGesture = PetGestureEnum.jump;
                    animator.SetTrigger(lastCelebrateGesture.ToString());
                    lastCelebrateGestureJumpCount++;
                }
                /*
                if (lastCelebrateGesture == PetGestureEnum.point)
                {
                    lastCelebrateGesture = PetGestureEnum.clamp;
                    animator.SetTrigger(lastCelebrateGesture.ToString()) ;
                } else (lastCelebrateGesture == PetGestureEnum.jump)
                {
                    lastCelebrateGesture = PetGestureEnum.point;
                    animator.SetTrigger(lastCelebrateGesture.ToString());
                }lastCelebrateGestureJumpCount
                */

            } else
            if (gestureEvent.PetGesture != PetGestureEnum.none)
            {
                animator.SetFloat("vel", 0);
                animator.SetTrigger(gestureEvent.PetGesture.ToString());
            }
        }

        private void InitPointing(GameObject pointingObject)
        {
            if (pointingObject != null && animator != null)
            {
                Vector3 lookPos = pointingObject.transform.position - transform.position;
                lookPos.y = 0;
                float bearing = Quaternion.LookRotation(lookPos).eulerAngles.y;
                pointingRot = Quaternion.Euler(0, bearing, 0) ;

                animator.SetFloat("vel",0);
                animator.SetTrigger(PetGestureEnum.point.ToString());
            }
        }

        //call from animation system event
        public void OnPointAnimationEnd()
        {
            OnGestureAnimationEnd();
        }
        //call from animation system event
        public void OnOkAnimationEnd()
        {
            OnGestureAnimationEnd();
        }
        //call from animation system event
        public void OnJumpAnimationEnd()
        {
            OnGestureAnimationEnd();
        }
        //call from animation system event
        public void OnClampAnimationEnd()
        {
            OnGestureAnimationEnd();
        }
        //call from animation system event
        public void OnFollowMeAnimationEnd()
        {
            OnGestureAnimationEnd();
        }
        //call from animation system event
        public void OnBlinkAnimationEnd()
        {
            OnGestureAnimationEnd();
        }

        private void updateTransform()
        {
            if (CurrentGesture == PetGestureEnum.point)
            {
                transform.rotation =
                    Quaternion.RotateTowards(transform.rotation, pointingRot, rotVel * Time.deltaTime);
            }
            else
            {
                if (ViewToCamera)
                {
                    Vector3 lookPos = Camera.main.transform.position - transform.position;
                    lookPos.y = 0;
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lookPos),
                        rotVel * Time.deltaTime);
                }
            }
        }

        public override void SubscribeEvents()
        {
            EventManager.Instance.AddListener<AddPetGestureEvent>(OnAddPetGestureEvent);
            EventManager.Instance.AddListener<AddPetGesturePointingEvent>(OnAddPetGesturePointingEvent);
            EventManager.Instance.AddListener<OnSceneMapModeChangeEvent>(OnSceneMapModeChangeEvent);
        }

        public override void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<AddPetGestureEvent>(OnAddPetGestureEvent);
            EventManager.Instance.RemoveListener<AddPetGesturePointingEvent>(OnAddPetGesturePointingEvent);
            EventManager.Instance.RemoveListener<OnSceneMapModeChangeEvent>(OnSceneMapModeChangeEvent);
        }

        private void OnAddPetGestureEvent(AddPetGestureEvent e)
        {
            AddGestureEvent(e);
        }

        private void OnAddPetGesturePointingEvent(AddPetGesturePointingEvent e)
        {
            AddGestureEvent(e);
        }

        private void OnSceneMapModeChangeEvent(OnSceneMapModeChangeEvent e)
        {
            currentMode = e.CurrentMode;
            switch (currentMode)
            {
                case SceneMode.FollowUserOnMap:
                    InitChatAnimationMode(false);
                    PendingPetGestureEventList.Clear();
                    break;
                case SceneMode.CenterOnPet:
                    InitChatAnimationMode(true);
                    break;
            }
        }

    }
}
