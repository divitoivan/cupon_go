﻿using UnityEngine;
using CuponGo.Events;

namespace CuponGo
{
    public class PetMove : MonoBehaviourEventHandler
    {

        [SerializeField] private float rotVel = 360; // (deg/seg)
        [SerializeField] private float moveVelMax = 5f; // (m/seg)
        [SerializeField] private float moveVelMin = 0.5f; // (m/seg)
        [SerializeField] private float moveTime = 1f; // (m/seg)
        [SerializeField] private float ContinuousMoveMaxDistance = 500f;
        [SerializeField] private float useBearingRotationThreshold = 0.1f;
        [SerializeField] private float walkDistanceThreshold = 1f;
        [SerializeField] private float moveVel = 1f;

        private Animator animator;
        private Vector3 moveDest;
        private Quaternion moveRot = Quaternion.identity;
        private Quaternion bearingRot = Quaternion.identity;
        private bool moveEnable = true;
        private bool bearingEnable = false;

        public bool MoveEnable
        {
            get { return moveEnable; }
            set { moveEnable = value; }
        }

        public bool BearingEnable
        {
            get { return bearingEnable; }
            set { bearingEnable = value; }
        }

        public float MoveVel
        {
            get { return moveVel; }
            set { moveVel = value; }
        }

        public float RotVel
        {
            get { return rotVel; }
            set { rotVel = value; }
        }

        public float UseBearingRotationThreshold
        {
            get { return useBearingRotationThreshold; }
            set { useBearingRotationThreshold = value; }
        }

        public float WalkDistanceThreshold
        {
            get { return walkDistanceThreshold; }
            set { walkDistanceThreshold = value; }
        }

        private void Start()
        {
            animator = GetComponent<Animator>();
            moveDest = transform.position;
        }

        private void Update()
        {
            if (MoveEnable)
            {
                updateTransform();
            }
        }

        public void moveTo(Vector3 dest, float forceMoveVel = -1f, float forceRotVel = -1f)
        {

            if (forceRotVel > 0f)
            {
                rotVel = forceRotVel;
            }

            float moveDist = Vector3.Distance(dest, transform.position);
            if (moveDist >= WalkDistanceThreshold)
            {
                if (forceMoveVel > 0f)
                {
                    moveVel = forceMoveVel;
                }
                else
                {

                    if (Mathf.Approximately(moveDist, float.PositiveInfinity))
                    {
                        moveVel = 1f;
                    }
                    else
                    {
                        moveVel = moveDist / moveTime;
                    }
                    if (moveVel > moveVelMax)
                    {
                        moveVel = moveVelMax;
                    }
                    else if (moveVel < moveVelMin)
                    {
                        moveVel = moveVelMin;
                    }

                }

                moveDest = dest;
                moveRot = Quaternion.LookRotation(moveDest - transform.position);
            }
            else
            {
                moveDest = transform.position;
            }

        }

        public void setForceBearing(float bearing)
        {
            bearingRot = Quaternion.Euler(0, bearing, 0);
        }

        private void updateTransform()
        {
            float animMoveVel = 0;

            float moveDist = Vector3.Distance(moveDest, transform.position);

            if (moveDist >= ContinuousMoveMaxDistance)
            {
                transform.position = moveDest;
            }
            else if (moveDist >= UseBearingRotationThreshold)
            {
                if (Quaternion.Angle(transform.rotation, moveRot) >= 0.1f)
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, moveRot, rotVel * Time.deltaTime);
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, moveDest, moveVel * Time.deltaTime);
                    animMoveVel = moveVel;
                }
            }
            else
            {
                if (!Mathf.Approximately(moveDist, 0f))
                {
                    transform.position = Vector3.MoveTowards(transform.position, moveDest, moveVel * Time.deltaTime);
                    animMoveVel = moveVel;
                }
                if (BearingEnable)
                {
                    transform.rotation =
                        Quaternion.RotateTowards(transform.rotation, bearingRot, rotVel * Time.deltaTime);
                }
            }

            if (animator != null)
            {
                animator.SetFloat("vel", animMoveVel / transform.localScale.x);
            }

            /*
            float animMoveVel = 0;

            if (moveDest.HasValue)
            {

                float moveDist = Vector3.Distance(moveDest.Value, transform.position);

                if (moveDist >= ContinuousMoveMaxDistance)
                {
                    transform.position = moveDest.Value;
                }
                else
                {
                    if (!Mathf.Approximately(moveDist, 0f))
                    {
                        transform.position =
                            Vector3.MoveTowards(transform.position, moveDest.Value, moveVel * Time.deltaTime);
                        animMoveVel = moveVel;
                    }
                }

                if (bearingEnable)
                {
                    transform.rotation =
                        Quaternion.RotateTowards(transform.rotation, bearingRot, rotVel * Time.deltaTime);
                }
                else
                {
                    if (Quaternion.Angle(transform.rotation, moveRot) >= 0.1f)
                    {
                        transform.rotation =
                            Quaternion.RotateTowards(transform.rotation, moveRot, rotVel * Time.deltaTime);
                    }
                }
                
            }
            
            if (animator != null)
            {
                animator.SetFloat("vel", animMoveVel / transform.localScale.x);
            }
            */
            
        }

        public override void SubscribeEvents()
        {

        }

        public override void UnsubscribeEvents()
        {

        }
    }
}