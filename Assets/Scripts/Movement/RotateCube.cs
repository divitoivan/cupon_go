﻿using UnityEngine;

namespace CuponGo
{
    public class RotateCube : MonoBehaviour
    {

        public float speed = 10f;
        [SerializeField] private Vector3 m_RotateDirecction = Vector3.up;

        private void Update()
        {
            transform.Rotate(m_RotateDirecction, speed * Time.deltaTime, Space.World);
        }
    }
}