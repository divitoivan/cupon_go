﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuponGo
{
	public class MusicBackground : Singleton<MusicBackground> {

		private AudioSource audioSource;

	    private void Start () {
			if (audioSource == null) {
				audioSource = gameObject.AddComponent<AudioSource> () as AudioSource;
				audioSource.clip = Resources.Load ("music/background_map", typeof(AudioClip)) as AudioClip;
				audioSource.playOnAwake = false;
				audioSource.loop = true;
				Play();
			}
		}

		public bool isPlaying()
		{
			if (audioSource != null)
			{
				return audioSource.isPlaying;
			}
			return false;	}

		public void Play()
		{
			if (audioSource != null)
			{
				audioSource.Play ();
			}
		}

		public void stop()
		{
			if (audioSource != null)
			{
				audioSource.Stop();
			}
		}

	}
}
