﻿
using System.Collections;
using UnityEngine;
using CuponGo.Events;

namespace CuponGo
{
    public class CameraSceneController : MonoBehaviourEventHandler {

        [SerializeField] private GameObject catchMsgPopupPrefab;
        [SerializeField] private Canvas msgCanvas;
        [SerializeField] private float stablePositionTime = 0.5f;
        [SerializeField] private float cuponCameraDistance = 2f;
        [SerializeField] private Transform cuponTransform;
        [SerializeField] private Cupon cupon;

        private bool haveCuponPosition = false;
        private float stablePositionTimeCount = 0f;
        private Quaternion lastRotation = Quaternion.identity;

        public Cupon Cupon
        {
            get
            {
                return cupon;
            }
            set
            {
                cupon = value;
                updateCupon();
            }
        }

        private void Start () {
            CuponGoManager GM = CuponGoManager.Instance;
            Screen.fullScreen = false;

            SensorHelper.ActivateRotation();

            updateCupon();

            if (!CuponGoManager.Instance.AppState.CatchCuponMessageShowed)
            {
                ShowCatchCuponMsg();
            }
        }

        private void Update () {

            if (!haveCuponPosition) {
                Quaternion rotation = SensorHelper.rotation;
                if (Quaternion.Angle (rotation, lastRotation) < 10f) {
                    stablePositionTimeCount += Time.deltaTime;
                    if (stablePositionTimeCount > stablePositionTime) {
                        haveCuponPosition = true;
                        Ray ray = new Ray (Camera.main.transform.position, Camera.main.transform.forward);
                        cuponTransform.position = ray.GetPoint (cuponCameraDistance);
                    }
                } else {
                    lastRotation = rotation;
                    stablePositionTimeCount = 0f;
                }
            }
        }

        private void updateCupon()
        {
            if (cuponTransform != null && Cupon != null)
            {
                CuponController controller = cuponTransform.GetComponent<CuponController>();
                if (controller != null)
                {
                    controller.Cupon = Cupon;
                }
            }
        }

        private void ShowCatchCuponMsg(){

            EventManager.Instance.Raise(new CameraActivateRotationEvent{activateRotation = false});

            GameObject popupGo = Instantiate(catchMsgPopupPrefab);
            popupGo.SetActive(true);
            popupGo.transform.SetParent(msgCanvas.transform, false);
            Popup popup = popupGo.GetComponent<Popup>();
            popup.onCloseEvent += () =>
            {
                EventManager.Instance.Raise(new CameraActivateRotationEvent{activateRotation = true}) ;
                CuponGoManager.Instance.AppState.CatchCuponMessageShowed = true;
            };
            popup.Open(msgCanvas);

        }


        public override void SubscribeEvents()
        {
            EventManager.Instance.AddListener<OnDropCuponOnBasketEvent>(OnDropCuponOnBasket);
        }

        public override void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<OnDropCuponOnBasketEvent>(OnDropCuponOnBasket);
        }

        private void OnDropCuponOnBasket(OnDropCuponOnBasketEvent e)
        {
            e.DropObject.SetActive(false);
            CuponGoManager.Instance.LoadMapSceneAfterCupon(cupon) ;
        }
    }
}