﻿using System.Collections;
using System.Collections.Generic;
using CuponGo.Events;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class CherArSceneVumarkController : MonoBehaviourEventHandler {

	
	private VuMarkManager mVuMarkManager;
	private VuMarkTarget mClosestVuMark;
	private Transform mClosetVuMarkTransform;
	private VuMarkTarget mCurrentVuMark;
	private Transform mCurrentVuMarkTransform;

	[SerializeField] private Animator bubbleAnimator;
	[SerializeField] private Text bubbleText;
	[SerializeField] private RectTransform bubbleTransform;
	
	void Start () {
		
		// register callbacks to VuMark Manager
		mVuMarkManager = TrackerManager.Instance.GetStateManager().GetVuMarkManager();
		mVuMarkManager.RegisterVuMarkDetectedCallback(OnVuMarkDetected);
		mVuMarkManager.RegisterVuMarkLostCallback(OnVuMarkLost);
		
	}
	
	void OnDestroy()
	{
		// unregister callbacks from VuMark Manager
		if(mVuMarkManager == null)
			return;
		mVuMarkManager.UnregisterVuMarkDetectedCallback(OnVuMarkDetected);
		mVuMarkManager.UnregisterVuMarkLostCallback(OnVuMarkLost);
	}
	
	public void OnVuMarkDetected(VuMarkTarget target)
	{
		Debug.Log("New VuMark: " + GetVuMarkString(target));
	}

	public void OnVuMarkLost(VuMarkTarget target)
	{
		Debug.Log("Lost VuMark: " + GetVuMarkString(target));

	}

	void Update () {
		UpdateClosestTarget();
	}
	
	void UpdateClosestTarget()
	{
		Camera cam = DigitalEyewearARController.Instance.PrimaryCamera ?? Camera.main;

		float closestDistance = Mathf.Infinity;
		mClosestVuMark = null;

		foreach (var bhvr in mVuMarkManager.GetActiveBehaviours())
		{
			Vector3 worldPosition = bhvr.transform.position;
			Vector3 camPosition = cam.transform.InverseTransformPoint(worldPosition);
			
			float distance = Vector3.Distance(Vector2.zero, camPosition);
			if (distance < closestDistance)
			{
				closestDistance = distance;
				mClosestVuMark = bhvr.VuMarkTarget;
				mClosetVuMarkTransform = bhvr.transform;
			}
		}

		if (mClosestVuMark == null)
		{
			if (mCurrentVuMark != null)
			{
				bubbleAnimator.SetBool("Show",false) ;
				mCurrentVuMark = null;
				mCurrentVuMarkTransform = null;
			}
		}
		else
		{
			if (mCurrentVuMark != mClosestVuMark)
			{
				string vuMarkId = GetVuMarkString(mClosestVuMark);
				//var vuMarkTitle = GetVuMarkDataType(mClosestVuMark);
				//var vuMarkImage = GetVuMarkImage(mClosestVuMark);

				switch (vuMarkId)
				{
						case "VuMark00":
							bubbleText.text = "CAMISA GABON";
							break;
						case "VuMark01":
							bubbleText.text = "CAMISA GAO";
							break;
						default:
							bubbleText.text = "";
							break;
				}
				
				
				if (mCurrentVuMark == null)
				{
					bubbleAnimator.SetBool("Show",true);
				}
            
				mCurrentVuMark = mClosestVuMark ;
				mCurrentVuMarkTransform = mClosetVuMarkTransform;

			}
			
			Vector2 anchorMin = bubbleTransform.anchorMin;
			Vector2 anchorMax = bubbleTransform.anchorMax;
			Vector3 bubbleRotation = bubbleTransform.localRotation.eulerAngles;
			Vector3 textRotation = bubbleText.transform.localRotation.eulerAngles;

			if (cam.WorldToScreenPoint(mCurrentVuMarkTransform.position).y < Screen.height / 2)
			{
				anchorMin.x = 0.5f;
				anchorMin.y = 0.8f;
				anchorMax.x = 1f;
				anchorMax.y = 1f;

				bubbleRotation.z = 0f;
				textRotation.z = 0f;

			}
			else
			{
				anchorMin.x = 0f;
				anchorMin.y = 0f;
				anchorMax.x = 0.5f;
				anchorMax.y = 0.2f;
				
				bubbleRotation.z = 180f;
				textRotation.z = 180f;
			}


			bubbleTransform.anchorMin = anchorMin;
			bubbleTransform.anchorMax = anchorMax;
			bubbleTransform.localRotation = Quaternion.Euler(bubbleRotation);
			bubbleText.transform.localRotation = Quaternion.Euler(textRotation);


		}

		
	}
	
	private string GetVuMarkDataType(VuMarkTarget vumark)
	{
		switch (vumark.InstanceId.DataType)
		{
			case InstanceIdType.BYTES:
				return "Bytes";
			case InstanceIdType.STRING:
				return "String";
			case InstanceIdType.NUMERIC:
				return "Numeric";
		}
		return "";
	}

	private string GetVuMarkString(VuMarkTarget vumark)
	{
		switch (vumark.InstanceId.DataType)
		{
			case InstanceIdType.BYTES:
				return vumark.InstanceId.HexStringValue;
			case InstanceIdType.STRING:
				return vumark.InstanceId.StringValue;
			case InstanceIdType.NUMERIC:
				return vumark.InstanceId.NumericValue.ToString();
		}
		return "";
	}

	private Sprite GetVuMarkImage(VuMarkTarget vumark)
	{
		var instanceImg = vumark.InstanceImage;
		if (instanceImg == null)
		{
			Debug.Log("VuMark Instance Image is null.");
			return null;
		}

		// First we create a texture
		Texture2D texture = new Texture2D(instanceImg.Width, instanceImg.Height, TextureFormat.RGBA32, false);
		texture.wrapMode = TextureWrapMode.Clamp;
		instanceImg.CopyToTexture(texture);
		texture.Apply();

		// Then we turn the texture into a Sprite
		Rect rect = new Rect(0, 0, texture.width, texture.height);
		return Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f));
	}

	public override void SubscribeEvents()
	{
		
	}

	public override void UnsubscribeEvents()
	{
		
	}
}
