﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuponGo
{
    public class CuponMenuScript : MonoBehaviour
    {

        [SerializeField] private CuponListScrollControler scrollController;

        private void Start()
        {

            LoadCuponList();

            scrollController.onCuponListItemClickEvent += onCuponItemClicked;

        }

        private void Update()
        {

        }

        private void LoadCuponList()
        {
            if (CuponService.IsBakendEnabled)
            {
                this.StartCoroutine(this.LoadCuponListCoroutine());
                return;
            }

            List<Cupon> defaultList = Cupon.GetCuponDebugList();

            if (defaultList != null)
            {
                this.LoadCupons(defaultList);
            }
        }

        private IEnumerator LoadCuponListCoroutine()
        {
            var res = CuponService.Instance.GetUserCuponsCoroutine();

            yield return res;

            if (res.Cancelled || res.Error != null)
            {
                yield break;
            }

            this.LoadCupons(res.Result);
        }

        private void LoadCupons(ICollection<Cupon> list)
        {
            var l = scrollController.CuponList;

            foreach (var cupon in list)
            {
                l.Add(cupon);
            }
        }

        public void onCuponItemClicked(GameObject sender, Cupon cupon)
        {
            CuponGoManager.Instance.LoadCuponDetailScene(cupon);
        }

    }
}
