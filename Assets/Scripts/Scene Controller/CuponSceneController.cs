﻿using CuponGo.Events;
using UnityEngine;

namespace CuponGo
{
    public class CuponSceneController : MonoBehaviour
    {

        [SerializeField] private CuponDetailItemController itemController;
        [SerializeField] private Canvas msgCanvas;
        [SerializeField] private GameObject useCuponPopupPrefab;

        private Cupon cupon;

        public Cupon Cupon
        {
            get { return cupon; }
            set
            {
                cupon = value;
                if (itemController != null)
                {
                    itemController.Cupon = cupon;
                }
            }
        }


        private void Start()
        {
            if (cupon != null)
            {
                itemController.Cupon = cupon;
            }
        }

        public void OnUseClicked(){

            GameObject popupGo = Instantiate(useCuponPopupPrefab);
            popupGo.SetActive(true);
            popupGo.transform.SetParent(msgCanvas.transform, false);
            Popup popup = popupGo.GetComponent<Popup>();
            popup.onCloseEvent += () =>
            {
                EventManager.Instance.Raise(new OnBackPressedEvent());
                //TODO agregar borrar cupon
            };
            popup.Open(msgCanvas);

        }

    }
}
