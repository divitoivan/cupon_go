﻿using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using UnityEngine.UI;

namespace CuponGo
{
    public class LoginScreen : MonoBehaviour
    {
        
        private const int STATE_INITIAL = 0;
        private const int STATE_INITIALIZING = 1;
        private const int STATE_INITIALIZED = 2;
        private const int STATE_LOGGINGIN = 3;
        private const int STATE_LOADING = 4;
        private const int STATE_LOADINGPICTURE = 5;
        private const int STATE_LOADED = 6;
        private const int STATE_CONNECTING = 7;

        [SerializeField] private Transform stateTextTransform;
        [SerializeField] private Transform logInButtonTransform;
        [SerializeField] private Transform retryButtonTransform;
        [SerializeField] private Transform logOutButtonTransform;
        [SerializeField] private Transform userButtonTransform;
        [SerializeField] private RawImage userButtonImage;
            
        public Texture btnTexture;
        private Texture2D btnPicture;

        //private bool _initialized;
        //private bool _loggedin;
        //private bool _loaded;
        //private bool _connecting;
        private int _state;

        private string _error;
        private string _userName;
        private string _userId;

        private static DateTime selectedDate = DateTime.Now;

        private GUIStyle _buttonStyle;

        void Start()
        {
#if UNITY_EDITOR
            Application.runInBackground = true;
#endif
        }

        public void OnRetryButtonClicked()
        {
            this._error = null;
            this.startLogin();
        }
        
        public void OnLogInButtonClicked()
        {
            this._state = STATE_LOGGINGIN;
            FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, this.HandleResult);
        }
        
        public void OnAccountButtonClicked()
        {
            this._state = STATE_CONNECTING;
            //CuponService.Instance.LoginAsync(this._userId, AccessToken.CurrentAccessToken.TokenString, res =>
            //{
            //    if (res.Cancelled || res.Error != null)
            //    {
            //        this._state = STATE_LOADED;
            //    }
            //    else
            //    {
            //        Debug.Log("...login OK.");
            //        //SceneManager.LoadScene("MainMenu");
            //        CuponGoManager.Instance.LoadScene(ScenesIndex.MainMenu, false);
            //    }
            //});

            this.StartCoroutine(this.Login(this._userId, AccessToken.CurrentAccessToken.TokenString));
        }
        
        public void OnLogOutButtonClicked()
        {
            CuponService.Instance.LogoutAsync(res =>
            {
                FB.LogOut();
                this._state = STATE_INITIALIZED;
            });
        }

        private void Update()
        {
            if (!string.IsNullOrEmpty(this._error))
            {
                stateTextTransform.GetComponent<Text>().text = String.Format("Error: {0}",this._error);
                stateTextTransform.gameObject.SetActive(true);
                retryButtonTransform.gameObject.SetActive(true);
                
                logInButtonTransform.gameObject.SetActive(false);
                logOutButtonTransform.gameObject.SetActive(false);
                userButtonTransform.gameObject.SetActive(false);
            }
            else
            {
                switch (this._state)
                {
                    case STATE_INITIALIZING:
                    case STATE_LOGGINGIN:
                    case STATE_LOADING:
                    case STATE_LOADINGPICTURE:
                    case STATE_CONNECTING:
                        stateTextTransform.GetComponent<Text>().text = "Loading...";
                        stateTextTransform.gameObject.SetActive(true);
                        
                        retryButtonTransform.gameObject.SetActive(false);
                        logInButtonTransform.gameObject.SetActive(false);
                        logOutButtonTransform.gameObject.SetActive(false);
                        userButtonTransform.gameObject.SetActive(false);
                        break;

                    case STATE_INITIALIZED:
                        logInButtonTransform.gameObject.SetActive(true);
                        
                        stateTextTransform.gameObject.SetActive(false);
                        retryButtonTransform.gameObject.SetActive(false);
                        logOutButtonTransform.gameObject.SetActive(false);
                        userButtonTransform.gameObject.SetActive(false);
                        break;

                    default:
                        logOutButtonTransform.gameObject.SetActive(true);
                        userButtonTransform.gameObject.SetActive(true);
                        
                        logInButtonTransform.gameObject.SetActive(false);
                        stateTextTransform.gameObject.SetActive(false);
                        retryButtonTransform.gameObject.SetActive(false);
                        break;
                }
            }
        }
/*
        void OnGUI()
        {
            //if (!btnTexture)
            //{
            //    Debug.LogError("Please assign a texture on the inspector");
            //    return;
            //}

            //if (GUI.Button(new Rect(10, 10, 50, 50), btnTexture))
            //{
            //    Debug.Log("Clicked the button with an image");
            //}

            //if (GUI.Button(new Rect(10, 70, 50, 30), "Click"))
            //    Debug.Log("Clicked the button with text");

            if (this._buttonStyle == null)
            {
                var centeredStyle2 = GUI.skin.GetStyle("Button");
                this._buttonStyle = new GUIStyle(centeredStyle2);
                this._buttonStyle.fontSize = 12;
            }

            float btnWidth = Screen.width / 2;
            float btnHeight = Screen.height / 10;
            float btnWidthDiv2 = btnWidth / 2f;
            float btnHeightDiv2 = btnHeight / 2f;
            float btnSpacing = btnHeightDiv2;

            if (!string.IsNullOrEmpty(this._error))
            {
                //GUI.Label(new Rect(Screen.width / 2 - btnWidthDiv2, Screen.height / 2 + btnHeightDiv2, btnWidth, btnHeight), this._error);
                if (GUI.Button(new Rect(Screen.width / 2 - btnWidthDiv2, Screen.height / 2 - btnHeightDiv2, btnWidth, btnHeight), this._error, this._buttonStyle))
                {
                    this._error = null;
                    this.startLogin();
                }
            }
            else
            {
                switch (this._state)
                {
                    case STATE_INITIALIZING:
                    case STATE_LOGGINGIN:
                    case STATE_LOADING:
                    case STATE_LOADINGPICTURE:
                    case STATE_CONNECTING:
                        GUI.Label(new Rect(Screen.width / 2 - btnWidthDiv2, Screen.height / 2 - btnHeightDiv2, btnWidth, btnHeight), "loading...");
                        break;

                    case STATE_INITIALIZED:
                        if (GUI.Button(new Rect(Screen.width / 2 - btnWidthDiv2, Screen.height / 2 - btnHeightDiv2 - btnSpacing - btnWidth, btnWidth, btnWidth), this.btnTexture, this._buttonStyle))
                        {
                            this._state = STATE_LOGGINGIN;
                            FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, this.HandleResult);
                        }
                        break;

                    default:
                        if (GUI.Button(new Rect(Screen.width / 2 - btnWidthDiv2, Screen.height / 2 - btnHeightDiv2 - btnSpacing - btnWidth, btnWidth, btnWidth), this.btnPicture, this._buttonStyle))
                        {
                            this._state = STATE_CONNECTING;
                            //CuponService.Instance.LoginAsync(this._userId, AccessToken.CurrentAccessToken.TokenString, res =>
                            //{
                            //    if (res.Cancelled || res.Error != null)
                            //    {
                            //        this._state = STATE_LOADED;
                            //    }
                            //    else
                            //    {
                            //        Debug.Log("...login OK.");
                            //        //SceneManager.LoadScene("MainMenu");
                            //        CuponGoManager.Instance.LoadScene(ScenesIndex.MainMenu, false);
                            //    }
                            //});

                            this.StartCoroutine(this.Login(this._userId, AccessToken.CurrentAccessToken.TokenString));
                        }

                        if (GUI.Button(new Rect(Screen.width / 2 - btnWidthDiv2, Screen.height / 2 - btnHeightDiv2, btnWidth, btnHeight), "Logout", this._buttonStyle))
                        {
                            CuponService.Instance.LogoutAsync(res =>
                            {
                                FB.LogOut();
                                this._state = STATE_INITIALIZED;
                            });
                        }

                        break;
                }
            }
        }
*/
        private IEnumerator Login(string user, string passwd)
        {
            var res = CuponService.Instance.LoginCoroutine(user, passwd);

            //print("login co started");
            yield return res;
            //print("login co finished");

            if (res.Cancelled)
            {
                this._state = STATE_LOADED;
            }
            else if (res.Error != null)
            {
                this._state = STATE_LOADED;
                this._error = res.Error.Message;
            }
            else
            {
                Debug.Log("...login OK.");
                CuponGoManager.Instance.LoadScene(ScenesIndex.Map, true);
            }
        }

        void Awake()
        {
#if !FB_LOGIN
            PlayerPrefs.SetInt("BACKEND_ENABLED", 0);
            CuponGoManager.Instance.LoadScene(ScenesIndex.Map, false);
#else
            CuponService.Instance.LogoutAsync(null);
            this.startLogin();
#endif
        }

        private void startLogin()
        {
            if (FB.IsInitialized)
            {
                if (FB.IsLoggedIn)
                {
                    this.ShowUser();
                }
                else
                {
                    this._state = STATE_INITIALIZED;
                }
            }
            else
            {
                this._state = STATE_INITIALIZING;

                CuponServerLib.CDebug.SetLogProvider(new UnityLogProvider());

                FB.Init(appId: "1522836167734837", onInitComplete: this.OnInitComplete, onHideUnity: this.OnHideUnity);
            }
        }

        private void OnDestroy()
        {
            if (this.btnPicture != null)
            {
                DestroyImmediate(this.btnPicture);
            }
        }

        private void OnInitComplete()
        {
            if (FB.IsInitialized)
            {
                this._state = STATE_INITIALIZED;

                if (FB.IsLoggedIn)
                {
                    this.ShowUser();
                }
            }
        }

        private void OnHideUnity(bool isGameShown)
        {
        }

        protected void HandleResult(IResult result)
        {
            if (result == null)
            {
                this._error = "result is null";
                return;
            }

            this._error = result.Error;

            if (!string.IsNullOrEmpty(result.Error))
            {
                return;
            }

            if (result.Cancelled)
            {
                this._error = "cancelled";
                return;
            }

            this.ShowUser();
        }

        private void ShowUser()
        {
            this._state = STATE_LOADING;
            //FB.API("/me?fields=id,first_name,picture", HttpMethod.GET, this.UserInfoCallback);
            //FB.API("/me?fields=name", HttpMethod.GET, this.UserInfoCallback);
            FB.API("/me?fields=email,first_name,picture.type(large)", HttpMethod.GET, this.UserInfoCallback);
        }

        private void UserInfoCallback(IGraphResult result)
        {
            if (result == null)
            {
                this._error = "result is null";
                return;
            }

            this._error = result.Error;

            if (!string.IsNullOrEmpty(this._error))
            {
                return;
            }

            if (result.Cancelled)
            {
                this._error = "cancelled";
                return;
            }

            if (string.IsNullOrEmpty(this._error))
            {
                print(result.RawResult);

                var dict = result.ResultDictionary;

                this._userName = dict["first_name"] as string;
                //this._userName = dict["name"] as string;
                //this._userId = dict["id"] as string;
                this._userId = dict["email"] as string;

                this._state = STATE_LOADINGPICTURE;
                StartCoroutine(LoadPicture((string)((IDictionary<string, object>)((IDictionary<string, object>)dict["picture"])["data"])["url"]));
            }
        }

        private IEnumerator LoadPicture(string url)
        {
            print("load picture: " + url);

            var www = new WWW(url);
            yield return www;

            if (www.error != null)
            {
                this._error = www.error;
            }
            else
            {
                //this.btnPicture = www.texture;

                if (this.btnPicture != null)
                {
                    DestroyImmediate(this.btnPicture);
                }

                var tex = www.texture;
                this.btnPicture = new Texture2D(tex.width, tex.height, TextureFormat.DXT1, false);
                www.LoadImageIntoTexture(this.btnPicture);

                if (userButtonImage != null)
                {
                    userButtonImage.texture = this.btnPicture;
                }

                this._state = STATE_LOADED;
            }

            www.Dispose();
        }

        public static void ShowMessage(string message)
        {
            using (var cls2 = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            using (var activity = cls2.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    using (var cls = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                    using (var act = cls.GetStatic<AndroidJavaObject>("currentActivity"))
                    using (var Toast = new AndroidJavaClass("android.widget.Toast"))
                    using (var context = act.Call<AndroidJavaObject>("getApplicationContext"))
                    {
                        int LENGTH_LONG = Toast.GetStatic<int>("LENGTH_LONG");
                    //var msg = new AndroidJavaObject("java.lang.String", "hola");

                    //using (var text = Toast.CallStatic<AndroidJavaObject>("makeText", context, msg, LENGTH_LONG))
                    using (var text = Toast.CallStatic<AndroidJavaObject>("makeText", context, message, LENGTH_LONG))
                        {
                            text.Call("show");
                        }
                    }
                }));
            }
        }

        private class UnityLogProvider : CuponServerLib.CDebug.ILogProvider
        {
            public void LogDebug(object message)
            {
                Debug.Log(message);
            }

            public void LogError(object message)
            {
                Debug.LogError(message);
            }

            public void LogWarn(object message)
            {
                Debug.LogWarning(message);
            }
        }
    }
}