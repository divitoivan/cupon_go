﻿using System;
using UnityEngine;
using CuponGo.Events;
using CuponGo.PerchaIPS;

namespace CuponGo
{
    public class MainCharacterController : MonoBehaviourEventHandler
    {

        private Transform mainCharacterTransform;
        private CharacterMove mainCharacterMove;

        private SceneMode currentSceneMode = SceneMode.FollowUserOnMap;

        private bool viewToCamera = false;

        private bool ViewToCamera
        {
            get { return viewToCamera; }
            set
            {
                viewToCamera = value;
                if (mainCharacterMove != null)
                {
                    mainCharacterMove.BearingEnable = viewToCamera;
                }
            }
        }

        private void Start()
        {

            GameObject go = GameObject.FindGameObjectWithTag(TagsEnum.player.ToString());
            if (go != null)
            {
                mainCharacterTransform = go.transform;
                mainCharacterMove = go.GetComponent<CharacterMove>();
            }

        }

        private void Update()
        {
            if (ViewToCamera && mainCharacterTransform != null && mainCharacterMove != null)
            {

                Vector3 lookPos = Camera.main.transform.position - mainCharacterTransform.position;
                lookPos.y = 0;

                mainCharacterMove.setBearing(Quaternion.LookRotation(lookPos).eulerAngles.y);

            }
        }

        public override void SubscribeEvents()
        {
            EventManager.Instance.AddListener<OnSceneMapModeChangeEvent>(OnSceneMapModeChange);
            EventManager.Instance.AddListener<OnUpdatePositionEstimation>(OnUpdatePositionEstimation);
            EventManager.Instance.AddListener<MainCharacterChangeEvent>(OnMainCharacterChangeEvent);
            EventManager.Instance.AddListener<MoveMainCharacterEvent>(OnMoveMainCharacterEvent);
        }

        public override void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<OnSceneMapModeChangeEvent>(OnSceneMapModeChange);
            EventManager.Instance.RemoveListener<OnUpdatePositionEstimation>(OnUpdatePositionEstimation);
            EventManager.Instance.RemoveListener<MainCharacterChangeEvent>(OnMainCharacterChangeEvent);
            EventManager.Instance.RemoveListener<MoveMainCharacterEvent>(OnMoveMainCharacterEvent);
        }

        private void OnSceneMapModeChange(OnSceneMapModeChangeEvent e)
        {
            currentSceneMode = e.CurrentMode;
            if (e.CurrentMode == SceneMode.FollowUserOnMap)
            {
                ViewToCamera = false;
            }

            if (e.CurrentMode == SceneMode.CenterOnPet)
            {
                if (mainCharacterTransform != null && mainCharacterMove != null)
                {
                    mainCharacterMove.MoveTo(mainCharacterTransform.position);
                }
                ViewToCamera = true;
            }
        }

        private void OnUpdatePositionEstimation(OnUpdatePositionEstimation e)
        {
            if (currentSceneMode == SceneMode.FollowUserOnMap)
            {

                if (e.PositionEstimation.position != null)
                {

                    Vector3 move = e.PositionEstimation.GetPositionVector();
                    move.y = 0;

                    mainCharacterMove.MoveTo(move);

                    if (!Double.IsNaN(e.PositionEstimation.bearing))
                    {
                        mainCharacterMove.setBearing((float) e.PositionEstimation.bearing);
                    }
                }

                double[] positionVariance = IpsUnityManager.Instance.PositionEstimation.positionVariance;

                if (positionVariance != null)
                {
                    mainCharacterMove.SetMoveError(2 * Mathf.Sqrt((float) positionVariance[0]),
                        2 * Mathf.Sqrt((float) positionVariance[1]));
                }

            }
        }

        private void OnMoveMainCharacterEvent(MoveMainCharacterEvent e)
        {
            Vector3 hitPoint = e.Dest;
            hitPoint.y = 0;
            mainCharacterMove.MoveTo(hitPoint,e.MoveVel);
        }

        private void OnMainCharacterChangeEvent(MainCharacterChangeEvent e)
        {
            GameObject go = e.NewCharacterTransform != null ? e.NewCharacterTransform.gameObject : null;
            if (go != null)
            {
                Vector3? characterPos = null;
                if (mainCharacterTransform != null)
                {
                    characterPos = mainCharacterTransform.transform.position;
                    //mainCharacterTransform.gameObject.tag = TagsEnum.untagged.ToString();
                    //mainCharacterTransform.gameObject.SetActive(false);
                    Destroy(mainCharacterTransform.gameObject);
                }

                mainCharacterTransform = go.transform;
                mainCharacterMove = go.GetComponent<CharacterMove>();

                mainCharacterTransform.gameObject.tag = TagsEnum.player.ToString();
                mainCharacterTransform.gameObject.SetActive(true);
                if (characterPos != null)
                {
                    mainCharacterTransform.position = characterPos.Value;
                }

                EventManager.Instance.Raise(new OnMainCharacterChangeEvent{NewCharacterTransform = mainCharacterTransform});
            }
        }

    }
}