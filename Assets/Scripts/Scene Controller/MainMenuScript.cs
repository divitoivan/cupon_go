﻿using CuponGo.PerchaIPS;
using UnityEngine;

namespace CuponGo
{
    public class MainMenuScript : MonoBehaviour
    {

        private void Start()
        {
            CuponGoManager GM = CuponGoManager.Instance;
            Screen.fullScreen = false;

            if (GM.GoogleAnalytics != null)
            {
                GM.GoogleAnalytics.LogScreen("Main Menu");
            }
        }
        
        public void openExplorerSceneOfflineTest()
        {
            PlayerPrefs.SetInt("BACKEND_ENABLED", 0);
            IpsParameters ipsParameters = IpsUnityManager.Instance.IpsParameters;
            ipsParameters.neural = false;
            IpsUnityManager.Instance.IpsParameters = ipsParameters;
            CuponGoManager.Instance.AppState.IPSEnable = false;
            
            CuponGoManager.Instance.LoadScene(ScenesIndex.Map);
        }

        public void openExplorerScene()
        {
            PlayerPrefs.SetInt("BACKEND_ENABLED", 1);
            IpsParameters ipsParameters = IpsUnityManager.Instance.IpsParameters;
            ipsParameters.neural = false;
            IpsUnityManager.Instance.IpsParameters = ipsParameters;
            CuponGoManager.Instance.AppState.IPSEnable = true;
            
            CuponGoManager.Instance.LoadScene(ScenesIndex.Login);
        }

        public void openCuponListMenu()
        {
            CuponGoManager.Instance.LoadScene(ScenesIndex.CuponMenu);
        }

    }
}
