﻿using System.Collections.Generic;
using CuponGo.Events;
using UnityEngine;

namespace CuponGo
{
    public class MapLevelSelector : MonoBehaviourEventHandler
    {

        [SerializeField] private List<TextAsset> levelMaterialList;

        private int currentLevel = -1;
        private bool forceLevel = false;
        private MapMeshCreator mapMeshCreator;

        public int CurrentLevel
        {
            get{ return currentLevel; }
            set{ currentLevel = value;}
        }
        public bool ForceLevel {
            get{ return forceLevel; }
            set{ forceLevel = value;}
        }

        public MapLevelSelector()
        {
            ForceLevel = false;
        }

        private void Awake()
        {
            mapMeshCreator = GetComponent<MapMeshCreator>( );
        }

        public void SetLevel(int level, bool forced = false)
        {

            if (CurrentLevel != level && level >= 0 && level < levelMaterialList.Count && (!ForceLevel || forced))
            {
                int prevLevel = CurrentLevel;
                CurrentLevel = level;

                mapMeshCreator.DeleteMap();
                JSONObject mapJsonObject = new JSONObject(levelMaterialList[level].text);
                mapMeshCreator.CreateMap(mapJsonObject);
                //cambio de piso
                EventManager.Instance.Raise(new OnChangeMapLevelEvent {CurrentLevel = level, PrevLevel = prevLevel});

            }
        }

        public override void SubscribeEvents()
        {
            EventManager.Instance.AddListener<ChangeMapLevelEvent>(OnSetLevelEvent);
        }

        public override void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<ChangeMapLevelEvent>(OnSetLevelEvent);
        }

        private void OnSetLevelEvent(ChangeMapLevelEvent e)
        {
            if (e.NewLevel == -1)
            {
                if (currentLevel == 0)
                {
                    SetLevel(1, false);
                }
                else
                {
                    SetLevel(0, false);
                }
            }
            SetLevel(e.NewLevel, e.ForceChange);
        }
    }
}