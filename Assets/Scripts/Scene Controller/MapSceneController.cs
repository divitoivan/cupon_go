﻿﻿using System;
using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
using CuponGo.PerchaIPS;
using CuponGo.Events;

namespace CuponGo
{
    public enum SceneMode {FollowUserOnMap, CenterOnPet}

    public class MapSceneController : MonoBehaviourEventHandler {

        [SerializeField] private int initialMapLevel = 0;
        [SerializeField] private float interactionMaxDistance = 2f;
        [SerializeField] private float positionEstimationUpdatePeriod = 0.2f;
        [SerializeField] private float initialMsgDelay = 2f;
        [SerializeField] private float cuponCreateDelay = 5f;
        [SerializeField] private float cuponCreateDistance = 3f;
        [SerializeField] private float characterMoveDebugVel = 1.5f;
        [SerializeField] private Canvas mainCanvas;
        [SerializeField] private RectTransform menuTransform;
        [SerializeField] private GameObject configPopupPrefab;
        [SerializeField] private GameObject goToStorePopupPrefab;
        [SerializeField] private GameObject promoPrefab;
        [SerializeField] private GameObject mainCharacterMalePrefab;
        [SerializeField] private GameObject mainCharacterFemalePrefab;

        [SerializeField] private GameObject goToStoreDestination;

        private IEnumerator ipsCoroutine;
        private Transform playerTransform;
        private Transform petTransform;

        private int level;
        private ConfigPopUp configPopUp;
        private GoToStorePopUp goToStorePopUp;
        private SceneMode currentMode = SceneMode.FollowUserOnMap;
        private bool setGoToPositionFlag = false;

        private PetMessage goToStorePetMessage;

        private List<GameObject> _cuponGameObjectList;

        public SceneMode CurrentMode {
            get{return currentMode; }
            set{ currentMode = value;}
        }

        private List<GameObject> CuponGameObjectList
        {
            get
            {
                return _cuponGameObjectList ?? new List<GameObject>();
            }
        }

        private void Start () {
            CuponGoManager GM = CuponGoManager.Instance;
            Screen.fullScreen = false;

            GameObject characterPrefab = CuponGoManager.Instance.AppState.CurrentCharacterIsMale
                ? mainCharacterMalePrefab
                : mainCharacterFemalePrefab;
            playerTransform = Instantiate(characterPrefab).transform;
            EventManager.Instance.Raise(new MainCharacterChangeEvent{NewCharacterTransform = playerTransform});

            GameObject go = GameObject.FindGameObjectWithTag(TagsEnum.pet.ToString());
            if(go != null)
            {
                petTransform = go.transform;
            }

            if (initialMapLevel >= 0)
            {
                EventManager.Instance.Raise(new ChangeMapLevelEvent(initialMapLevel));
            }


            if (!CuponGoManager.Instance.AppState.InitialPetMessageShowed)
            {
                StartCoroutine(AddPetInitialMsg(initialMsgDelay));
            }

            if (GM.GoogleAnalytics != null)
            {
                GM.GoogleAnalytics.LogScreen("Virtual Map");
            }

            ipsCoroutine = PositionEstimationRequestCoroutine();

            if (CuponGoManager.Instance.AppState.IPSEnable)
            {
                StartCoroutine(ipsCoroutine);
            }
        }



        private IEnumerator AddPetInitialMsg(float delay){

            yield return new WaitForSeconds(delay);

            PetMessage message1 = new PetMessage
            {
                Text = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.MapScenePetMsgWelcome1.ToString())
            };
            EventManager.Instance.Raise(new AddPetMessageEvent{PetMessage = message1});

            PetMessage message2 = new PetMessage
            {
                Text = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.MapScenePetMsgWelcome2.ToString())
            };
            message2.onMessageAccepted += () =>
            {
                CuponGoManager.Instance.AppState.InitialPetMessageShowed = true;
                EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.FollowUserOnMap});
            };

            EventManager.Instance.Raise(new AddPetMessageEvent{PetMessage = message2});

        }

        private IEnumerator PositionEstimationRequestCoroutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(positionEstimationUpdatePeriod);
                
                if (CuponGoManager.Instance.AppState.IPSEnable)
                {
                    IpsUnityManager.Instance.UpdatePositionEstimation();
                    PositionEstimation positionEstimation = IpsUnityManager.Instance.PositionEstimation;

                    EventManager.Instance.Raise(new OnUpdatePositionEstimation{PositionEstimation = positionEstimation});
                }
            }
        }

        public void onCuponItemClicked(GameObject sender, Cupon cupon)
        {
            if (configPopUp != null)
            {
                configPopUp.Close();
                configPopUp = null;
            }
            StartCoroutine(addCuponDelayed(cupon, cuponCreateDelay)) ;
        }

        public void OpenCuponListMenu()
        {
            CuponGoManager.Instance.LoadScene(ScenesIndex.CuponMenu);
        }

        public void OpenAuction()
        {
            //CuponGoManager.Instance.LoadScene(ScenesIndex.QRCapture);
            //CuponGoManager.Instance.LoadScene(ScenesIndex.ArScene);
            CuponGoManager.Instance.LoadScene(ScenesIndex.CherArScene);
        }

        public void OpenCamara()
        {
            CuponGoManager.Instance.LoadScene(ScenesIndex.CameraSigns);
        }

        public void OpenAr()
        {
            //CuponGoManager.Instance.LoadScene(ScenesIndex.ArScene);
            CuponGoManager.Instance.LoadScene(ScenesIndex.ShopWindow);
        }


        public void OpenFriendList(){

            PetMessage message = new PetMessage
            {
                Text = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.MapScenePetMsgWelcome1.ToString())
            };

            EventManager.Instance.Raise(new AddPetMessageEvent{PetMessage = message});

        }

        public void OpenConfigMenu()
        {
            StartCoroutine(OpenConfigMenuDelayed(0.5f));
        }

        private IEnumerator OpenConfigMenuDelayed(float delay)
        {
            yield return new WaitForSeconds(delay);

            EventManager.Instance.Raise(new CameraActivateRotationEvent{activateRotation = false});

            GameObject popup = Instantiate(configPopupPrefab);
            popup.SetActive(true);
            popup.transform.localScale = Vector3.zero;
            popup.transform.SetParent(mainCanvas.transform, false);
            configPopUp = popup.GetComponent<ConfigPopUp>();
            configPopUp.SetCuponList(Cupon.GetCuponDebugList());
            configPopUp.AddCuponController.onCuponListItemClickEvent += onCuponItemClicked;
            configPopUp.onCloseEvent += () =>
            {
                EventManager.Instance.Raise(new CameraActivateRotationEvent{activateRotation = true}) ;
                menuTransform.gameObject.SetActive (true);
            };

            configPopUp.Open(mainCanvas);
            menuTransform.gameObject.SetActive (false);
        }

        public void OpenGoToStorePopup()
        {
            EventManager.Instance.Raise(new CameraActivateRotationEvent{activateRotation = false});

            GameObject popup = Instantiate(goToStorePopupPrefab);
            popup.SetActive(true);
            popup.transform.localScale = Vector3.zero;
            popup.transform.SetParent(mainCanvas.transform, false);
            goToStorePopUp = popup.GetComponent<GoToStorePopUp>();
            goToStorePopUp.onCloseEvent += () =>
            {
                EventManager.Instance.Raise(new CameraActivateRotationEvent{activateRotation = true});
            };
            goToStorePopUp.Open(mainCanvas);

        }

        public void OpenFindProductPopup()
        {
        }

        public void OpenFindPromoPopup()
        {
        }

        public void OpenFindFriendPopup()
        {
        }

        public void OpenFindCarPopup()
        {
            EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.FollowUserOnMap});
            EventManager.Instance.Raise(new AddPetGestureEvent{PetGesture = PetGestureEnum.follow}) ;
            EventManager.Instance.Raise(new MovePetEvent(goToStoreDestination.transform.position));
            
            Cupon iceCreamCupon = new Cupon
            {
                Id = 1, // debug catch
                CompanyName = "Ice Cream",
                CuponDescription = LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.DefaultCuponCherDescription.ToString()),
                //ImageUrl = "http://pspet.ddns.net:8083/Cupongo/Brand/Logo?id=4"
                ImageUrl = "http://i.imgur.com/VvpsxaP.jpg",
                SponsorName = "Ice Cream",
                SponsorImageUrl = "http://i.imgur.com/VvpsxaP.jpg"//http://i.imgur.com/UgAFIrX.png "http://i.imgur.com/QD0YrmU.jpg"
            };
            
            StartCoroutine(addCuponDelayed(iceCreamCupon, 5f)) ;
            StartCoroutine(ShowIceCreamMsg()) ;
            
        }

        private IEnumerator ShowIceCreamMsg()
        {
            yield return new WaitForSeconds(7f);
            PetMessage iceCreamPetMessage = new PetMessage
            {
                Text = "The Ice Shop offers you a free Ice Cream before you leave, interested?"
            };
            iceCreamPetMessage.onMessageAccepted += () =>
            {
                EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.FollowUserOnMap});
            };

            EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.CenterOnPet});
            EventManager.Instance.Raise(new AddPetMessageEvent{PetMessage = iceCreamPetMessage, urgentMessage = true}) ;
        } 

        public void onPopupClose()
        {
        }

        private void SetSceneMode(SceneMode sceneMode){
            if (sceneMode != currentMode) {
                SceneMode lastMode = currentMode;
                currentMode = sceneMode;

                EventManager.Instance.Raise(new OnSceneMapModeChangeEvent{CurrentMode = currentMode,PrevMode = lastMode});

                switch (sceneMode) {
                    case SceneMode.FollowUserOnMap:
                        EventManager.Instance.Raise(new CameraCenterOnObjectEvent{CenterGameObject = null});
                        menuTransform.gameObject.SetActive (true);
                        break;

                    case SceneMode.CenterOnPet:
                        EventManager.Instance.Raise(new CameraCenterOnObjectEvent{CenterGameObject = petTransform.gameObject});
                        menuTransform.gameObject.SetActive (false);
                        break;
                }
            }
        }

        private IEnumerator SetSceneModeDelayed(SceneMode sceneMode, float delay)
        {
            yield return new WaitForSeconds(delay);
            SetSceneMode(sceneMode);
        }

        private IEnumerator addCuponDelayed(Cupon cupon, float delay)
        {
            yield return new WaitForSeconds(delay);

            //this.addCupon(cupon);
            EventManager.Instance.Raise(new OnNewCuponEvent { Cupon = cupon });
        }

        private void addCupon(Cupon cupon)
        {
            GameObject cuponGO = Instantiate(promoPrefab);
            CuponGameObjectList.Add(cuponGO);
            cuponGO.SetActive(true);

            Ray ray = new Ray(playerTransform.position,Quaternion.AngleAxis(UnityEngine.Random.Range(-45f,45f), Vector3.up) * Camera.main.transform.TransformDirection(Vector3.forward));
            Vector3 position = ray.GetPoint(cuponCreateDistance);

            position.y = 1f;
            cuponGO.transform.position = position;
            CuponController controller = cuponGO.GetComponent<CuponController>();
            if (controller != null)
            {
                controller.Cupon = cupon;
            }
            EventManager.Instance.Raise(new OnCuponAppearsEvent{CuponGameObject = cuponGO,Cupon = cupon});
        }

        public override void SubscribeEvents()
        {
            EventManager.Instance.AddListener<OnTouchClickEvent>(OnTouchClickEvent);
            EventManager.Instance.AddListener<OnUpdatePositionEstimation>(OnUpdatePositionEstimation);
            EventManager.Instance.AddListener<ChangeMapSceneModeEvent>(OnChangeMapSceneModeEvent);
            EventManager.Instance.AddListener<GoToStoreEvent>(OnGoToStoreEvent);
            EventManager.Instance.AddListener<OnAcceptPetMessageEvent>(OnAcceptPetMessageEvent);
            EventManager.Instance.AddListener<MainCharacterChangeGenderEvent>(OnMainCharacterChangeGenderEvent);
            EventManager.Instance.AddListener<OnMainCharacterChangeEvent>(OnMainCharacterChangeEvent);
            EventManager.Instance.AddListener<SetGoToPositionEvent>(OnSetGoToPositionEvent);
            EventManager.Instance.AddListener<OnEnableIPSChangeEvent>(OnEnableIPSChangeEvent);
            EventManager.Instance.AddListener<OnNewCuponEvent>(OnNewCuponEvent);
            EventManager.Instance.AddListener<OnChangeMapLevelEvent>(OnChangeMapLevelEvent);
        }

        public override void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<OnTouchClickEvent>(OnTouchClickEvent);
            EventManager.Instance.RemoveListener<OnUpdatePositionEstimation>(OnUpdatePositionEstimation);
            EventManager.Instance.RemoveListener<ChangeMapSceneModeEvent>(OnChangeMapSceneModeEvent);
            EventManager.Instance.RemoveListener<GoToStoreEvent>(OnGoToStoreEvent);
            EventManager.Instance.RemoveListener<OnAcceptPetMessageEvent>(OnAcceptPetMessageEvent);
            EventManager.Instance.RemoveListener<MainCharacterChangeGenderEvent>(OnMainCharacterChangeGenderEvent);
            EventManager.Instance.RemoveListener<OnMainCharacterChangeEvent>(OnMainCharacterChangeEvent);
            EventManager.Instance.RemoveListener<SetGoToPositionEvent>(OnSetGoToPositionEvent);
            EventManager.Instance.RemoveListener<OnEnableIPSChangeEvent>(OnEnableIPSChangeEvent);
            EventManager.Instance.RemoveListener<OnNewCuponEvent>(OnNewCuponEvent);
            EventManager.Instance.RemoveListener<OnChangeMapLevelEvent>(OnChangeMapLevelEvent);
        }

        private void OnTouchClickEvent(OnTouchClickEvent e)
        {
            Vector3 hitPoint = e.ClickedPoint;
            SceneMode currenSceneMode = currentMode;

            switch (currenSceneMode)
            {
                case SceneMode.FollowUserOnMap:

                    if (e.ClickedObject.CompareTag(TagsEnum.floor.ToString()))
                    {
                        if (setGoToPositionFlag && goToStoreDestination != null)
                        {
                            setGoToPositionFlag = false;
                            goToStoreDestination.transform.position = e.ClickedPoint;
                        }
                        else
                        {
                            if (!CuponGoManager.Instance.AppState.IPSEnable)
                            {
                                EventManager.Instance.Raise(new MoveMainCharacterEvent{Dest = e.ClickedPoint, MoveVel = characterMoveDebugVel});
                            }
                        }
                    } else
                    if (e.ClickedObject.CompareTag(TagsEnum.pet.ToString ())) {
                        EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.CenterOnPet});
                        EventManager.Instance.Raise(new MovePetEvent(true));
                    } else
                    if (e.ClickedObject.CompareTag(TagsEnum.cupon.ToString ()) && Vector3.Distance(playerTransform.position, hitPoint) <= interactionMaxDistance) {
                        CuponController controller = e.ClickedObject.GetComponent<CuponController>() ;
                        if (controller != null && controller.Cupon != null)
                        {
                            CuponGoManager.Instance.LoadCameraDetailScene(controller.Cupon);
                        }
                    }
                    break;

                case SceneMode.CenterOnPet:

                    break;
            }
        }

        private void OnUpdatePositionEstimation(OnUpdatePositionEstimation e)
        {
            if (e.PositionEstimation != null && e.PositionEstimation.position != null)
            {
                if (currentMode == SceneMode.FollowUserOnMap)
                {
                    Vector3 move = e.PositionEstimation.GetPositionVector();
                    int level = Mathf.FloorToInt(move.y);

                    if (level != this.level)
                    {
                        this.level = level;
                        EventManager.Instance.Raise(new ChangeMapLevelEvent(level));
                    }

                }
            }
        }

        private void OnChangeMapSceneModeEvent(ChangeMapSceneModeEvent e)
        {
            SetSceneMode(e.newSceneMode);
        }

        private void OnGoToStoreEvent(GoToStoreEvent e)
        {
            goToStorePetMessage = new PetMessage
            {
                Text = String.Format(LocalizationManager.Instance.GetLocalizedValue(LocalizationEnum.MapScenePetMsgGoToStore.ToString()),e.StoreName)
            };
            goToStorePetMessage.onMessageAccepted += () =>
            {
                EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.FollowUserOnMap});
                EventManager.Instance.Raise(new AddPetGestureEvent{PetGesture = PetGestureEnum.follow}) ;
                EventManager.Instance.Raise(new MovePetEvent(goToStoreDestination.transform.position));
            };

            EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.CenterOnPet});
            EventManager.Instance.Raise(new AddPetMessageEvent{PetMessage = goToStorePetMessage, urgentMessage = true}) ;

        }

        private void OnAcceptPetMessageEvent(OnAcceptPetMessageEvent e)
        {
        }

        private void OnMainCharacterChangeGenderEvent(MainCharacterChangeGenderEvent e)
        {
            GameObject mainCharacter = Instantiate(e.characterIsMale ? mainCharacterMalePrefab : mainCharacterFemalePrefab);
            EventManager.Instance.Raise(new MainCharacterChangeEvent{NewCharacterTransform = mainCharacter.transform});
            CuponGoManager.Instance.AppState.CurrentCharacterIsMale = e.characterIsMale;
        }

        private void OnMainCharacterChangeEvent(OnMainCharacterChangeEvent e)
        {
            playerTransform = e.NewCharacterTransform ;
        }

        private void OnSetGoToPositionEvent(SetGoToPositionEvent e)
        {
            setGoToPositionFlag = true;
        }

        private void OnEnableIPSChangeEvent(OnEnableIPSChangeEvent e)
        {
            if (CuponGoManager.Instance.AppState.IPSEnable)
            {
                ipsCoroutine = PositionEstimationRequestCoroutine();
                StartCoroutine(ipsCoroutine);
            }
            else
            {
                StopCoroutine(ipsCoroutine);
            }
        }
        
        private void OnNewCuponEvent(OnNewCuponEvent e)
        {
            this.addCupon(e.Cupon);
        }

        private void OnChangeMapLevelEvent(OnChangeMapLevelEvent e)
        {
            foreach (GameObject cuponGoGameObject in CuponGameObjectList)
            {
                Destroy(cuponGoGameObject);
            }
            CuponGameObjectList.Clear();
        }
        
    }
}


