﻿using System.Collections;
using UnityEngine;
using CuponGo.Events;
using UnityEngine.SceneManagement;

namespace CuponGo
{
    public class PetController : MonoBehaviourEventHandler
    {

        private Transform petTransform;
        private CharacterMove petMove;
        private PetGesture petGestureController;
        private PetMessageController petMessageController;
        private PetMenuController petMenuController;

        private Transform mainCharacterTransform;
        private CharacterMove mainCharacterMove;

        [SerializeField] private float petMoveRadiusFromPlayer = 2f;
        [SerializeField] private float petMoveTime = 2f;
        [SerializeField] private float petMoveFollowDelay = 1f;

        private float lastAngle = 180f;
        private Vector3 lastPosCenter;
        private float minAngleDelta = 20f;
        private SceneMode currentMode = SceneMode.FollowUserOnMap;
        private bool followMainCharacter = true;

        public bool FollowMainCharacter
        {
            get { return followMainCharacter; }
            set { followMainCharacter = value; }
        }

        private void Start()
        {

            GameObject go = GameObject.FindGameObjectWithTag(TagsEnum.pet.ToString());
            if (go != null)
            {
                petTransform = go.transform;
                petMove = go.GetComponent<CharacterMove>();
                petGestureController = go.GetComponent<PetGesture>();
                petMessageController = go.GetComponent<PetMessageController>();
                petMenuController = go.GetComponent<PetMenuController>();
            }

            go = GameObject.FindGameObjectWithTag(TagsEnum.player.ToString());
            if (go != null)
            {
                mainCharacterTransform = go.transform;
                mainCharacterMove = go.GetComponent<CharacterMove>();
            }

            if (petMove != null)
            {
                StartCoroutine(ChangePetPositionCoroutine());
            }
        }

        private IEnumerator ChangePetPositionCoroutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(petMoveTime);
                if (currentMode == SceneMode.FollowUserOnMap && followMainCharacter)
                {
                    float angle = lastAngle + Random.value * 90f - 45f;
                    if (lastAngle - angle <= minAngleDelta)
                    {
                        angle = lastAngle + (angle < lastAngle ? -minAngleDelta : minAngleDelta);
                    }
                    Vector3 center = Vector3.zero;
                    float forcePetVel = -1;
                    if (mainCharacterMove != null)
                    {
                        center = mainCharacterMove.Moving ? lastPosCenter : mainCharacterTransform.position;
                        forcePetVel = mainCharacterMove.Moving ? mainCharacterMove.MoveVel : -1;
                    }
                    ChangePetPosition(center, angle, forcePetVel);
                }
            }
        }

        public void ChangePetPosition(Vector3 center, float angle, float forcePetVel = -1)
        {
            lastAngle = angle;
            lastPosCenter = center;
            petMove.MoveTo(
                center + petMoveRadiusFromPlayer * (Quaternion.AngleAxis(angle, Vector3.up) * Vector3.forward),
                forcePetVel);
        }

        private IEnumerator ChangePetPositionAfterTime(float time, Vector3 center, float angle, float forcePetVel = -1)
        {
            yield return new WaitForSeconds(time);
            if (currentMode == SceneMode.FollowUserOnMap)
            {
                ChangePetPosition(center, angle, forcePetVel);
            }
        }

        private void UpdateVisualElements()
        {
            switch (currentMode)
            {
                case SceneMode.FollowUserOnMap:
                    petMessageController.ShowingMessage = false;
                    if (petMessageController.CurrentPetMessage != null)
                    {
                        petMessageController.ShowingExclamationMark = true;
                    }

                    petMenuController.ShowingMenu = false;
                    break;
                case SceneMode.CenterOnPet:
                    petMessageController.ShowingExclamationMark = false ;
                    if (petMessageController.CurrentPetMessage != null)
                    {
                        petMessageController.ShowingMessage = true;
                        petMenuController.ShowingMenu = false;
                    }
                    else
                    {
                        petMessageController.ShowingMessage = false ;
                        petMenuController.ShowingMenu = true;
                    }
                    petMove.MoveTo(petTransform.position);
                    break;
            }
        }

        public override void SubscribeEvents()
        {
            EventManager.Instance.AddListener<OnPetGestureChangeEvent>(OnPetGestureChange);
            EventManager.Instance.AddListener<OnSceneMapModeChangeEvent>(OnSceneMapModeChange);
            EventManager.Instance.AddListener<OnCuponAppearsEvent>(OnCuponAppears);
            EventManager.Instance.AddListener<OnMainCharacterMoveEvent>(OnMainCharacterMove);
            EventManager.Instance.AddListener<OnTouchClickEvent>(OnTouchClickEvent);
            EventManager.Instance.AddListener<OnAddPetMessageEvent>(OnAddPetGestureEvent);
            EventManager.Instance.AddListener<OnAcceptPetMessageEvent>(OnAcceptPetMessageEvent);
            EventManager.Instance.AddListener<MovePetEvent>(OnMovePetEvent);
            EventManager.Instance.AddListener<OnMainCharacterChangeEvent>(OnMainCharacterChangeEvent);
        }

        public override void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<OnPetGestureChangeEvent>(OnPetGestureChange);
            EventManager.Instance.RemoveListener<OnSceneMapModeChangeEvent>(OnSceneMapModeChange);
            EventManager.Instance.RemoveListener<OnCuponAppearsEvent>(OnCuponAppears);
            EventManager.Instance.RemoveListener<OnMainCharacterMoveEvent>(OnMainCharacterMove);
            EventManager.Instance.RemoveListener<OnTouchClickEvent>(OnTouchClickEvent);
            EventManager.Instance.RemoveListener<OnAddPetMessageEvent>(OnAddPetGestureEvent);
            EventManager.Instance.RemoveListener<OnAcceptPetMessageEvent>(OnAcceptPetMessageEvent);
            EventManager.Instance.RemoveListener<MovePetEvent>(OnMovePetEvent);
            EventManager.Instance.RemoveListener<OnMainCharacterChangeEvent>(OnMainCharacterChangeEvent);
        }

        private void OnCuponAppears(OnCuponAppearsEvent e)
        {
            EventManager.Instance.Raise(new AddPetGesturePointingEvent(e.CuponGameObject,petMove.RotVel));
        }

        private void OnPetGestureChange(OnPetGestureChangeEvent e)
        {
            petMove.MoveEnable = e.CurrentPetGesture == PetGestureEnum.none;
        }

        public void OnMainCharacterMove(OnMainCharacterMoveEvent e)
        {
            if (petMove != null && currentMode == SceneMode.FollowUserOnMap && FollowMainCharacter)
            {
                StartCoroutine(ChangePetPositionAfterTime(petMoveFollowDelay, e.MoveDest, lastAngle, e.MoveVel));
            }
        }

        private void OnSceneMapModeChange(OnSceneMapModeChangeEvent e)
        {
            currentMode = e.CurrentMode;
            UpdateVisualElements();
        }

        private void OnTouchClickEvent(OnTouchClickEvent e)
        {

        }

        private void OnAddPetGestureEvent(OnAddPetMessageEvent gestureEvent)
        {
            UpdateVisualElements();
        }

        private void OnAcceptPetMessageEvent(OnAcceptPetMessageEvent e)
        {
            UpdateVisualElements();
        }

        private void OnMovePetEvent(MovePetEvent e)
        {
            FollowMainCharacter = e.FollowMainCharacter;
            if (!e.FollowMainCharacter)
            {
                petMove.MoveTo(e.Dest,1.5f) ;
            }
        }

        private void OnMainCharacterChangeEvent(OnMainCharacterChangeEvent e)
        {
            mainCharacterTransform = e.NewCharacterTransform;
            mainCharacterMove = e.NewCharacterTransform != null ? e.NewCharacterTransform.GetComponent<CharacterMove>() : null;
        }

    }
}