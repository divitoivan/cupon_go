﻿using System.Collections;
using System.Collections.Generic;
using CuponGo.Events;
using UnityEngine;

public class PetMenuController : MonoBehaviourEventHandler
{

    [SerializeField] private RectTransform petMenuTransform;
    [SerializeField] private RectTransform mainMenuTransform;

    private float activeCanvasDelay = 0.1f;
    private bool showingMenu;

    public bool ShowingMenu {
        get { return showingMenu; }
        set
        {
            bool prevShowing = showingMenu;
            showingMenu = value;
            if (prevShowing != showingMenu)
            {
                UpdateVisualElements();
            }
        }
    }

    private void UpdateVisualElements()
    {
        if (ShowingMenu)
        {
            StartCoroutine(ActiveObjectAsync(petMenuTransform.gameObject, true, activeCanvasDelay));
            StartCoroutine(ActiveObjectAsync(mainMenuTransform.gameObject, true, activeCanvasDelay));
        }
        else
        {
            StartCoroutine(ActiveObjectAsync(petMenuTransform.gameObject, false, activeCanvasDelay));
            StartCoroutine(ActiveObjectAsync(mainMenuTransform.gameObject, false, activeCanvasDelay));
        }
    }

    private IEnumerator ActiveObjectAsync(GameObject gameObject, bool active, float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(active);
    }


    public void OnGoToStoreButtonClicked()
    {

    }

    public void OnReturnToCarClicked()
    {

    }

    public void OnExitButtonClicked()
    {

    }

    public void OnGoToStoreOkButtonClicked()
    {

    }

    public override void SubscribeEvents()
    {
    }

    public override void UnsubscribeEvents()
    {
    }
}
