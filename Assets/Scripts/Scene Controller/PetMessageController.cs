﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CuponGo.Events;

namespace CuponGo
{
    public class PetMessageController : MonoBehaviourEventHandler
    {

        [SerializeField] private Transform exclamationMark;
        [SerializeField] private Canvas canvas;
        [SerializeField] private RectTransform petMessageTransform;
        [SerializeField] private Text messageText;

        private float activeCanvasDelay = 0.1f;
        private List<PetMessage> petMessageList;
        private bool showingMessage = false;
        private bool showingExclamationMark = false;

        public bool ShowingMessage {
            get { return showingMessage; }
            set
            {
                bool prevShowingMessage = showingMessage;
                showingMessage = value;
                if (prevShowingMessage != showingMessage)
                {
                    UpdateVisualElements();
                }
            }
        }

        public bool ShowingExclamationMark {
            get { return showingExclamationMark; }
            set
            {
                bool prevShowingMark = showingExclamationMark;
                showingExclamationMark = value;
                if (showingExclamationMark != prevShowingMark)
                {
                    UpdateVisualElements() ;
                }
            }
        }

        private List<PetMessage> PetMessageList
        {
            get { return petMessageList ?? (petMessageList = new List<PetMessage>()); }
        }

        public PetMessage CurrentPetMessage {
            get
            {
                return PetMessageList.Count > 0 ? PetMessageList[0] : null;
            }
        }

        private void AddPetMessage(PetMessage petMessage, bool urgent = false)
        {
            if(petMessage == null) return;

            if (urgent)
            {
                PetMessageList.Insert(0,petMessage);
            }
            else
            {
                PetMessageList.Add(petMessage);
            }
            UpdateVisualElements();
            EventManager.Instance.Raise(new OnAddPetMessageEvent());
        }

        public void RemoveCurrentPetMessage()
        {
            if (PetMessageList.Count <= 0) return;

            PetMessage msg = PetMessageList[0];
            PetMessageList.RemoveAt(0);
            msg.OnMsgAccepted();
            UpdateVisualElements();
            EventManager.Instance.Raise(new OnAcceptPetMessageEvent{petMessage = msg});
        }

        private IEnumerator RemoveCurrentPetMessageDelayed(float delay)
        {
            yield return new WaitForSeconds(delay);
            RemoveCurrentPetMessage();
        }

        private void UpdateVisualElements()
        {
            if (ShowingMessage && CurrentPetMessage != null)
            {
                messageText.text = CurrentPetMessage.Text;
                StartCoroutine(ActiveCanvasAsync(true, activeCanvasDelay));
            }
            else
            {
                StartCoroutine(ActiveCanvasAsync(false, activeCanvasDelay)) ;
            }

            if (exclamationMark != null)
            {
                exclamationMark.gameObject.SetActive(ShowingExclamationMark);
            }
        }

        public void OnOkButtonClicked()
        {
            EventManager.Instance.Raise(new AcceptPetMessageEvent());
        }

        public void OnMenuOkButtonClicked()
        {
            EventManager.Instance.Raise(new ChangeMapSceneModeEvent{newSceneMode = SceneMode.FollowUserOnMap});
        }

        private IEnumerator ActiveCanvasAsync(bool active, float time)
        {
            yield return new WaitForSeconds(time);
            if (ShowingMessage == active)
            {
                petMessageTransform.gameObject.SetActive(active);
                if (ShowingMessage)
                {
                    CurrentPetMessage.OnMsgShowed();
                }
            }
        }

        public override void SubscribeEvents()
        {
            EventManager.Instance.AddListener<AddPetMessageEvent>(OnAddPetMessageEvent);
            EventManager.Instance.AddListener<AcceptPetMessageEvent>(OnAcceptPetMessageEvent);
        }

        public override void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<AddPetMessageEvent>(OnAddPetMessageEvent);
            EventManager.Instance.RemoveListener<AcceptPetMessageEvent>(OnAcceptPetMessageEvent);
        }

        private void OnAddPetMessageEvent(AddPetMessageEvent e)
        {
            AddPetMessage(e.PetMessage, e.urgentMessage);
        }


        private void OnAcceptPetMessageEvent(AcceptPetMessageEvent e)
        {
            RemoveCurrentPetMessage();
        }
    }
}