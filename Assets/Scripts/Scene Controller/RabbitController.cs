﻿using System.Collections;
using System.Collections.Generic;
using CuponGo;
using CuponGo.Events;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using Vuforia;

public class RabbitController : MonoBehaviourEventHandler, ITrackableEventHandler
{

	[SerializeField] private Transform _imageConteinerTransform;
	
	[SerializeField] private float _imageConteinerScaleScaler = 1f;
	[SerializeField] private float _imageConteinerRotationScaler = 1f;

	private ImageTargetBehaviour _imageTargetBehaviour;	
	private bool _imageConteinerVisible = true;
	
	void Start ()
	{
		_imageTargetBehaviour = GetComponent<ImageTargetBehaviour>();
		if (_imageTargetBehaviour)
		{
			_imageTargetBehaviour.RegisterTrackableEventHandler(this);
		}
		_imageConteinerTransform.gameObject.SetActive(_imageConteinerVisible);
	}

	private void Update()
	{
		if(!_imageConteinerVisible)
			return;
		
		if (_imageTargetBehaviour.CurrentStatus == TrackableBehaviour.Status.NOT_FOUND ||
		    _imageTargetBehaviour.CurrentStatus == TrackableBehaviour.Status.UNDEFINED ||
		    _imageTargetBehaviour.CurrentStatus == TrackableBehaviour.Status.UNKNOWN)
		{
			return;
		}
		
		float verticalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragVerticalAxisName);
		float horizontalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragHorizontalAxisName);
		
		
		Vector3 signScalerDelta = Vector3.one * (verticalMove * _imageConteinerScaleScaler);
			
		Vector3 signScale1 = _imageConteinerTransform.localScale;
		//Vector3 signScale1 =Vector3.one*_imageTargetBehaviour.GetSize().y;
		signScale1 += signScalerDelta;
		if (signScale1.x < 0.001f)
		{
			signScale1 = Vector3.one * 0.001f;
		}
		_imageConteinerTransform.localScale = signScale1;
		//_imageTargetBehaviour.SetHeight(signScale1.magnitude);
		Vector3 conteinerRootRotation = _imageConteinerTransform.localRotation.eulerAngles;
		conteinerRootRotation.y -= horizontalMove * _imageConteinerRotationScaler;
		_imageConteinerTransform.localRotation = Quaternion.Euler(conteinerRootRotation);
		
	}

	public override void SubscribeEvents()
	{
		//EventManager.Instance.AddListener<OnProductBubbleClicked>(OnProductBubbleClicked);
	}

	public override void UnsubscribeEvents()
	{
		//EventManager.Instance.RemoveListener<OnProductBubbleClicked>(OnProductBubbleClicked);
	}

	public void OnProductBubbleClicked(OnProductBubbleClicked e)
	{
		if (_imageTargetBehaviour.CurrentStatus == TrackableBehaviour.Status.DETECTED ||
		    _imageTargetBehaviour.CurrentStatus == TrackableBehaviour.Status.EXTENDED_TRACKED ||
		    _imageTargetBehaviour.CurrentStatus == TrackableBehaviour.Status.TRACKED)
		{
			_imageConteinerVisible = !_imageConteinerVisible;
			_imageConteinerTransform.gameObject.SetActive(_imageConteinerVisible);
			EventManager.Instance.Raise(new OnProductImageConteinerShow{Show = _imageConteinerVisible});
		}
		
	}

	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			_imageConteinerTransform.gameObject.SetActive(_imageConteinerVisible);
			EventManager.Instance.Raise(new OnProductImageConteinerShow{Show = _imageConteinerVisible});
		}
		else
		{
			_imageConteinerTransform.gameObject.SetActive(false);
			EventManager.Instance.Raise(new OnProductImageConteinerShow{Show = false});
		}
	}
}
