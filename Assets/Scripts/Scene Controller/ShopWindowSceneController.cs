﻿using System;
using System.Collections;
using System.Collections.Generic;
using CuponGo;
using CuponGo.Events;
using Facebook.Unity;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class ShopWindowSceneController : MonoBehaviourEventHandler
{

	[SerializeField] private Animator billboard1Animator;
	[SerializeField] private Animator billboard2Animator;
	[SerializeField] private Animator billboard3Animator;
	[SerializeField] private Canvas backgroundCanvas;
	[SerializeField] private Transform cuponTransform;
	[SerializeField] private Transform conteinerTransform;
	
	[SerializeField] private bool show;
	
	[SerializeField] private float cuponCameraDistance = 2f;
	[SerializeField] private float stablePositionTime = 0.5f;
	[SerializeField] private float backgroundCanvasDistance = 3f;
	
	[SerializeField] private float _pitchZoomScaler = 5f;
	[SerializeField] private float _horizontalRotationScaler = 180f; 
	[SerializeField] private float _verticalRotationScaler = 180f;
	
	private bool haveCuponPosition = false;
	private Quaternion lastRotation = Quaternion.identity;
	private float stablePositionTimeCount = 0f;

	// Use this for initialization
	void Start () {
		
		CuponGoManager GM = CuponGoManager.Instance;

		SensorHelper.ActivateRotation();

	}

	private void Update()
	{
		if (haveCuponPosition)
		{
			float horizontalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragHorizontalAxisName);
			float verticalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragVerticalAxisName);
			float pitch = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchPitchAxisName);
			
			Vector3 conteinerPosition = conteinerTransform.localPosition;
			conteinerPosition.z += pitch*_pitchZoomScaler;
			backgroundCanvas.planeDistance = conteinerPosition.z + backgroundCanvasDistance;
			conteinerTransform.localPosition = conteinerPosition;

			Vector3 conteinerRootRotation = conteinerTransform.parent.localRotation.eulerAngles;
			conteinerRootRotation.y += horizontalMove * _horizontalRotationScaler;
			conteinerRootRotation.x -= verticalMove * _verticalRotationScaler;
			conteinerTransform.parent.localRotation = Quaternion.Euler(conteinerRootRotation);
		}
		else
		{
			Quaternion rotation = SensorHelper.rotation;
			if (Quaternion.Angle (rotation, lastRotation) < 10f) {
				stablePositionTimeCount += Time.deltaTime;
				if (stablePositionTimeCount > stablePositionTime) {
					haveCuponPosition = true;
					Vector3 camaraRotation = Camera.main.transform.rotation.eulerAngles;
					camaraRotation.x = 0;
					camaraRotation.z = 0;
					conteinerTransform.parent.rotation = Quaternion.Euler(camaraRotation);
				}
			} else {
				lastRotation = rotation;
				stablePositionTimeCount = 0f;
			}
		}
		
		
		
	}

	IEnumerator InitAnimDelay()
	{
		yield return new WaitForSeconds(1);
		billboard1Animator.SetBool("Show",true);
		billboard2Animator.SetBool("Show",true);
		billboard3Animator.SetBool("Show",true);
	}

	public override void SubscribeEvents()
	{
		
		EventManager.Instance.AddListener<OnTouchClickEvent>(OnTouchClickEvent);
	}

	public override void UnsubscribeEvents()
	{
		
		EventManager.Instance.RemoveListener<OnTouchClickEvent>(OnTouchClickEvent);
	}

	private void OnTouchClickEvent(OnTouchClickEvent e)
	{
		if (e.ClickedObject.CompareTag(TagsEnum.cupon.ToString()))
		{
			e.ClickedObject.SetActive(false);
			StartCoroutine(InitAnimDelay());
		}
	}
}
