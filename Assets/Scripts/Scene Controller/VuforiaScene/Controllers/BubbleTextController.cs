﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CuponGo.Events;


namespace CuponGo
{
	public class BubbleTextController : MonoBehaviourEventHandler
	{

		private BubbleTextView _bubbleTextView;
		private Transform _bubbleTransform;
		private RectTransform _bubbleRectTransform;
		private RectTransform _bubbleTextRectTransform;
		private VuforiaProductModel _vuforiaProductModel;

		[SerializeField] private RectTransform _plusIconRectTransform;
		[SerializeField] private RectTransform _minusIconRectTransform;
		private bool _plusIconActive = false;
		private bool _bubbleActive = false;

		void Start()
		{
			_bubbleActive = false;
			_bubbleTextView = new BubbleTextView(this.GetComponentInChildren<Image>());
			_bubbleRectTransform = this.GetComponentInChildren<Image>().rectTransform;
			_bubbleTextRectTransform = this.GetComponentInChildren<Text>().rectTransform;			
			_plusIconActive = true;
			_bubbleTextView.ShowBubble(_bubbleActive,_plusIconActive);
		}
		
		public void RefreshBubblePosition(Transform tagTransform)
		{
			if (tagTransform == null)
			{
				return;
			}
			
			Vector2 anchorMin = _bubbleRectTransform.anchorMin;
			Vector2 anchorMax = _bubbleRectTransform.anchorMax;
			Vector3 bubbleRotation = _bubbleRectTransform.localRotation.eulerAngles;
			Vector3 textRotation = _bubbleRectTransform.transform.localRotation.eulerAngles;
	
			if (Camera.main.WorldToScreenPoint(tagTransform.position).y < Screen.height / 2)
			{
				anchorMin.x = 0.3f;
				anchorMin.y = 0.7f;
				anchorMax.x = 0.99f;
				anchorMax.y = 0.99f;
	
				bubbleRotation.z = 0f;
				textRotation.z = 0f;
	
			}
			else
			{
				anchorMin.x = 0.01f;
				anchorMin.y = 0.01f;
				anchorMax.x = 0.7f;
				anchorMax.y = 0.3f;
					
				bubbleRotation.z = 180f;
				textRotation.z = 180f;
			}
	
			_bubbleRectTransform.anchorMin = anchorMin;
			_bubbleRectTransform.anchorMax = anchorMax;
			_bubbleRectTransform.localRotation = Quaternion.Euler(bubbleRotation);
			_bubbleTextRectTransform.transform.localRotation = Quaternion.Euler(textRotation);
			//_bubblePlusInfoIconTransform.transform.localRotation = Quaternion.Euler(textRotation);
			//_bubbleMinusInfoIconTransform.transform.localRotation = Quaternion.Euler(textRotation);
			
		}
		/********************************************************************************************/
		/* Events 
		/********************************************************************************************/
		private void OnBubbleActivateEvent(BubbleActivateEvent e)
		{
			if (_bubbleTextView == null)
				return;
			
			_vuforiaProductModel = e.VuforiaProductModel;
			if (e.Active)
			{
				_bubbleTextView.SetBubbleText(e.VuforiaProductModel.VuforiaBubbleModel.BubbleText);
			}
			_bubbleTextView.ShowBubble(e.Active,_plusIconActive);
			if( _plusIconActive == false )
				EventManager.Instance.Raise(new TrackableStateChangeEvent {ProductModel = _vuforiaProductModel, Active = true});
		}
		
		public void OnBubbleTextClicked()
		{
			if (_bubbleTextView == null)
				return;
			
			_plusIconActive = !_plusIconActive;
			_bubbleTextView.ChangeBubbleSign(_plusIconActive);
			EventManager.Instance.Raise(new TrackableStateChangeEvent {ProductModel = _vuforiaProductModel, Active = !_plusIconActive});
		}
		public override void SubscribeEvents()
		{
			EventManager.Instance.AddListener<BubbleActivateEvent>(OnBubbleActivateEvent);
		}

		public override void UnsubscribeEvents()
		{
			EventManager.Instance.RemoveListener<BubbleActivateEvent>(OnBubbleActivateEvent);

		}
	}
}
