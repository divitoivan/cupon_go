﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.CompilerServices;
using CuponGo.Events;
using UnityStandardAssets.CrossPlatformInput;
using Vuforia;

namespace CuponGo
{
	public class Target3DModelController : MonoBehaviour
	{
		private VuforiaProductModel _vuforiaProductModel;
		public VuforiaProductModel VuforiaProductModel
		{
			get { return _vuforiaProductModel; }
			set
			{
				_vuforiaProductModel = value;
				if (_vuforiaProductModel.Vuforia3DTargetModel.VuforiaTargetModel is VuforiaTargetSignModel)
				{
					this.gameObject.AddComponent<TargetSignController>();
					this.gameObject.GetComponent<TargetSignController>().VuforiaProductModel = _vuforiaProductModel;
				}
				else if (_vuforiaProductModel.Vuforia3DTargetModel.VuforiaTargetModel is VuforiaTargetCubeModel)
				{
					this.gameObject.AddComponent<TargetCubeController>();
					this.gameObject.GetComponent<TargetCubeController>().VuforiaProductModel = _vuforiaProductModel;
				}
			}
		}
		
	}
}