﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using CuponGo.Events;
using UnityStandardAssets.CrossPlatformInput;
using Vuforia;

namespace CuponGo
{
	public class TargetCubeController : MonoBehaviourEventHandler
	{
		private VuforiaProductModel _vuforiaProductModel;

		public VuforiaProductModel VuforiaProductModel
		{
			get { return _vuforiaProductModel; }
			set
			{
				_vuforiaProductModel = value;
				//TODO Las imagenes se deberían descargar al setear el producto, no en el start. 
				//TODO Esto falla cuando se apagan los gameobjects. Conviene usar reactive para integrar ambas cosas 
				/*EventManager.Instance.Raise(new TargetCubeDownloadProductImageEvent {Id = _vuforiaProductModel.Id});
			
				transform.localPosition=new Vector3(0,1,0);
			
				_localScale= this.transform.localScale;
				_localRotation = Quaternion.identity; //Euler(45,45,45);
			
				var list = GetComponentsInChildren<UnityEngine.UI.Image>().ToList();
				_targetCubeView = new TargetCubeView(list, this.transform);
				_targetCubeView.SetTargetScale(_localScale);*/
			}
		}
		
		[SerializeField] private float _scaleScaler = 1f;
		[SerializeField] private float _rotationScaler = 500f;

		private TargetCubeView _targetCubeView;
		private Vector3 _localScale;
		private Quaternion _localRotation;
		
		public float speed = 50f;
		[SerializeField] private Vector3 m_RotateDirecction = new Vector3(0,1,0);
		
		private delegate void DownloadProductImageCallback(List<Texture2D> texture);
	
		/********************************************************************************************/
		/* Unity methods
		/********************************************************************************************/
		public void Start()
		{	
			EventManager.Instance.Raise(new TargetCubeDownloadProductImageEvent {Id = _vuforiaProductModel.Id});
			transform.localPosition=new Vector3(0,1,0);
			
			_localScale= this.transform.localScale;
			_localRotation = Quaternion.identity; //Euler(45,45,45);
			
			var list = GetComponentsInChildren<UnityEngine.UI.Image>().ToList();
			_targetCubeView = new TargetCubeView(list, this.transform);
			_targetCubeView.SetTargetScale(_localScale);
		}

		public void Update()
		{
			ChangeProductScale();
			_targetCubeView.SetTargetScale(_localScale);
			
			transform.Rotate(m_RotateDirecction, speed * Time.deltaTime, Space.World);
		}
		
		/********************************************************************************************/
		/* Custom methods
		/********************************************************************************************/
		private void ChangeProductImages(List<Texture2D> textureList)
		{
			List<Sprite> sprite = new List<Sprite>();

			if (textureList == null)
				return;
			if (textureList.Count == 0)
				return;

			foreach (Texture2D texture in textureList)
			{
				sprite.Add(Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f)));
			}
			_targetCubeView.SetImageSprite(sprite);	
		}

		private void ChangeProductScale()
		{
			float verticalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragVerticalAxisName);
		
			Vector3 scalerDelta = Vector3.one * (verticalMove * _scaleScaler);
			
			Vector3 scale = _localScale;
			scale += scalerDelta;
			if (scale.x < 0.001f)
			{
				scale = Vector3.one * 0.001f;
			}
			_localScale = scale;
		}

		private IEnumerator DownloadProductImage(VuforiaProductModel vuforiaProductModel, DownloadProductImageCallback callback)
		{
			int counter=1;
			string imageName = vuforiaProductModel.ProductName;
			List<string> urlList = vuforiaProductModel.Vuforia3DTargetModel.Target3DModelImageUrl;

			List<Texture2D> textureList = new List<Texture2D>();
			
			foreach (string url in urlList)
			{
				var www = new WWW(url);

				yield return www;

				if (www.error == null)
				{
					var txt = www.texture;

					if (txt != null)
					{
						var fname = Application.temporaryCachePath + imageName + counter + ".jpg";
						counter++;
						if (System.IO.File.Exists(fname))
						{
							System.IO.File.Delete(fname);
						}

						using (var s = System.IO.File.Create(fname))
						{
							var bs = www.bytes;
							s.Write(bs, 0, bs.Length);
						}
						
						textureList.Add(www.texture);
					}
				}
			}
			callback(textureList);			
		}
		
		/********************************************************************************************/
		/* Events 
		/********************************************************************************************/
		
		public void OnDownloadProductImage(TargetCubeDownloadProductImageEvent e)
		{
			if (e.Id == _vuforiaProductModel.Id)
			{
				StartCoroutine(DownloadProductImage(
					_vuforiaProductModel,
					ChangeProductImages));
			}
		}
		
		public override void SubscribeEvents()
		{
			EventManager.Instance.AddListener<TargetCubeDownloadProductImageEvent>(OnDownloadProductImage);
		}

		public override void UnsubscribeEvents()
		{
			EventManager.Instance.RemoveListener<TargetCubeDownloadProductImageEvent>(OnDownloadProductImage);
		}

	}
}