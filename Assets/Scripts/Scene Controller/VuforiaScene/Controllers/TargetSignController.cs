﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.CompilerServices;
using CuponGo.Events;
using UnityStandardAssets.CrossPlatformInput;
using Vuforia;

namespace CuponGo
{
	public class TargetSignController : MonoBehaviourEventHandler
	{
		private VuforiaProductModel _vuforiaProductModel;
		public VuforiaProductModel VuforiaProductModel
		{
			get { return _vuforiaProductModel; }
			set
			{
				_vuforiaProductModel = value; 
				//TODO Las imagenes se deberían descargar al setear el producto, no en el start. 
				//TODO Esto falla cuando se apagan los gameobjects. Conviene usar reactive para integrar ambas cosas
				/*EventManager.Instance.Raise(new TargetSignDownloadProductImageEvent {Id = value.Id});

				_localScale= this.transform.localScale;
				_localRotation= this.transform.localRotation;
				_targetSignView = new TargetSignView(this.GetComponentInChildren<UnityEngine.UI.Image>(), this.transform);
				_targetSignView.SetTargetScale(_localScale);*/
			}
		}
		
		[SerializeField] private float _scaleScaler = 1f;
		[SerializeField] private float _rotationScaler = 500f;

		private TargetSignView _targetSignView;
		private Vector3 _localScale;
		private Quaternion _localRotation;
				
		private delegate void DownloadProductImageCallback(Texture2D texture);
	
		/********************************************************************************************/
		/* Unity methods
		/********************************************************************************************/
		public void Start()
		{	
			EventManager.Instance.Raise(new TargetSignDownloadProductImageEvent {Id = _vuforiaProductModel.Id});

			_localScale= this.transform.localScale;
			_localRotation= this.transform.localRotation;
			_targetSignView = new TargetSignView(this.GetComponentInChildren<UnityEngine.UI.Image>(), this.transform);
			_targetSignView.SetTargetScale(_localScale);
		}

		public void Update()
		{
			ChangeProductScale();
			_targetSignView.SetTargetScale(_localScale);
			ChangeProductRotation();
			_targetSignView.SetTargetRotation(_localRotation);
		}
		
		/********************************************************************************************/
		/* Custom methods
		/********************************************************************************************/
		private void ChangeProductImage(Texture2D texture)
		{
			Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
			_targetSignView.SetImageSprite(sprite);
		}

		private void ChangeProductScale()
		{
			float verticalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragVerticalAxisName);
		
			Vector3 scalerDelta = Vector3.one * (verticalMove * _scaleScaler);
			
			Vector3 scale = _localScale;
			scale += scalerDelta;
			if (scale.x < 0.001f)
			{
				scale = Vector3.one * 0.001f;
			}
			_localScale = scale;
		}

		private void ChangeProductRotation()
		{
			float horizontalMove = CrossPlatformInputManager.GetAxis(MobileInput.inputTouchDragHorizontalAxisName);

			Vector3 conteinerRootRotation = _localRotation.eulerAngles;
			conteinerRootRotation.y -= horizontalMove * _rotationScaler;
			_localRotation = Quaternion.Euler(conteinerRootRotation);
			
		}


		private IEnumerator DownloadProductImage(VuforiaProductModel vuforiaProductModel, DownloadProductImageCallback callback)
		{
			string imageName = vuforiaProductModel.ProductName;
			string url = vuforiaProductModel.Vuforia3DTargetModel.Target3DModelImageUrl[0];
			
			var www = new WWW(url);

			yield return www;

			if (www.error == null)
			{
				var txt = www.texture;

				if (txt != null)
				{
					var fname = Application.temporaryCachePath + imageName + ".jpg";

					if (System.IO.File.Exists(fname))
					{
						System.IO.File.Delete(fname);
					}

					using (var s = System.IO.File.Create(fname))
					{
						var bs = www.bytes;
						s.Write(bs, 0, bs.Length);
					}

					callback(www.texture);
				}
			}
		}
		
		/********************************************************************************************/
		/* Events 
		/********************************************************************************************/
		
		public void OnDownloadProductImage(TargetSignDownloadProductImageEvent e)
		{
			if (e.Id == _vuforiaProductModel.Id)
			{
				StartCoroutine(DownloadProductImage(
					_vuforiaProductModel,
					ChangeProductImage));
			}
		}
		
		public override void SubscribeEvents()
		{
			EventManager.Instance.AddListener<TargetSignDownloadProductImageEvent>(OnDownloadProductImage);
		}

		public override void UnsubscribeEvents()
		{
			EventManager.Instance.RemoveListener<TargetSignDownloadProductImageEvent>(OnDownloadProductImage);
		}

	}
}