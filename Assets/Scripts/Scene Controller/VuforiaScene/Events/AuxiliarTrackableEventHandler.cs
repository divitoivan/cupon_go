﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public delegate void OnTrackableStateChangedDelegate(TrackableBehaviour.Status previousStatus,
	TrackableBehaviour.Status newStatus);

public class AuxiliarTrackableEventHandler : ITrackableEventHandler
{

	private readonly OnTrackableStateChangedDelegate handler;

	public AuxiliarTrackableEventHandler(OnTrackableStateChangedDelegate handler)
	{
		this.handler = handler;
	}

	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		handler(previousStatus, newStatus);
	}
}
