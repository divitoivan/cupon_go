﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuponGo.Events {

	public class TargetSignDownloadProductImageEvent : Event
	{
		public long Id{ get; set; }
	}
	
	public class TargetCubeDownloadProductImageEvent : Event
	{
		public long Id{ get; set; }
	}

	public class Target3DModelActivateEvent : Event
	{
		public long Id { get; set; }
		public bool Active;
	}
	
	public class BubbleActivateEvent : Event
	{
		public bool Active;
		public VuforiaProductModel VuforiaProductModel;
	}

	public class BubbleClickedEvent : Event
	{
	}
	
	public class TrackableStateChangeEvent: Event
	{
		public VuforiaProductModel ProductModel;
		public bool Active;
	}
}
