﻿using CuponGo.Events;
using UnityEngine;

namespace CuponGo
{
    public class TargetEventHandler : MonoBehaviourEventHandler
    {
        private VuforiaProductModel _productModel;
        private GameObject _targetObject;
        
        private void Awake()
        {
            _productModel = this.GetComponentInChildren<Target3DModelController>().VuforiaProductModel;
            _targetObject = transform.GetChild(0).gameObject;
        }

        public void OnTrackableStateChange( TrackableStateChangeEvent e)
        {
            if (e.ProductModel.Id == _productModel.Id)
            {
                if (e.Active)
                {
                    OnTrackingFound();
                }
                else
                {
                    OnTrackingLost();
                }
            }
        }

        /*
        TODO Lograr que se puedan apagar los gameobjects cuando no se los usa.
        Lo idea sería que los gameobjects esten desactivados mientras que no sean invocados por algun gameobject. El problema
        es que al descargar las imagenes, se detienen las corutinas de descarga, y no se pueden bajar las imagenes. Va a
        haber que lograr mejorar esto, porque no es demasiado escalable. 
        */
        private void OnTrackingFound()
        {
            //_targetObject.SetActive(true);
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
            Canvas[] canvasComponents = GetComponentsInChildren<Canvas>(true);

            // Enable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = true;
            }
            
            // Enable canvas:
            foreach (Canvas component in canvasComponents)
            {
                component.enabled = true;
            }
            
            Debug.Log("Trackable " + _productModel.ProductName+ " found");
        }


        private void OnTrackingLost()
        {
            //_targetObject.SetActive(false);

            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
            Canvas[] canvasComponents = GetComponentsInChildren<Canvas>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }
            
            // Enable canvas:
            foreach (Canvas component in canvasComponents)
            {
                component.enabled = false;
            }
            
            Debug.Log("Trackable " + _productModel.ProductName+ " lost");
        }
        public override void SubscribeEvents()
        {
            EventManager.Instance.AddListener<TrackableStateChangeEvent>(OnTrackableStateChange);
        }

        public override void UnsubscribeEvents()
        {
            EventManager.Instance.RemoveListener<TrackableStateChangeEvent>(OnTrackableStateChange);
        }
    }
}
