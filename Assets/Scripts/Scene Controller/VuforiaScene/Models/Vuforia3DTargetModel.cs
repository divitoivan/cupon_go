﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Vuforia3DTargetModel: VuforiaBaseModel
{	
	private static readonly string SignJson= "sign";
	private static readonly string CubeJson= "cube";

	private VuforiaTargetBaseModel _vuforiaTarget;
	public VuforiaTargetBaseModel VuforiaTargetModel{
		get { return _vuforiaTarget; } 
		set{ _vuforiaTarget = value; }
	}
	
	public string Target3DModelName
	{
		get { return _vuforiaTarget.TargetModelName; } 
	}
	
	public List<string> Target3DModelImageUrl
	{
		get
		{
			List<string> list = new List<string>();

			if (_vuforiaTarget is VuforiaTargetSignModel)
			{
				VuforiaTargetSignModel target = _vuforiaTarget as VuforiaTargetSignModel;
				list.Add(target.Target3DSignModelImageUrl);
			}
			else if (_vuforiaTarget is VuforiaTargetCubeModel)
			{
				VuforiaTargetCubeModel target = _vuforiaTarget as VuforiaTargetCubeModel;
				foreach (string str in  target.Target3DCubeModelImageUrlList)
				{
					list.Add(str);
				}
			}
			return list;
		}
	}
	
	public void FromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		FromJson(jsonObject);
	}
	
	public void FromJson(JSONObject jsonObject)
	{
		if (jsonObject.GetField(SignJson) != null)
		{
			VuforiaTargetSignModel target = new VuforiaTargetSignModel();
			_vuforiaTarget  = target;
			target.FromJson(jsonObject.GetField(SignJson));			
		}
		else if (jsonObject.GetField(CubeJson) != null)
		{
			VuforiaTargetCubeModel target = new VuforiaTargetCubeModel();
			_vuforiaTarget  = target;
			target.FromJson(jsonObject.GetField(CubeJson));
		}
	}
	public Vuforia3DTargetModel()
	{
	}
	
	public Vuforia3DTargetModel(string jsonString): this()
	{
		FromJson(jsonString);
	}
	
	public Vuforia3DTargetModel(JSONObject jsonObject): this()
	{
		FromJson(jsonObject);
	}
	
	public static Vuforia3DTargetModel ModelFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return  ModelFromJson(jsonObject);
	}
	
	public static Vuforia3DTargetModel ModelFromJson(JSONObject jsonObject)
	{
		Vuforia3DTargetModel vuforia3DTargetModel=new Vuforia3DTargetModel(jsonObject);
		return vuforia3DTargetModel;
	}
	
	public static List<Vuforia3DTargetModel> ListFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return ListFromJson(jsonObject);
	}
	
	public static List<Vuforia3DTargetModel> ListFromJson(JSONObject jsonObject)
	{
		List<Vuforia3DTargetModel> list = new List<Vuforia3DTargetModel>();
		
		if (jsonObject.type == JSONObject.Type.ARRAY)
		{	
			foreach (JSONObject jsonItem in  jsonObject.list)
			{
				list.Add(Vuforia3DTargetModel.ModelFromJson(jsonItem));
			}
		}
		return list;
	}
}


