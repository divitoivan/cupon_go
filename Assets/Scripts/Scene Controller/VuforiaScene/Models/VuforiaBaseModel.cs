﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VuforiaBaseModel
{
	[SerializeField] private long _id;	//Id of the target model
	
	private static readonly string BaseModelJsonId = "id";


	public long Id { get { return _id; } set { _id=value; } }

	public void FromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		FromJson(jsonObject);
	}
	
	public void FromJson(JSONObject jsonObject)
	{
		Id =  jsonObject.GetField(BaseModelJsonId).i;
	}
	
	public VuforiaBaseModel()
	{
	}
	
	public VuforiaBaseModel(string jsonString)
	{
		FromJson(jsonString);
	}
	
	public VuforiaBaseModel(JSONObject jsonObject)
	{
		FromJson(jsonObject);
	}
	
	
	public static VuforiaBaseModel ModelFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return  ModelFromJson(jsonObject);
	}
	
	public static VuforiaBaseModel ModelFromJson(JSONObject jsonObject)
	{
		VuforiaBaseModel vuforiaBaseModel=new VuforiaBaseModel(jsonObject);
		return vuforiaBaseModel;
	}
	
	public static List<VuforiaBaseModel> ListFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return ListFromJson(jsonObject);
	}
	
	public static List<VuforiaBaseModel> ListFromJson(JSONObject jsonObject)
	{
		List<VuforiaBaseModel> list = new List<VuforiaBaseModel>();
		
		if (jsonObject.type == JSONObject.Type.ARRAY)
		{	
			foreach (JSONObject jsonItem in  jsonObject.list)
			{
				list.Add(VuforiaBaseModel.ModelFromJson(jsonItem));
			}
		}
		return list;
	}
	
}


