﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class VuforiaBubbleModel : VuforiaBaseModel{

	[SerializeField] private string _bubbleText;
	[SerializeField] private bool _isActive;
	
	private static readonly string BubbleTextJsonName = "text";
	private static readonly string BubbleTextIsActiveJsonName = "is_active";
	
	public string BubbleText { get { return _bubbleText; } set { _bubbleText=value; } }
	public bool IsActive{ get { return _isActive; } set { _isActive = value; } }

	public void FromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		FromJson(jsonObject);
	}
	
	public void FromJson(JSONObject jsonObject)
	{
		IsActive = jsonObject.GetField(BubbleTextIsActiveJsonName).b;
		if( IsActive )
			BubbleText = jsonObject.GetField(BubbleTextJsonName).str;
		base.FromJson(jsonObject);
	}
	
	public VuforiaBubbleModel()
	{
	}
	
	public VuforiaBubbleModel(string jsonString)
	{
		FromJson(jsonString);
	}
	
	public VuforiaBubbleModel(JSONObject jsonObject)
	{
		FromJson(jsonObject);
	}
	
	public static VuforiaBubbleModel ModelFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return  ModelFromJson(jsonObject);
	}
	
	public static VuforiaBubbleModel ModelFromJson(JSONObject jsonObject)
	{
		VuforiaBubbleModel vuforiaBubbleModel=new VuforiaBubbleModel(jsonObject);
		return vuforiaBubbleModel;
	}
	
	public static List<VuforiaBubbleModel> ListFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return ListFromJson(jsonObject);
	}
	
	public static List<VuforiaBubbleModel> ListFromJson(JSONObject jsonObject)
	{
		List<VuforiaBubbleModel> list = new List<VuforiaBubbleModel>();
		
		if (jsonObject.type == JSONObject.Type.ARRAY)
		{	
			foreach (JSONObject jsonItem in  jsonObject.list)
			{
				list.Add(VuforiaBubbleModel.ModelFromJson(jsonItem));
			}
		}
		return list;
	}

}
