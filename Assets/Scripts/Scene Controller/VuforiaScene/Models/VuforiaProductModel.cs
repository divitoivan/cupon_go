﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class VuforiaProductModel: VuforiaBaseModel {

	[SerializeField] private string _productName;
	
	[SerializeField] public VuforiaTagModel VuforiaTagModel;
	[SerializeField] public Vuforia3DTargetModel Vuforia3DTargetModel;
	[SerializeField] public VuforiaBubbleModel VuforiaBubbleModel;

	
	private static readonly string ProductJsonName = "product_name";
	private static readonly string TagJson= "tag";
	private static readonly string Target3DJson= "target_3d_model";
	private static readonly string BubbleTextJson= "bubble_text";
	
	public string ProductName { get { return _productName; } set { _productName=value; } }

	public void FromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		FromJson(jsonObject);
	}
	
	public void FromJson(JSONObject jsonObject)
	{
		base.FromJson(jsonObject);
		ProductName=jsonObject.GetField(ProductJsonName).str;
		
		VuforiaTagModel.FromJson(jsonObject.GetField(TagJson));
		Vuforia3DTargetModel.FromJson(jsonObject.GetField(Target3DJson));
		VuforiaBubbleModel.FromJson(jsonObject.GetField(BubbleTextJson));
	}
	
	public VuforiaProductModel()
	{
		VuforiaTagModel = new VuforiaTagModel();
		Vuforia3DTargetModel = new Vuforia3DTargetModel();
		VuforiaBubbleModel = new VuforiaBubbleModel();
	}
	
	public VuforiaProductModel(string jsonString): this()
	{
		FromJson(jsonString);
	}
	
	public VuforiaProductModel(JSONObject jsonObject): this()
	{
		FromJson(jsonObject);
	}
	
	public static VuforiaProductModel ModelFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return  ModelFromJson(jsonObject);
	}
	
	public static VuforiaProductModel ModelFromJson(JSONObject jsonObject)
	{
		VuforiaProductModel vuforiaProductModel=new VuforiaProductModel(jsonObject);
		return vuforiaProductModel;
	}
	
	public static List<VuforiaProductModel> ListFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return ListFromJson(jsonObject);
	}
	
	public static List<VuforiaProductModel> ListFromJson(JSONObject jsonObject)
	{
		List<VuforiaProductModel> list = new List<VuforiaProductModel>();
		
		if (jsonObject.type == JSONObject.Type.ARRAY)
		{	
			foreach (JSONObject jsonItem in  jsonObject.list)
			{
				list.Add(VuforiaProductModel.ModelFromJson(jsonItem));
			}
		}
		return list;
	}
}
