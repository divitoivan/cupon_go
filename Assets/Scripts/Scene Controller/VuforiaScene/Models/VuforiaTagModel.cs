﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VuforiaTagModel: VuforiaBaseModel
{
	[SerializeField] private string _tagName;
	
	private static readonly string TagModelJsonName = "tag_name";


	public string TagName { get { return _tagName; } set { _tagName=value; } }
	
	public void FromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		FromJson(jsonObject);
	}
	
	public void FromJson(JSONObject jsonObject)
	{
		TagName = jsonObject.GetField(TagModelJsonName).str;
		base.FromJson(jsonObject);
	}
	public VuforiaTagModel()
	{
	}
	
	public VuforiaTagModel(string jsonString)
	{
		FromJson(jsonString);
	}
	
	public VuforiaTagModel(JSONObject jsonObject)
	{
		FromJson(jsonObject);
	}
	
	public static VuforiaTagModel ModelFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return  ModelFromJson(jsonObject);
	}
	
	public static VuforiaTagModel ModelFromJson(JSONObject jsonObject)
	{
		VuforiaTagModel vuforiaTagModel=new VuforiaTagModel(jsonObject);
		return vuforiaTagModel;
	}
	
	public static List<VuforiaTagModel> ListFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return ListFromJson(jsonObject);
	}
	
	public static List<VuforiaTagModel> ListFromJson(JSONObject jsonObject)
	{
		List<VuforiaTagModel> list = new List<VuforiaTagModel>();
		
		if (jsonObject.type == JSONObject.Type.ARRAY)
		{	
			foreach (JSONObject jsonItem in  jsonObject.list)
			{
				list.Add(VuforiaTagModel.ModelFromJson(jsonItem));
			}
		}
		return list;
	}
}


