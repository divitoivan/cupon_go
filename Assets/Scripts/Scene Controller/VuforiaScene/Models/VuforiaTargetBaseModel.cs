﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VuforiaTargetBaseModel: VuforiaBaseModel
{
	[SerializeField] private string _targetModelName;
	
	private static readonly string TargetModelJsonName= "model_name";

	public string TargetModelName { get { return _targetModelName; } set { _targetModelName=value; } }

	public void FromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		FromJson(jsonObject);
	}
	
	public void FromJson(JSONObject jsonObject)
	{
		_targetModelName = jsonObject.GetField(TargetModelJsonName).str;
		base.FromJson(jsonObject);
	}
	
	public VuforiaTargetBaseModel()
	{
	}
	
	public VuforiaTargetBaseModel(string jsonString)
	{
		FromJson(jsonString);
	}
	
	public VuforiaTargetBaseModel(JSONObject jsonObject)
	{
		FromJson(jsonObject);
	}
	
	public static VuforiaTargetBaseModel ModelFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return  ModelFromJson(jsonObject);
	}
	
	public static VuforiaTargetBaseModel ModelFromJson(JSONObject jsonObject)
	{
		VuforiaTargetBaseModel vuforiaTargetModel=new VuforiaTargetBaseModel(jsonObject);
		return vuforiaTargetModel;
	}
	
	public static List<VuforiaTargetBaseModel> ListFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return ListFromJson(jsonObject);
	}
	
	public static List<VuforiaTargetBaseModel> ListFromJson(JSONObject jsonObject)
	{
		List<VuforiaTargetBaseModel> list = new List<VuforiaTargetBaseModel>();
		
		if (jsonObject.type == JSONObject.Type.ARRAY)
		{	
			foreach (JSONObject jsonItem in  jsonObject.list)
			{
				list.Add(VuforiaTargetBaseModel.ModelFromJson(jsonItem));
			}
		}
		return list;
	}
}


