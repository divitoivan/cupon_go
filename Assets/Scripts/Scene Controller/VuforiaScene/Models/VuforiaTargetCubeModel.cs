﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VuforiaTargetCubeModel: VuforiaTargetBaseModel
{
	[SerializeField] private List<string> _target3DCubeModelImageUrlList;
	
	private static readonly string Target3DModelJsonImageName= "image_url_list";

	public string Target3DCubeModelName { get { return base.TargetModelName; } set { base.TargetModelName=value; } }

	public List<string> Target3DCubeModelImageUrlList
	{
		get
		{
			if(_target3DCubeModelImageUrlList == null )
				_target3DCubeModelImageUrlList = new List<string>();
			return _target3DCubeModelImageUrlList;
		}
		set
		{
			_target3DCubeModelImageUrlList = value;
		}
	}

	public void FromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		FromJson(jsonObject);
	}
	
	public void FromJson(JSONObject jsonObject)
	{		
		foreach (JSONObject jsonItem in  jsonObject.GetField(Target3DModelJsonImageName).list)
		{
			Target3DCubeModelImageUrlList.Add(jsonItem.str);
		}
		base.FromJson(jsonObject);
	}
	public VuforiaTargetCubeModel()
	{
	}
	
	public VuforiaTargetCubeModel(string jsonString)
	{
		FromJson(jsonString);
	}
	
	public VuforiaTargetCubeModel(JSONObject jsonObject)
	{
		FromJson(jsonObject);
	}
	
	public static Vuforia3DTargetModel ModelFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return  ModelFromJson(jsonObject);
	}
	
	public static Vuforia3DTargetModel ModelFromJson(JSONObject jsonObject)
	{
		Vuforia3DTargetModel vuforia3DTargetModel=new Vuforia3DTargetModel(jsonObject);
		return vuforia3DTargetModel;
	}
	
	public static List<Vuforia3DTargetModel> ListFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return ListFromJson(jsonObject);
	}
	
	public static List<Vuforia3DTargetModel> ListFromJson(JSONObject jsonObject)
	{
		List<Vuforia3DTargetModel> list = new List<Vuforia3DTargetModel>();
		
		if (jsonObject.type == JSONObject.Type.ARRAY)
		{	
			foreach (JSONObject jsonItem in  jsonObject.list)
			{
				list.Add(Vuforia3DTargetModel.ModelFromJson(jsonItem));
			}
		}
		return list;
	}
}


