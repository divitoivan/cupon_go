﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VuforiaTargetSignModel: VuforiaTargetBaseModel
{
	[SerializeField] private string _target3DSignModelImageUrl;
	
	private static readonly string Target3DModelJsonImageName= "image_url";

	public string Target3DSignModelName { get { return base.TargetModelName; } set { base.TargetModelName=value; } }
	public string Target3DSignModelImageUrl { get { return _target3DSignModelImageUrl; } set { _target3DSignModelImageUrl=value; } }

	public void FromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		FromJson(jsonObject);
	}
	
	public void FromJson(JSONObject jsonObject)
	{
		_target3DSignModelImageUrl = jsonObject.GetField(Target3DModelJsonImageName).str;
		base.FromJson(jsonObject);
	}
	
	public VuforiaTargetSignModel()
	{
	}
	
	public VuforiaTargetSignModel(string jsonString)
	{
		FromJson(jsonString);
	}
	
	public VuforiaTargetSignModel(JSONObject jsonObject)
	{
		FromJson(jsonObject);
	}
	
	public static Vuforia3DTargetModel ModelFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return  ModelFromJson(jsonObject);
	}
	
	public static Vuforia3DTargetModel ModelFromJson(JSONObject jsonObject)
	{
		Vuforia3DTargetModel vuforia3DTargetModel=new Vuforia3DTargetModel(jsonObject);
		return vuforia3DTargetModel;
	}
	
	public static List<Vuforia3DTargetModel> ListFromJson(string jsonString)
	{
		JSONObject jsonObject = new JSONObject(jsonString);
		return ListFromJson(jsonObject);
	}
	
	public static List<Vuforia3DTargetModel> ListFromJson(JSONObject jsonObject)
	{
		List<Vuforia3DTargetModel> list = new List<Vuforia3DTargetModel>();
		
		if (jsonObject.type == JSONObject.Type.ARRAY)
		{	
			foreach (JSONObject jsonItem in  jsonObject.list)
			{
				list.Add(Vuforia3DTargetModel.ModelFromJson(jsonItem));
			}
		}
		return list;
	}
}


