﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BubbleTextView
{
	public Image BubbleImage{ get; set; }
	private Text _bubbleText;
	
	private RectTransform _plusIconRectTransform;
	private RectTransform _minusIconRectTransform;
	
	public BubbleTextView()
	{
	}

	public BubbleTextView(Image bubbleImage)
	{
		BubbleImage = bubbleImage;
		_plusIconRectTransform = BubbleImage.transform.GetChild(1).GetComponent<RectTransform>();
		_minusIconRectTransform = BubbleImage.transform.GetChild(2).GetComponent<RectTransform>();
		_bubbleText = BubbleImage.GetComponentInChildren<Text>();
	}
	
	public void SetBubbleText(string bubbleText)
	{
		_bubbleText.text = bubbleText;
	}

	public void ShowBubble(bool active, bool plusActive )
	{
		BubbleImage.enabled = active;
		_bubbleText.enabled = active;
		if (active == true)
		{
			ChangeBubbleSign(plusActive);
		}
		else
		{
			_plusIconRectTransform.gameObject.SetActive(false);
			_minusIconRectTransform.gameObject.SetActive(false);
		}
	}

	public void ChangeBubbleSign(bool plusActive)
	{
		_plusIconRectTransform.gameObject.SetActive(plusActive);
		_minusIconRectTransform.gameObject.SetActive(!plusActive);
	}
}

