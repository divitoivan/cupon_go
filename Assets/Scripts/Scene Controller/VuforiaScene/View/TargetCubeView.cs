﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetCubeView {
	public List<Image> CurrentImage { get; set; }
	public Transform ViewTransform { get; set; }

	public void SetImageSprite(List<Sprite> sprite)
	{
		if (sprite != null)
		{
			int minSize = Math.Min(CurrentImage.Count, sprite.Count);
			for (int i = 0; i < minSize; i++)
			{
				CurrentImage[i].sprite = sprite[i];
			}
		}
	}

	public void SetTargetScale(Vector3 scale)
	{
		ViewTransform.localScale = scale;
	}
	public void SetTargetRotation(Quaternion rotation)
	{
		ViewTransform.localRotation= rotation;
	}

	public TargetCubeView()
	{
	}

	public TargetCubeView(List<Image> image, Transform transform)
	{
		CurrentImage = image;
		ViewTransform = transform;
	}
}
