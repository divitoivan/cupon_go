﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetSignView {
	public Image CurrentImage { get; set; }
	public Transform ViewTransform { get; set; }

	public void SetImageSprite(Sprite sprite)
	{
		CurrentImage.sprite = sprite;
	}

	public void SetTargetScale(Vector3 scale)
	{
		ViewTransform.localScale = scale;
	}
	public void SetTargetRotation(Quaternion rotation)
	{
		ViewTransform.localRotation= rotation;
	}

	public TargetSignView()
	{
	}

	public TargetSignView(Image image, Transform transform)
	{
		CurrentImage = image;
		ViewTransform = transform;
	}
}
