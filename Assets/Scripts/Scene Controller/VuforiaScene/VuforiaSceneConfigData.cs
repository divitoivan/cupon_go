﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class VuforiaSceneConfigData
{	
	public static readonly string JsonDataSetName= "test";

	public static readonly string JsonFolderPath=Path.Combine(Application.streamingAssetsPath, "VuforiaScene");
	public static readonly string JsonProductsListFile = "ProductsList.json";

}


