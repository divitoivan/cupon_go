﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using CuponGo.Events;
using UnityEngine.UI;
using Vuforia;

namespace CuponGo
{
	public class VuforiaSceneManager : MonoBehaviourEventHandler
	{
		private List<VuforiaProductModel> _vuforiaProductsList;

		private TrackableBehaviour _currentTrackableBehaviour;

		private VuforiaARController _vuforiaArController;

		[SerializeField] private RectTransform _bubbleRectTransform;
		/********************************************************************************************
		***	Configure before start
		********************************************************************************************/

		public void Awake()
		{	
			_vuforiaArController = VuforiaARController.Instance;
			_vuforiaArController.RegisterVuforiaStartedCallback(OnVuforiaStarted);
			_vuforiaArController.RegisterOnPauseCallback(OnVuforiaPaused);
			
			//Load models from json
			StartCoroutine(LoadProductsList(Path.Combine(VuforiaSceneConfigData.JsonFolderPath,
				VuforiaSceneConfigData.JsonProductsListFile)));
		}

		public IEnumerator LoadProductsList(string filePath)
		{
			string fileContent = null;

			if (filePath.Contains("://"))
			{
				WWW www = new WWW(filePath);
				yield return www;
				if (string.IsNullOrEmpty(www.error))
				{
					fileContent = www.text;
				}
				else
				{
					Debug.LogError(www.error);
				}
			}
			else
			{
				if (File.Exists(filePath))
				{
					fileContent = System.IO.File.ReadAllText(filePath);
				}
				else
				{
					Debug.LogError("Cannot find file!");
				}
			}
			if (fileContent != null)
			{
				_vuforiaProductsList = VuforiaProductModel.ListFromJson(fileContent);
				_vuforiaArController.RegisterVuforiaStartedCallback(LoadDataSet);
			}
		}

		public void LoadDataSet()
		{
			ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
			DataSet dataSet = objectTracker.CreateDataSet();

			if (dataSet.Load(VuforiaSceneConfigData.JsonDataSetName))
			{
				objectTracker.Stop(); // stop tracker so that we can add new dataset

				if (!objectTracker.ActivateDataSet(dataSet))
				{
					// Note: ImageTracker cannot have more than 100 total targets activated
					Debug.Log("<color=yellow>Failed to Activate DataSet: " + VuforiaSceneConfigData.JsonDataSetName + "</color>");
				}

				if (!objectTracker.Start())
				{
					Debug.Log("<color=yellow>Tracker Failed to Start.</color>");
				}

				IEnumerable<TrackableBehaviour> tbs = TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();
				foreach (TrackableBehaviour tb in tbs)
				{
					foreach (VuforiaProductModel productModel in _vuforiaProductsList)
					{
						if (productModel.VuforiaTagModel.TagName == tb.TrackableName)
						{
							if (productModel.Vuforia3DTargetModel.Target3DModelName != null)
							{
								// instantiate augmentation object and parent to trackable

								GameObject targetObject =
									Instantiate((GameObject) Resources.Load(productModel.Vuforia3DTargetModel.Target3DModelName));
								targetObject.transform.parent = tb.gameObject.transform;
								targetObject.transform.localPosition = new Vector3(0f, 0f, 0f);
								targetObject.transform.localRotation = Quaternion.identity;
								targetObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

								targetObject.GetComponent<Target3DModelController>().VuforiaProductModel = productModel;
							} 
							tb.gameObject.name = productModel.ProductName;
							// add additional script components for trackable
							tb.gameObject.AddComponent<TargetEventHandler>();
							//tb.gameObject.AddComponent<TurnOffBehaviour>();
							tb.RegisterTrackableEventHandler(new AuxiliarTrackableEventHandler((status, newStatus) =>
							{
								if (newStatus == TrackableBehaviour.Status.DETECTED ||
									newStatus == TrackableBehaviour.Status.TRACKED ||
									newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
								{
									TargetStateChange(productModel, true);
								}
								else
								{
									TargetStateChange(productModel, false);
								}
							}));
							break;
						}
					}
				}
				
			}
			else
			{
				Debug.LogError("<color=yellow>Failed to load dataset: '" + VuforiaSceneConfigData.JsonDataSetName + "'</color>");
			}
			
			_vuforiaArController.UnregisterVuforiaStartedCallback(LoadDataSet);
		}

		/********************************************************************************************
		***	Configure after start
		********************************************************************************************/

		private void Update()
		{
			UpdateClosestTrackableBehaviour();
		}

		private void UpdateClosestTrackableBehaviour()
		{
			if (_bubbleRectTransform == null)
				return;
			
			TrackableBehaviour closestTrackableBehaviour = GetNearestTagTrack();

			if (closestTrackableBehaviour != null)
			{
				_bubbleRectTransform.gameObject.GetComponentInChildren<BubbleTextController>()
					.RefreshBubblePosition(closestTrackableBehaviour.transform);
			}
			_currentTrackableBehaviour = closestTrackableBehaviour;
		}

		private TrackableBehaviour GetNearestTagTrack()
		{
			if (Camera.main == null)
				return null;

			float closestDistance = Mathf.Infinity;
			TrackableBehaviour closestTrack = null;
			foreach (TrackableBehaviour track in TrackerManager.Instance.GetStateManager().GetActiveTrackableBehaviours())
			{
				Vector3 worldPosition = track.transform.position;
				Vector3 camPosition = Camera.main.transform.InverseTransformPoint(worldPosition);

				float distance = Vector3.Distance(Vector2.zero, camPosition);
				if (distance < closestDistance)
				{
					closestDistance = distance;
					closestTrack = track;
				}
			}
			return closestTrack;
		}

		private void TargetStateChange(VuforiaProductModel vuforiaProductModel, bool active)
		{
			if (active)
			{
				if (vuforiaProductModel.VuforiaBubbleModel.IsActive)
				{
					EventManager.Instance.Raise(new BubbleActivateEvent {VuforiaProductModel = vuforiaProductModel, Active = true});
				}
				else
				{
					EventManager.Instance.Raise(new TrackableStateChangeEvent {ProductModel = vuforiaProductModel, Active = true});
				}
			}
			else
			{
				EventManager.Instance.Raise(new BubbleActivateEvent {VuforiaProductModel = vuforiaProductModel, Active = false});
				EventManager.Instance.Raise(new TrackableStateChangeEvent {ProductModel = vuforiaProductModel, Active = false});
			}
		}
		
		private void OnVuforiaStarted()
		{
			CameraDevice.Instance.SetFocusMode(
				CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		}
  
		private void OnVuforiaPaused(bool paused)
		{
			if (!paused) // resumed
			{
				// Set again autofocus mode when app is resumed
				CameraDevice.Instance.SetFocusMode(
					CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
			}
		}
		
		public override void SubscribeEvents()
		{
		}

		public override void UnsubscribeEvents()
		{
		}

	}
}