﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuponGo
{
    public sealed class TagsEnum
    {

        private readonly string name;
        private readonly int value;

        public static readonly TagsEnum untagged = new TagsEnum(0, "Untagged");
        public static readonly TagsEnum floor = new TagsEnum(1, "floor");
        public static readonly TagsEnum cupon = new TagsEnum(2, "cupon");
		public static readonly TagsEnum player = new TagsEnum(3, "Player");
		public static readonly TagsEnum pet = new TagsEnum(4, "pet");
		public static readonly TagsEnum controller = new TagsEnum(5, "controller");
        public static readonly TagsEnum analytics = new TagsEnum(6, "analytics");

        private TagsEnum(int value, string name)
        {
            this.name = name;
            this.value = value;
        }

        public override string ToString()
        {
            return name;
        }

    }
}
