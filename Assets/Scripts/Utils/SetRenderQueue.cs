﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetRenderQueue : MonoBehaviour
{

	[SerializeField] private Material material;

	private void Start()
	{
		Image image = GetComponent<Image>();
		if (image != null)
		{
			image.material = material;
		}
		
		RawImage rawImage = GetComponent<RawImage>();
		if (rawImage != null)
		{
			rawImage.material = material;
		}
		/*
		Material[] materials = GetComponent<CanvasRenderer>().materials;
		for (int i = 0; i < materials.Length && i < m_queues.Length; ++i) {
			materials[i].renderQueue = m_queues[i];
		}
		*/
	}

}
