﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace CuponGo
{
    public static class Utils
    {

		public static IEnumerator loadRenderTexture(string url, Renderer renderer, double cachedHours = 24){
			string filePath;
			bool fromWeb;
			WWW www = getCachedWWW(url, cachedHours,out filePath, out fromWeb);
			yield return www;

			if (string.IsNullOrEmpty(www.error))
			{
				if (fromWeb)
				{
					File.WriteAllBytes(filePath, www.bytes);
				}
				Texture2D tex = new Texture2D(512, 512, TextureFormat.DXT5, false);
				tex.LoadImage(www.bytes);
				tex.wrapMode = TextureWrapMode.Clamp;
				renderer.material.mainTexture = tex;
				renderer.material.color = new Color(1f, 1f, 1f, 1f);

				//TODO esto esta mal hay que implementar uniRx y poner esto en le callback de cargar imagen
				//Vector3 scale = new Vector3 (1f,1f * (float)tex.height / (float)tex.width,1f);
				//renderer.gameObject.transform.localScale = scale;

			}
			else
			{
				if (!fromWeb)
				{
					File.Delete(filePath);
					loadRenderTexture(url, renderer, 0); // force download
				}
			}
		}

        public static IEnumerator loadImage(string url, Image image, double cachedHours = 24)
        {
            string filePath;
            bool fromWeb;
            WWW www = getCachedWWW(url, cachedHours,out filePath, out fromWeb);
            yield return www;

            if (string.IsNullOrEmpty(www.error))
            {
                if (fromWeb)
                {
                    Debug.Log("file save " + filePath);
                    File.WriteAllBytes(filePath, www.bytes);
                }
                else
                {
                    Debug.Log("file load " + filePath);
                }
                Texture2D tex = new Texture2D(2, 2);
                tex.LoadImage(www.bytes);
                image.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            }
            else
            {
                Debug.Log("file error " + www.error);
                if (!fromWeb)
                {
                    File.Delete(filePath) ;
                    loadImage(url, image); // force download
                }
            }


        }

        private static WWW getCachedWWW(string url,double cachedHours, out string filePathOut, out bool fromWeb)
        {
            string filePath = Application.persistentDataPath;
            byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(url);
            string aux = System.Convert.ToBase64String(plainTextBytes).Replace("=", string.Empty).Replace('+', '-').Replace('/', '_');
            filePath += "/data/" + aux;
            string loadFilepath = filePath;
            fromWeb = false;
            WWW www;
            bool useCached = false;
            useCached = System.IO.File.Exists(filePath);
            if (useCached)
            {
                System.DateTime written = File.GetLastWriteTimeUtc(filePath);
                System.DateTime now = System.DateTime.UtcNow;
                double totalHours = now.Subtract(written).TotalHours;
                if (totalHours > cachedHours)
                    useCached = false;
            }
            if (System.IO.File.Exists(filePath))
            {
#if UNITY_EDITOR_WIN
                string pathforwww = "file:///" + loadFilepath;
#elif UNITY_ANDROID
                string pathforwww = "file://" + loadFilepath;
#endif
                www = new WWW(pathforwww);
            }
            else
            {
                fromWeb = true;
                www = new WWW(url);
            }

            filePathOut = filePath;
            return www;
        }

    }


}