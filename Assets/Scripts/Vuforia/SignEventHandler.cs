﻿using System;
using System.Collections;
using System.Collections.Generic;
using CuponGo;
using CuponGo.Events;
using UnityEngine;
using Vuforia;

public class SignEventHandler : MonoBehaviourEventHandler,ITrackableEventHandler {
	
	[SerializeField] private Animator signAnimator;
	[SerializeField] private Transform conteinerTransform;
	//[SerializeField] private CherImageIndex ImageIndex;
	
	private TrackableBehaviour mTrackableBehaviour;
	private IEnumerator disableIEnumerator;
	private bool _show = false;

	void Start () {
		
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
		
	}

	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		//EventManager.Instance.Raise(new OnCherImageTrackableStateChanged{ImageIndex = ImageIndex, newStatus = newStatus});
	}
	
	private void ShowArImage(bool show)
	{
		if(_show == show)
			return;
		
		_show = show;
		if (show)
		{
			if (disableIEnumerator != null)
			{
				StopCoroutine(disableIEnumerator);
			}
			conteinerTransform.gameObject.SetActive(true);
		}
		else
		{
			disableIEnumerator = DisableConteinerAfterDelay(1.0f) ;
			StartCoroutine(disableIEnumerator);
		}
		signAnimator.SetBool("Show",show);
	}

	private IEnumerator DisableConteinerAfterDelay(float delay)
	{
		yield return new WaitForSeconds(delay);
		disableIEnumerator = null;
		conteinerTransform.gameObject.SetActive(false);
	}

	public override void SubscribeEvents()
	{
		//EventManager.Instance.AddListener<ShowCherArProduct>(OnShowArProduct);
	}

	public override void UnsubscribeEvents()
	{
		//EventManager.Instance.RemoveListener<ShowCherArProduct>(OnShowArProduct);
	}
	
	/*private void OnShowArProduct(ShowCherArProduct showCherArProduct)
	{
		if (showCherArProduct.ImageIndex == ImageIndex)
		{
			ShowArImage(showCherArProduct.Show);
		}
	}*/

}
